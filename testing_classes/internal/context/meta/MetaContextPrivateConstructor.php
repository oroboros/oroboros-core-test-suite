<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\testclass\internal\context\meta;

/**
 * <Oroboros Testing Meta Context With Private Constructor>
 * A copy of the metacontext class with a different vendor and an inaccessible
 * private constructor, but the same overall namespace structure and logic,
 * for testing factorization. This class has no means of instantiation, and
 * should cause an expected error when the Factory attempts to load it.
 * Testing this functionality is its only purpose, and it is otherwise useless.
 *
 * Otherwise it is identical to the standard MetaContext class in all aspects.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
final class MetaContextPrivateConstructor
    implements \oroboros\core\interfaces\contract\core\context\ContextualContract
{
    use \oroboros\core\traits\core\context\meta\MetaContextTrait;

    private function __construct( $context, $value )
    {
        parent::__construct( $context, $value );
    }
}
