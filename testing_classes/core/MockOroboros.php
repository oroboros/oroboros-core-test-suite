<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\testclass\core;

/**
 * <Oroboros Core Mock Base Accessor>
 * This is a clone of the base accessor that does not automatically
 * get packaged during the setup build for testing pre-initialization
 * trait logic. It is otherwise identical to the standard core base
 * accessor.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
class MockOroboros
    implements \oroboros\core\interfaces\contract\OroborosContract,
    \oroboros\core\interfaces\api\OroborosApi,
    \oroboros\environment\interfaces\enumerated\CoreEnvironment,
    \oroboros\environment\interfaces\enumerated\Environment
{

    //Include the core logic
    use \oroboros\core\traits\OroborosTrait;

    /**
     * Set the class type to core.
     */
    const OROBOROS_CLASS_TYPE = \oroboros\core\interfaces\enumerated\type\ClassType::CLASS_TYPE_CORE;

    /**
     * Set the class scope to global core accessor.
     */
    const OROBOROS_CLASS_SCOPE = \oroboros\core\interfaces\enumerated\scope\ClassScope::CLASS_SCOPE_CORE_GLOBAL;

    /**
     * Set the class context to core internal
     */
    const OROBOROS_CLASS_CONTEXT = \oroboros\core\interfaces\enumerated\context\ClassContext::CLASS_CONTEXT_CORE;

    /**
     * Declare the api as the baseline core api.
     */
    const OROBOROS_API = \oroboros\core\interfaces\enumerated\index\ApiIndex::INDEX_CORE_API;
    const OROBOROS_CORE = true;
    const OROBOROS_PATTERN = false;
    const OROBOROS_UTILITY = false;
    const OROBOROS_LIBRARY = false;
    const OROBOROS_ROUTINE = false;
    const OROBOROS_EXTENSION = false;
    const OROBOROS_MODULE = false;
    const OROBOROS_COMPONENT = false;
    const OROBOROS_ADAPTER = false;
    const OROBOROS_CONTROLLER = false;
    const OROBOROS_MODEL = false;
    const OROBOROS_VIEW = false;

    /**
     * Resets the mock core to an uninitialized
     * state for unit testing purposes.
     *
     * All static baseline, static control api, extendable,
     * and oroboros parameters will be reset to their default
     * uninitialized status.
     *
     * @return void
     */
    public static function reset()
    {
        self::$_oroboros_initialization_registry = array();
        self::$_static_fingerprint = null;
        self::$_static_baseline_initialized = false;
        self::$_static_baseline_child_instances = array();
        self::$_static_baseline_allow_reinitialization = true;
        self::$_static_baseline_use_dependency_validation = false;
        self::$_static_baseline_use_parameter_validation = false;
        self::$_static_baseline_use_parameter_validation_strict_checks = false;
        self::$_static_baseline_use_flag_validation = false;
        self::$_static_baseline_allow_flag_setting = true;
        self::$_static_baseline_allow_flag_unsetting = true;
        self::$_static_baseline_persist_parameters = false;
        self::$_static_baseline_persist_dependencies = false;
        self::$_static_baseline_persist_flags = false;
        self::$_static_baseline_dependencies = array();
        self::$_static_baseline_dependencies_required = array();
        self::$_static_baseline_dependencies_valid = array();
        self::$_static_baseline_dependencies_default = array();
        self::$_static_baseline_parameters = array();
        self::$_static_baseline_parameters_required = array();
        self::$_static_baseline_parameters_valid = array();
        self::$_static_baseline_parameters_default = array();
        self::$_static_baseline_flags = array();
        self::$_static_baseline_flags_required = array();
        self::$_static_baseline_flags_valid = array();
        self::$_static_baseline_flags_default = array();
        self::$_static_baseline_initialization_tasks = array();
        self::$_extendable_contract = null;
        self::$_extendable_context = null;
        self::$_extendable_extensions = null;
        self::$_static_control_api_indexes = array();
        self::$_static_control_api_cache = null;
        self::$_static_control_api_cache_compiled = false;
        self::$_static_control_api_method_blacklist = array();
        self::$_static_control_api_method_cache = array();
        self::$_static_control_api_customizations = array();
        self::$_static_control_api_extensions = array();
        self::$_static_control_api_reserved_indexes = array(
            'api',
            'register',
            'unregister',
            'check'
        );
        self::$_static_control_api_parser = null;
    }

}
