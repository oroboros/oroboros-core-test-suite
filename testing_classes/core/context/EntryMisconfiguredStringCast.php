<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\testclass\core\context;

/**
 * This class returns a misconfigured stringable result for testing error
 * handling within the __toString method of the context entry class.
 * @author Brian Dayhoff <mopsyd@me.com>
 */
class EntryMisconfiguredStringCast
    implements \oroboros\core\interfaces\contract\core\context\EntryContract
{

    use \oroboros\core\traits\core\context\EntryTrait;

    const OROBOROS_CLASS_TYPE = \oroboros\core\interfaces\enumerated\type\CoreClassTypes::CLASS_TYPE_CORE_CONTEXT;
    const OROBOROS_CLASS_SCOPE = \oroboros\core\interfaces\enumerated\scope\CoreClassScopes::CLASS_SCOPE_CORE_ABSTRACT;
    const OROBOROS_CLASS_CONTEXT = \oroboros\core\interfaces\enumerated\context\ClassContext::CLASS_CONTEXT_CORE;
    const OROBOROS_API = \oroboros\core\interfaces\enumerated\index\ApiIndex::INDEX_CORE_API;
    const OROBOROS_CORE = true;
    const OROBOROS_PATTERN = false;
    const OROBOROS_UTILITY = false;
    const OROBOROS_LIBRARY = false;
    const OROBOROS_ROUTINE = false;
    const OROBOROS_EXTENSION = false;
    const OROBOROS_MODULE = false;
    const OROBOROS_COMPONENT = false;
    const OROBOROS_ADAPTER = false;
    const OROBOROS_CONTROLLER = false;
    const OROBOROS_MODEL = false;
    const OROBOROS_VIEW = false;

    /**
     * Intentionally returns a non-stringable object to test error handling for __toString
     * @return \stdClass
     */
    protected function _getStringRepresentation()
    {
        return new \stdClass();
    }

}