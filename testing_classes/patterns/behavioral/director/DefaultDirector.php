<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\testclass\patterns\behavioral\director;

/**
 * <DefaultDirector>
 * A barebones, concrete director class. This is for
 * testing abstract functionality only.
 * @author Brian Dayhoff <mopsyd@me.com>
 */
class DefaultDirector
    extends \oroboros\testclass\patterns\behavioral\worker\DefaultWorker
    implements \oroboros\core\interfaces\contract\patterns\behavioral\DirectorContract
{

    use \oroboros\core\traits\patterns\behavioral\DirectorTrait;

    const OROBOROS_CLASS_SCOPE = 'director-test';

    public function __construct()
    {
        parent::__construct();
        $this->_director_category = null;
        $this->_worker_category = null;
        $this->_worker_scope = null;
    }

    public function setTestParameters()
    {
        $this->_setDirectorCategory( 'unit-test' );
        parent::setTestParameters();
    }

    public function getDefaultWorker()
    {
        $worker = new \oroboros\testclass\patterns\behavioral\worker\DefaultWorker();
        $worker->setTestParameters();
        return $worker;
    }

    public function getOutOfScopeWorker()
    {
        $worker = new \oroboros\testclass\patterns\behavioral\worker\DefaultWorker();
        return $worker;
    }

    public function setBadDirectorCategory()
    {
        $this->_setDirectorCategory( new \stdClass() );
    }

}
