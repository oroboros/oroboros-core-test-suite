<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\testclass\patterns\behavioral\manager;

/**
 * <DefaultDirector>
 * A barebones, concrete director class. This is for
 * testing abstract functionality only.
 * @author Brian Dayhoff <mopsyd@me.com>
 */
class DefaultManager
    extends \oroboros\testclass\patterns\behavioral\director\DefaultDirector
    implements \oroboros\core\interfaces\contract\patterns\behavioral\ManagerContract
{

    use \oroboros\core\traits\patterns\behavioral\ManagerTrait
    {
        \oroboros\core\traits\patterns\behavioral\ManagerTrait::__construct as private manager_constructor;
    }

    const OROBOROS_CLASS_SCOPE = 'manager-test';

    public function __construct( $params = null, $dependencies = null,
        $flags = null )
    {
        $this->manager_constructor( $params, $dependencies, $flags );
        $this->_baselineEnableParameterValidation();
        parent::__construct( $params, $dependencies, $flags );
        $this->_director_category = null;
        $this->_worker_category = null;
        $this->_worker_scope = null;
    }

    public function setTestParameters()
    {
        parent::setTestParameters();
        $this->_setWorkerScope( $this::OROBOROS_CLASS_SCOPE );
    }

    public function getDependencies()
    {
        return $this->_getDependencies();
    }

    public function getSettings()
    {
        return $this->_baseline_parameters;
    }

    public function checkSetting( $setting, $value )
    {
        return $this->_checkSetting( $setting, $value );
    }

    protected function _managerSetDirectorCategory()
    {
        $this->_setDirectorCategory( $this::OROBOROS_CLASS_SCOPE );
    }

    protected function _baselineSetParametersValid()
    {
        return array(
            'foo' => 'string' );
    }

    protected function _baselineSetParametersRequired()
    {
        return array(
            'foo' );
    }

    protected function _baselineSetParametersDefault()
    {
        return array(
            'foo' => 'bar' );
    }

}
