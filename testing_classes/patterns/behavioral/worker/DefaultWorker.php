<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\testclass\patterns\behavioral\worker;

/**
 * <DefaultWorker>
 * A barebones, concrete worker class. This is for
 * testing abstract functionality only.
 * @author Brian Dayhoff <mopsyd@me.com>
 */
class DefaultWorker
    extends \oroboros\testclass\DefaultStaticBaselineClass
    implements \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract
{

    use \oroboros\core\traits\patterns\behavioral\WorkerTrait
    {
        \oroboros\core\traits\patterns\behavioral\WorkerTrait::__construct as private worker_constructor;
    }

    const OROBOROS_CLASS_TYPE = 'unit-test';
    const OROBOROS_CLASS_SCOPE = 'worker-test';

    public function __construct()
    {
        parent::__construct();
        $this->worker_constructor();
        $this->_worker_category = null;
        $this->_worker_scope = null;
    }

    public function setTestParameters()
    {
        $this->_setWorkerCategory( $this::OROBOROS_CLASS_TYPE );
        $this->_setWorkerScope( $this::OROBOROS_CLASS_SCOPE );
    }

    public function setBadCategory()
    {
        $this->_setWorkerCategory( new \stdClass() );
    }

    public function setBadScope()
    {
        $this->_setWorkerScope( new \stdClass() );
    }

}
