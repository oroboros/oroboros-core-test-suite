<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\testclass;

/**
 * Description of DocBlockParseTestClass
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
class DocBlockParseTestClass
    extends \oroboros\core\abstracts\patterns\structural\AbstractStaticControlApi
{

    const TEST_CONST = 'testconst';

    private $_test_property = 'testproperty';

    /**
     * This is a test constructor
     */
    public function __construct()
    {
        self::_registerStaticControlApiIndex( 'foo' );
        self::_registerStaticControlApiIndex( 'bar' );
        self::_registerStaticControlApiIndex( 'baz' );
        self::_registerStaticControlApiIndex( 'get' );
    }

    public function testPublic()
    {

    }

    public function testProtected()
    {

    }

    public function testPrivate()
    {

    }

    /**
     * This is a test doc block comment.
     *
     *
     * This is another comment in the test function.
     *
     *
     * @param type $foo Foo has a detailed comment.
     * The comment for foo takes several lines.
     * The parser should be able to reconstruct the comment
     * for foo without issue, although it might indent
     * the extra lines to make it prettier.
     * @param type $bar Bar also has a detailed comment.
     * It's not as long as the comment for foo, but it's pretty long.
     * @param \stdClass $baz This is a comment about baz!
     * @return void Void is the last thing we want to return.
     * Void has a long comment too.
     */
    protected static function fooMatch( $foo, $bar, \stdClass $baz )
    {

    }

    protected static function _fooMatchTwo()
    {

    }

    protected static function barMatch()
    {

    }

    protected static function _barMatchTwo()
    {

    }

    protected static function __barNoMatch()
    {

    }

    protected static function _bazMatch()
    {

    }

    protected static function bazMatch0()
    {

    }

}