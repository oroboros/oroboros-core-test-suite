<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\patterns\behavioral;

/**
 * <Worker Pattern Test Cases>
 * These tests prove the stable compliance of worker patterns.
 * A worker is a default class that receives commands from a director.
 * @group patterns
 * @group behavioral
 * @group worker
 * @covers \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract
 * @covers \oroboros\core\traits\patterns\behavioral\WorkerTrait
 * @covers \oroboros\core\traits\core\BaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\collection\traits\CollectionTrait
 * @covers \oroboros\collection\traits\ContainerTrait
 * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
 * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
 */
class WorkerTest
    extends \oroboros\tests\AbstractTestClass
{

    const TEST_CLASS = '\\oroboros\\testclass\\patterns\\behavioral\\worker\\DefaultWorker';

    protected $expected_director_category = false;
    protected $expected_category = 'unit-test';
    protected $expected_scope = 'worker-test';
    protected $expected_worker_scope = 'worker-test';

    /**
     * @group patterns
     * @group behavioral
     * @group worker
     * @covers \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract
     * @covers \oroboros\core\traits\patterns\behavioral\WorkerTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testWorkerValid()
    {
        $test = $this->getTestClass();
        try
        {
            $test->setBadCategory();
        } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
        {

        }
        try
        {
            $test->setBadScope();
        } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
        {

        }
        $test->setTestParameters();
        $this->assertEquals( $this->expected_category,
            $test->getWorkerCategory(),
            'Worker category should by default be defined as the workers class constant OROBOROS_CLASS_TYPE, or false if that is not defined.' );
        $this->assertEquals( $this->expected_scope, $test->getWorkerScope(),
            'Worker scope should by default be defined as the workers class constant OROBOROS_CLASS_SCOPE, or false if that is not defined.' );
        $this->assertTrue( true );
    }

}
