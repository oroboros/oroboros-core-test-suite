<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\standards\psr\psr7;

/**
 * <Psr7 Uri Test Cases>
 * These tests prove the stable compliance of Psr-7 uris.
 * @group standards
 * @group psr
 * @group psr7
 * @group uri
 * @covers \oroboros\message\Uri
 * @covers \oroboros\message\abstracts\AbstractUri
 * @covers \oroboros\message\traits\UriTrait
 * @covers \oroboros\message\interfaces\contract\UriContract
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 * @covers \oroboros\core\traits\core\BaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\collection\traits\CollectionTrait
 * @covers \oroboros\collection\traits\ContainerTrait
 * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
 * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
 */
class UriTest
    extends \oroboros\tests\AbstractTestClass
{

    const CONCRETE_CLASS = '\\oroboros\\message\\Uri';

    /**
     * <Accept Cases>
     * These should always assert true.
     */

    /**
     * This SHOULD assert true.
     * @group standards
     * @group psr
     * @group psr7
     * @group uri
     * @covers \oroboros\message\Uri
     * @covers \oroboros\message\abstracts\AbstractUri
     * @covers \oroboros\message\traits\UriTrait
     * @covers \oroboros\message\interfaces\contract\UriContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testUriValid()
    {
        $class = $this::CONCRETE_CLASS;
        $uri = $this->_getDefaultObject();
        $this->assertTrue( $uri instanceof $class );
    }

    /**
     * This SHOULD assert true.
     * @group standards
     * @group psr
     * @group psr7
     * @group uri
     * @covers \oroboros\message\Uri
     * @covers \oroboros\message\abstracts\AbstractUri
     * @covers \oroboros\message\traits\UriTrait
     * @covers \oroboros\message\interfaces\contract\UriContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testUriString()
    {
        $uri = $this->_getDefaultObject();
        $expected = $this->_getDefaultParams()['uri'];
        $this->assertTrue( (string) $uri === $this->_getDefaultParams()['uri'] );
    }

    /**
     * This SHOULD assert true.
     * @group standards
     * @group psr
     * @group psr7
     * @group uri
     * @covers \oroboros\message\Uri
     * @covers \oroboros\message\abstracts\AbstractUri
     * @covers \oroboros\message\traits\UriTrait
     * @covers \oroboros\message\interfaces\contract\UriContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testUriDefaults()
    {
        $uri = $this->_getDefaultObject();
        $this->assertTrue( $uri->getScheme() === 'http' );
        $this->assertTrue( $uri->withScheme( 'https' )->getScheme() === 'https' );
        $this->assertTrue( $uri->getAuthority() === 'username:password@example.com:80' );
        $this->assertTrue( $uri->withPort( 8080 )->getPort() === 8080 );
        $this->assertTrue( $uri->withUserInfo( 'foo', 'bar' )->getUserInfo() ===
            'foo:bar' );
        $this->assertTrue( $uri->withUserInfo( 'foo' )->getUserInfo() === 'foo' );
        $this->assertTrue( $uri->getHost() === 'example.com' );
        $this->assertTrue( $uri->withHost( 'foo.com' )->getHost() === 'foo.com' );
        $this->assertTrue( $uri->getPath() === '/foo/bar/baz' );
        $this->assertTrue( $uri->withPath( '/bar/baz/quux' )->getPath() === '/bar/baz/quux' );
        $this->assertTrue( $uri->getQuery() === 'foo=bar&baz=quux&foobar&bazquux' );
        $this->assertTrue( $uri->withQuery( 'foo=bar' )->getQuery() === 'foo=bar' );
        $this->assertTrue( $uri->getFragment() === 'foobarbaz' );
        $this->assertTrue( $uri->withFragment( 'bazquux' )->getFragment() === 'bazquux' );
    }

    /**
     * Fetches the default initialization parameters
     * @return array
     */
    protected function _getDefaultParams()
    {
        $params = array(
            'uri' => 'http://username:password@example.com:80/foo/bar/baz?foo=bar&baz=quux&foobar&bazquux#foobarbaz'
        );
        return $params;
    }

    /**
     * Fetches the default test object
     * @return instanceof self::CONCRETE_CLASS
     */
    protected function _getDefaultObject()
    {
        $class = $this::CONCRETE_CLASS;
        $params = $this->_getDefaultParams();
        return new $class( $params['uri'] );
    }

}
