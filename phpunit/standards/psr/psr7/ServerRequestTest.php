<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\standards\psr\psr7;

/**
 * <Psr7 Server Request Stream Test Cases>
 * These tests prove the stable compliance of Psr-7 server request streams.
 * @group standards
 * @group psr
 * @group psr7
 * @group stream
 * @group request
 * @covers \oroboros\message\Stream
 * @covers \oroboros\message\abstracts\AbstractStream
 * @covers \oroboros\message\traits\StreamTrait
 * @covers \oroboros\message\interfaces\contract\StreamContract
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 * @covers \oroboros\core\traits\core\BaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\collection\traits\CollectionTrait
 * @covers \oroboros\collection\traits\ContainerTrait
 * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
 * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
 */
class ServerRequestTest
    extends RequestTest
{

    const CONCRETE_CLASS = '\\oroboros\\message\\ServerRequest';

    /**
     * Represents a serialized set of superglobals from an http request that
     * have enough parameters to test all request methods.
     * The files will not be present on the system, so some
     * additional testing of files may be required beyond this.
     * @var array
     */
    protected $_test_parameters = array(
        //Represents an http $_SERVER superglobal in serialized format.
        'params' => 'a:37:{s:9:"HTTP_HOST";s:19:"core.oroboros.local";s:15:"HTTP_CONNECTION";s:10:"keep-alive";s:14:"CONTENT_LENGTH";s:5:"67482";s:18:"HTTP_CACHE_CONTROL";s:9:"max-age=0";s:11:"HTTP_ORIGIN";s:26:"http://core.oroboros.local";s:30:"HTTP_UPGRADE_INSECURE_REQUESTS";s:1:"1";s:15:"HTTP_USER_AGENT";s:137:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36 OPR/49.0.2725.39";s:12:"CONTENT_TYPE";s:68:"multipart/form-data; boundary=----WebKitFormBoundaryfvK729qzHLhmB2Eo";s:11:"HTTP_ACCEPT";s:85:"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";s:8:"HTTP_DNT";s:1:"1";s:12:"HTTP_REFERER";s:47:"http://core.oroboros.local/test_upload_form.php";s:20:"HTTP_ACCEPT_ENCODING";s:13:"gzip, deflate";s:20:"HTTP_ACCEPT_LANGUAGE";s:14:"en-US,en;q=0.9";s:11:"HTTP_COOKIE";s:74:"PHPSESSID=1tuiaf9n6rl597iong8s1v1la5; test_cookie_1=foo; test_cookie_2=bar";s:4:"PATH";s:29:"/usr/bin:/bin:/usr/sbin:/sbin";s:16:"SERVER_SIGNATURE";s:0:"";s:15:"SERVER_SOFTWARE";s:31:"Apache/2.4.29 (Unix) PHP/7.0.24";s:11:"SERVER_NAME";s:19:"core.oroboros.local";s:11:"SERVER_ADDR";s:3:"::1";s:11:"SERVER_PORT";s:2:"80";s:11:"REMOTE_ADDR";s:3:"::1";s:13:"DOCUMENT_ROOT";s:90:"/Users/briandayhoff/Sites/sites/oroboros/Core/Oroboros-Core-Architecture/tests/development";s:14:"REQUEST_SCHEME";s:4:"http";s:14:"CONTEXT_PREFIX";s:0:"";s:21:"CONTEXT_DOCUMENT_ROOT";s:90:"/Users/briandayhoff/Sites/sites/oroboros/Core/Oroboros-Core-Architecture/tests/development";s:12:"SERVER_ADMIN";s:13:"mopsyd@me.com";s:15:"SCRIPT_FILENAME";s:113:"/Users/briandayhoff/Sites/sites/oroboros/Core/Oroboros-Core-Architecture/tests/development/test_serverrequest.php";s:11:"REMOTE_PORT";s:5:"53204";s:17:"GATEWAY_INTERFACE";s:7:"CGI/1.1";s:15:"SERVER_PROTOCOL";s:8:"HTTP/1.1";s:14:"REQUEST_METHOD";s:4:"POST";s:12:"QUERY_STRING";s:20:"foo=bar&bar=baz&quux";s:11:"REQUEST_URI";s:44:"/test_serverrequest.php?foo=bar&bar=baz&quux";s:11:"SCRIPT_NAME";s:23:"/test_serverrequest.php";s:8:"PHP_SELF";s:23:"/test_serverrequest.php";s:18:"REQUEST_TIME_FLOAT";d:1512214868.0239999;s:12:"REQUEST_TIME";i:1512214868;}',
        //Represents an http $_COOKIES superglobal in serialized format.
        'cookies' => 'a:3:{s:9:"PHPSESSID";s:26:"1tuiaf9n6rl597iong8s1v1la5";s:13:"test_cookie_1";s:3:"foo";s:13:"test_cookie_2";s:3:"bar";}',
        //Represents an http $_GET superglobal in serialized format.
        'query' => 'a:3:{s:3:"foo";s:3:"bar";s:3:"bar";s:3:"baz";s:4:"quux";s:0:"";}',
        //Represents an http $_FILES superglobal in serialized format.
        'files' => 'a:5:{s:15:"filesToUpload_0";a:5:{s:4:"name";s:14:"logo_black.png";s:4:"type";s:9:"image/png";s:8:"tmp_name";s:26:"/private/var/tmp/phpYGrLFQ";s:5:"error";i:0;s:4:"size";i:6042;}s:15:"filesToUpload_1";a:5:{s:4:"name";a:1:{s:3:"foo";a:1:{s:3:"bar";a:1:{i:0;s:13:"logo_blue.png";}}}s:4:"type";a:1:{s:3:"foo";a:1:{s:3:"bar";a:1:{i:0;s:9:"image/png";}}}s:8:"tmp_name";a:1:{s:3:"foo";a:1:{s:3:"bar";a:1:{i:0;s:26:"/private/var/tmp/phprTR8er";}}}s:5:"error";a:1:{s:3:"foo";a:1:{s:3:"bar";a:1:{i:0;i:0;}}}s:4:"size";a:1:{s:3:"foo";a:1:{s:3:"bar";a:1:{i:0;i:9197;}}}}s:15:"filesToUpload_2";a:5:{s:4:"name";a:1:{s:3:"foo";a:1:{s:3:"bar";a:1:{i:0;s:14:"logo_green.png";}}}s:4:"type";a:1:{s:3:"foo";a:1:{s:3:"bar";a:1:{i:0;s:9:"image/png";}}}s:8:"tmp_name";a:1:{s:3:"foo";a:1:{s:3:"bar";a:1:{i:0;s:26:"/private/var/tmp/phpbrPppu";}}}s:5:"error";a:1:{s:3:"foo";a:1:{s:3:"bar";a:1:{i:0;i:0;}}}s:4:"size";a:1:{s:3:"foo";a:1:{s:3:"bar";a:1:{i:0;i:9075;}}}}s:15:"filesToUpload_3";a:5:{s:4:"name";a:1:{s:3:"foo";a:1:{s:3:"bar";s:15:"logo_orange.png";}}s:4:"type";a:1:{s:3:"foo";a:1:{s:3:"bar";s:9:"image/png";}}s:8:"tmp_name";a:1:{s:3:"foo";a:1:{s:3:"bar";s:26:"/private/var/tmp/phpKIjrvk";}}s:5:"error";a:1:{s:3:"foo";a:1:{s:3:"bar";i:0;}}s:4:"size";a:1:{s:3:"foo";a:1:{s:3:"bar";i:9111;}}}s:15:"filesToUpload_4";a:5:{s:4:"name";a:1:{s:3:"foo";a:1:{s:3:"bar";a:4:{i:0;s:15:"logo_purple.png";i:1;s:12:"logo_red.png";i:2;s:14:"logo_white.png";i:3;s:15:"logo_yellow.png";}}}s:4:"type";a:1:{s:3:"foo";a:1:{s:3:"bar";a:4:{i:0;s:9:"image/png";i:1;s:9:"image/png";i:2;s:9:"image/png";i:3;s:9:"image/png";}}}s:8:"tmp_name";a:1:{s:3:"foo";a:1:{s:3:"bar";a:4:{i:0;s:26:"/private/var/tmp/php7TuYxN";i:1;s:26:"/private/var/tmp/phpJO2KRI";i:2;s:26:"/private/var/tmp/phpoMQasF";i:3;s:26:"/private/var/tmp/phppzPQB1";}}}s:5:"error";a:1:{s:3:"foo";a:1:{s:3:"bar";a:4:{i:0;i:0;i:1;i:0;i:2;i:0;i:3;i:0;}}}s:4:"size";a:1:{s:3:"foo";a:1:{s:3:"bar";a:4:{i:0;i:9149;i:1;i:8972;i:2;i:5650;i:3;i:8823;}}}}}',
        //Represents an http $_POST superglobal in serialized format.
        'parsed' => 'a:1:{s:6:"submit";s:12:"Upload Image";}',
        //Represents an http $_SESSION superglobal in serialized format.
        'attributes' => 'a:1:{s:3:"foo";s:11:"session_bar";}'
    );

    /**
     * This SHOULD assert true.
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @group request
     * @covers \oroboros\message\Stream
     * @covers \oroboros\message\abstracts\AbstractStream
     * @covers \oroboros\message\traits\StreamTrait
     * @covers \oroboros\message\interfaces\contract\StreamContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testMessageDefaults()
    {
        $message = $this->_getDefaultObject();
        $this->assertTrue( $message->getProtocolVersion() === '1.1' );
        $this->assertTrue( $message->getBody() instanceof \Psr\Http\Message\StreamInterface );
        $this->assertTrue( (string) $message->getBody() === (string) $message );
    }

    /**
     * This SHOULD assert true.
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @group request
     * @covers \oroboros\message\ServerRequest
     * @covers \oroboros\message\abstracts\AbstractServerRequest
     * @covers \oroboros\message\traits\ServerRequestTrait
     * @covers \oroboros\message\interfaces\contract\ServerRequestContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testServerRequestDefaults()
    {
        $request = $this->_getDefaultObject();
        //Everything exept 'files' should be able to reverse-serialize and wind up with the same result.
        $this->assertTrue( serialize( $request->getAttributes() ) === $this->_test_parameters['attributes'] );
        $this->assertTrue( serialize( $request->getCookieParams() ) === $this->_test_parameters['cookies'] );
        $this->assertTrue( serialize( $request->getParsedBody() ) === $this->_test_parameters['parsed'] );
        $this->assertTrue( serialize( $request->getQueryParams() ) === $this->_test_parameters['query'] );
        $this->assertTrue( serialize( $request->getServerParams() ) === $this->_test_parameters['params'] );
    }

    /**
     * This SHOULD assert true.
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @group request
     * @covers \oroboros\message\ServerRequest
     * @covers \oroboros\message\abstracts\AbstractServerRequest
     * @covers \oroboros\message\traits\ServerRequestTrait
     * @covers \oroboros\message\interfaces\contract\ServerRequestContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testServerRequestQuery()
    {
        $request = $this->_getDefaultObject();
        $new_query = array(
            'foo' => 'bar',
            'baz' => 'quux'
        );
        $this->assertTrue( $request->withQueryParams( $new_query )->getQueryParams()
            === $new_query );
    }

    /**
     * This SHOULD assert true.
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @group request
     * @covers \oroboros\message\ServerRequest
     * @covers \oroboros\message\abstracts\AbstractServerRequest
     * @covers \oroboros\message\traits\ServerRequestTrait
     * @covers \oroboros\message\interfaces\contract\ServerRequestContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testServerRequestParsed()
    {
        $request = $this->_getDefaultObject();
        $new_parsed = array(
            'foo' => 'bar',
            'baz' => 'quux'
        );
        $this->assertTrue( $request->withParsedBody( $new_parsed )->getParsedBody()
            === $new_parsed );
    }

    /**
     * This SHOULD assert true.
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @group request
     * @covers \oroboros\message\ServerRequest
     * @covers \oroboros\message\abstracts\AbstractServerRequest
     * @covers \oroboros\message\traits\ServerRequestTrait
     * @covers \oroboros\message\interfaces\contract\ServerRequestContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testServerRequestAttributes()
    {
        $request = $this->_getDefaultObject();
        $this->assertTrue( $request->withAttribute( 'foo', 'bar' )->getAttribute( 'foo' )
            === 'bar' );
        $this->assertFalse( $request->withAttribute( 'foo', 'bar' )->getAttribute( 'bar' )
            === 'bar' );
        $this->assertTrue( $request->withAttribute( 'foo', 'bar' )->getAttribute( 'bar' )
            === null );
        $this->assertTrue( $request->withAttribute( 'foo', 'bar' )->getAttribute( 'bar',
                'bar' ) === 'bar' );
        $this->assertTrue( $request->withoutAttribute( 'foo' )->getAttribute( 'foo' )
            === null );
    }

    /**
     * This SHOULD assert true.
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @group request
     * @covers \oroboros\message\ServerRequest
     * @covers \oroboros\message\abstracts\AbstractServerRequest
     * @covers \oroboros\message\traits\ServerRequestTrait
     * @covers \oroboros\message\interfaces\contract\ServerRequestContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testServerRequestFiles()
    {
        $request = $this->_getDefaultObject();
        $default_files = $request->getUploadedFiles();
        $this->assertTrue( is_array( $request->getUploadedFiles() ) );
        //checks parse results from single form upload
        $this->assertTrue( array_key_exists( 'filesToUpload_0', $default_files ) &&
            ( $default_files['filesToUpload_0'] instanceof \Psr\Http\Message\UploadedFileInterface ) );
        //checks parse results from namespaced form upload
        $this->assertTrue( array_key_exists( 'filesToUpload_1', $default_files )
            && array_key_exists( 'foo', $default_files['filesToUpload_1'] )
            && array_key_exists( 'bar', $default_files['filesToUpload_1']['foo'] )
            && array_key_exists( 0,
                $default_files['filesToUpload_1']['foo']['bar'] )
            && ( $default_files['filesToUpload_1']['foo']['bar'][0] instanceof \Psr\Http\Message\UploadedFileInterface ) );
        //checks parse results from alternate format namespaced form upload
        $this->assertTrue( array_key_exists( 'filesToUpload_2', $default_files )
            && array_key_exists( 'foo', $default_files['filesToUpload_2'] )
            && array_key_exists( 'bar', $default_files['filesToUpload_2']['foo'] )
            && array_key_exists( 0,
                $default_files['filesToUpload_2']['foo']['bar'] )
            && ( $default_files['filesToUpload_2']['foo']['bar'][0] instanceof \Psr\Http\Message\UploadedFileInterface ) );
        //checks parse results from alternate format namespaced form upload with named end key
        $this->assertTrue( array_key_exists( 'filesToUpload_3', $default_files )
            && array_key_exists( 'foo', $default_files['filesToUpload_3'] )
            && array_key_exists( 'bar', $default_files['filesToUpload_3']['foo'] )
            && array_key_exists( 'bar',
                $default_files['filesToUpload_3']['foo']['bar'] )
            && ( $default_files['filesToUpload_3']['foo']['bar']['bar'] instanceof \Psr\Http\Message\UploadedFileInterface ) );
        //checks parse results from namespaced form upload with multi-upload enabled. There should be four of these.
        $this->assertTrue( array_key_exists( 'filesToUpload_4', $default_files )
            && array_key_exists( 'foo', $default_files['filesToUpload_4'] )
            && array_key_exists( 'bar', $default_files['filesToUpload_4']['foo'] )
            && count( $default_files['filesToUpload_4']['foo']['bar'] ) === 4
            && (!in_array( false,
                array_map( function($file)
                {
                    return $file instanceof \Psr\Http\Message\UploadedFileInterface;
                }, $default_files['filesToUpload_4']['foo']['bar'] ) ) ) );
    }

    /**
     * Fetches the default initialization parameters
     * @return array
     */
    protected function _getDefaultParams()
    {
        $params = $this->_test_parameters;
        foreach ( $params as
            $key =>
            $value )
        {
            $params[$key] = unserialize( $value );
        }
        return $params;
    }

    /**
     * Fetches the default test object
     * @return \Psr\Http\Message\ServerRequestInterface
     */
    protected function _getDefaultObject()
    {
        $class = $this::CONCRETE_CLASS;
        $params = $this->_getDefaultParams();
        return new $class( null, null, null, null, null, null,
            $params['params'], $params['cookies'], $params['query'],
            $params['files'], $params['parsed'], $params['attributes'] );
    }

}
