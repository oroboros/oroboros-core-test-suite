<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\standards\psr\psr7;

/**
 * <Psr7 Request Stream Test Cases>
 * These tests prove the stable compliance of Psr-7 request streams.
 * @group standards
 * @group psr
 * @group psr7
 * @group stream
 * @group request
 * @group message
 * @covers \oroboros\message\Request
 * @covers \oroboros\message\abstracts\AbstractRequest
 * @covers \oroboros\message\traits\RequestTrait
 * @covers \oroboros\message\interfaces\contract\RequestContract
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 * @covers \oroboros\core\traits\core\BaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\collection\traits\CollectionTrait
 * @covers \oroboros\collection\traits\ContainerTrait
 * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
 * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
 */
class RequestTest
    extends MessageTest
{

    const CONCRETE_CLASS = '\\oroboros\\message\\Request';

    private $_request_methods_valid = array(
        'GET',
        'POST',
        'PUT',
        'DELETE',
        'OPTIONS',
        'TRACE',
        'HEAD',
        'CONNECT',
        'PATCH' );

    /**
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @group request
     * @group message
     * @covers \oroboros\message\Request
     * @covers \oroboros\message\abstracts\AbstractRequest
     * @covers \oroboros\message\traits\RequestTrait
     * @covers \oroboros\message\interfaces\contract\RequestContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testRequestDefaults()
    {
        $request = $this->_getDefaultObject();
        $this->assertTrue( in_array( $request->getMethod(),
                $this->_request_methods_valid ) );
        $this->assertTrue( $request->getUri() instanceof \Psr\Http\Message\UriInterface );
        $this->assertTrue( $request->getBody() instanceof \Psr\Http\Message\StreamInterface );
    }

    /**
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @group request
     * @group message
     * @covers \oroboros\message\Request
     * @covers \oroboros\message\abstracts\AbstractRequest
     * @covers \oroboros\message\traits\RequestTrait
     * @covers \oroboros\message\interfaces\contract\RequestContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testRequestTarget()
    {
        $request = $this->_getDefaultObject();
        $this->assertTrue( $request->withRequestTarget( 'http://example.com/' )->getRequestTarget()
            === 'http://example.com/'
        );
    }

    /**
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @group request
     * @group message
     * @covers \oroboros\message\Request
     * @covers \oroboros\message\abstracts\AbstractRequest
     * @covers \oroboros\message\traits\RequestTrait
     * @covers \oroboros\message\interfaces\contract\RequestContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testRequestUri()
    {
        $request = $this->_getDefaultObject();
        $uri = new \oroboros\message\Uri( 'http://foo.example.com/' );
        $this->assertTrue( (string) $request->withUri( $uri )->getUri() === 'http://foo.example.com/' );
    }

    /**
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @group request
     * @group message
     * @covers \oroboros\message\Request
     * @covers \oroboros\message\abstracts\AbstractRequest
     * @covers \oroboros\message\traits\RequestTrait
     * @covers \oroboros\message\interfaces\contract\RequestContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testRequestMethod()
    {
        $request = $this->_getDefaultObject();
        $this->assertTrue( in_array( $request->withMethod( 'POST' )->getMethod(),
                $this->_request_methods_valid ) );
        $this->assertTrue( in_array( $request->withMethod( 'PUT' )->getMethod(),
                $this->_request_methods_valid ) );
        $this->assertTrue( in_array( $request->withMethod( 'DELETE' )->getMethod(),
                $this->_request_methods_valid ) );
        $this->assertTrue( in_array( $request->withMethod( 'OPTIONS' )->getMethod(),
                $this->_request_methods_valid ) );
        $this->assertTrue( in_array( $request->withMethod( 'TRACE' )->getMethod(),
                $this->_request_methods_valid ) );
        $this->assertTrue( in_array( $request->withMethod( 'HEAD' )->getMethod(),
                $this->_request_methods_valid ) );
        $this->assertTrue( in_array( $request->withMethod( 'CONNECT' )->getMethod(),
                $this->_request_methods_valid ) );
        $this->assertTrue( in_array( $request->withMethod( 'PATCH' )->getMethod(),
                $this->_request_methods_valid ) );
    }

    /**
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @group request
     * @group message
     * @covers \oroboros\message\Stream
     * @covers \oroboros\message\abstracts\AbstractStream
     * @covers \oroboros\message\traits\StreamTrait
     * @covers \oroboros\message\interfaces\contract\StreamContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testMessageDefaults()
    {
        $message = $this->_getDefaultObject();
        $this->assertTrue( $message->getProtocolVersion() === '1.1' );
        $this->assertTrue( $message->getBody() instanceof \Psr\Http\Message\StreamInterface );
        $this->assertTrue( (string) $message->getBody() === 'Test stream content.' );
        $this->assertTrue( (string) $message->getBody() === (string) $message );
    }

    /**
     * Fetches the default initialization parameters
     * @return array
     */
    protected function _getDefaultParams()
    {
        $params = array(
            'stream' => file_get_contents( __DIR__ . '/streamfile.txt' ),
            'headers' => array(
                'content-type' => 'text/plain',
                'content-disposition' => 'attachment; filename=foobarzippityzang.txt',
                'test-header' => 'foo, bar, baz'
            ),
            'protocol' => '1.1',
            'host' => 'example.com',
            'uri' => 'http://example.com/foo/bar/baz',
            'method' => 'GET'
        );
        return $params;
    }

    /**
     * Fetches the default test object
     * @return \Psr\Http\Message\RequestInterface
     */
    protected function _getDefaultObject()
    {
        $class = $this::CONCRETE_CLASS;
        $params = $this->_getDefaultParams();
        return new $class( $params['stream'], $params['headers'],
            $params['protocol'], $params['host'], $params['uri'],
            $params['method'] );
    }

}
