<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\standards\psr\psr7;

/**
 * <Psr7 Message Stream Test Cases>
 * These tests prove the stable compliance of Psr-7 message streams.
 * @group standards
 * @group psr
 * @group psr7
 * @group stream
 * @group message
 * @covers \oroboros\message\Message
 * @covers \oroboros\message\abstracts\AbstractMessage
 * @covers \oroboros\message\traits\MessageTrait
 * @covers \oroboros\message\interfaces\contract\MessageContract
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 * @covers \oroboros\core\traits\core\BaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\collection\traits\CollectionTrait
 * @covers \oroboros\collection\traits\ContainerTrait
 * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
 * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
 */
class MessageTest
    extends StreamTest
{

    const CONCRETE_CLASS = '\\oroboros\\message\\Message';

    /**
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @group message
     * @covers \oroboros\message\Message
     * @covers \oroboros\message\abstracts\AbstractMessage
     * @covers \oroboros\message\traits\MessageTrait
     * @covers \oroboros\message\interfaces\contract\MessageContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testMessageDefaults()
    {
        $message = $this->_getDefaultObject();
        $this->assertTrue( $message->getProtocolVersion() === '1.1' );
        $this->assertTrue( $message->getHeaders() === array(
            'content-type' => array(
                'text/plain'
            ),
            'content-disposition' => array(
                'attachment; filename=foobarzippityzang.txt'
            ),
            'test-header' => array(
                'foo',
                'bar',
                'baz'
            )
            )
        );
        $this->assertTrue( $message->getBody() instanceof \Psr\Http\Message\StreamInterface );
        $this->assertTrue( (string) $message->getBody() === 'Test stream content.' );
        $this->assertTrue( (string) $message->getBody() === (string) $message );
    }

    /**
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @group message
     * @covers \oroboros\message\Message
     * @covers \oroboros\message\abstracts\AbstractMessage
     * @covers \oroboros\message\traits\MessageTrait
     * @covers \oroboros\message\interfaces\contract\MessageContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testMessageHeaders()
    {
        $message = $this->_getDefaultObject();
        $this->assertTrue( $message->withAddedHeader( 'foo', 'bar' )->hasHeader( 'foo' ) );
        $this->assertFalse( $message->withAddedHeader( 'foo', 'bar' )->withoutHeader( 'foo' )->hasHeader( 'foo' ) );
    }

    /**
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @group message
     * @covers \oroboros\message\Message
     * @covers \oroboros\message\abstracts\AbstractMessage
     * @covers \oroboros\message\traits\MessageTrait
     * @covers \oroboros\message\interfaces\contract\MessageContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testProtocol()
    {
        $message = $this->_getDefaultObject();
        $this->assertTrue( $message->withProtocolVersion( '1.0' )->getProtocolVersion()
            === '1.0' );
    }

    /**
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @group message
     * @covers \oroboros\message\Message
     * @covers \oroboros\message\abstracts\AbstractMessage
     * @covers \oroboros\message\traits\MessageTrait
     * @covers \oroboros\message\interfaces\contract\MessageContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testStreamToString()
    {
        $stream = $this->_getDefaultObject();
        $stream->rewind();
        $this->assertTrue( is_string( $stream->getContents() ) );
        $this->assertTrue( is_string( (string) $stream ) );
    }

    /**
     * Fetches the default initialization parameters
     * @return array
     */
    protected function _getDefaultParams()
    {
        $params = array(
            'stream' => file_get_contents( __DIR__ . '/streamfile.txt' ),
            'headers' => array(
                'content-type' => 'text/plain',
                'content-disposition' => 'attachment; filename=foobarzippityzang.txt',
                'test-header' => 'foo, bar, baz'
            ),
            'protocol' => '1.1'
        );
        return $params;
    }

    /**
     * Fetches the default test object
     * @return \Psr\Http\Message\MessageInterface
     */
    protected function _getDefaultObject()
    {
        $class = $this::CONCRETE_CLASS;
        $params = $this->_getDefaultParams();
        return new $class( $params['stream'], $params['headers'],
            $params['protocol'] );
    }

}
