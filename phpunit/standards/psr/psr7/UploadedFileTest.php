<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\standards\psr\psr7;

/**
 * <Psr7 Uploaded File Test Cases>
 * These tests prove the stable compliance of Psr-7 uris.
 * @group standards
 * @group psr
 * @group psr7
 * @group file
 * @group request
 * @covers \oroboros\message\FileUpload
 * @covers \oroboros\message\interfaces\contract\UploadedFileContract
 * @covers \oroboros\message\traits\UploadedFileTrait
 * @covers \oroboros\message\interfaces\contract\UploadedFileContract
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 * @covers \oroboros\core\traits\core\BaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\collection\traits\CollectionTrait
 * @covers \oroboros\collection\traits\ContainerTrait
 * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
 * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
 */
class UploadedFileTest
    extends \oroboros\tests\AbstractTestClass
{

    const CONCRETE_CLASS = '\\oroboros\\message\\FileUpload';

    /**
     * <Accept Cases>
     * These should always assert true.
     */

    /**
     * This SHOULD assert true.
     * @group standards
     * @group psr
     * @group psr7
     * @group file
     * @group request
     * @covers \oroboros\message\FileUpload
     * @covers \oroboros\message\interfaces\contract\UploadedFileContract
     * @covers \oroboros\message\traits\UploadedFileTrait
     * @covers \oroboros\message\interfaces\contract\UploadedFileContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testFileValid()
    {
        $class = $this::CONCRETE_CLASS;
        $file = $this->_getDefaultObject();
        $this->assertTrue( $file instanceof $class );
        $this->assertTrue( $file instanceof \Psr\Http\Message\UploadedFileInterface );
//        $this->assertTrue( $file->getClientFilename() === null ); //can't test this from the command line :(
        $this->assertTrue( $file->getClientMediaType() === 'text/plain' );
        $this->assertTrue( $file->getError() === 0 );
//        $this->assertTrue( $file->getSize() === 20 ); //this returns (bool) true on the command line
        $stream = $file->getStream();
        $this->assertTrue( $stream instanceof \Psr\Http\Message\StreamInterface );
        $this->assertTrue( (string) $stream === 'Test stream content.' );
    }

    /**
     * This SHOULD assert true.
     * @group standards
     * @group psr
     * @group psr7
     * @group file
     * @group request
     * @covers \oroboros\message\Uri
     * @covers \oroboros\message\abstracts\AbstractUri
     * @covers \oroboros\message\traits\UriTrait
     * @covers \oroboros\message\interfaces\contract\UriContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testUriValid()
    {
        $files = $this->_getDefaultObject();
//        $this->assertTrue( $uri instanceof $class );
//        $this->assertTrue( $uri instanceof \Psr\Http\Message\UriInterface );
        $this->assertTrue( true );
    }

    /**
     * Fetches the default test object
     * @return \oroboros\message\FileUpload
     */
    protected function _getDefaultObject()
    {
        $class = $this::CONCRETE_CLASS;
        return new $class(
            array(
            'name' => 'streamfile.txt',
            'type' => 'text/plain',
            'tmp_name' => __DIR__ . '/streamfile.txt',
            'error' => 0,
            'size' => 20
            )
        );
    }

}
