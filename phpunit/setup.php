<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Baseline Unit Testing Setup>
 * This file will set up the baseline unit testing environment,
 * and insure that errors are properly tracked while tests are running.
 */
//All errors are shown
error_reporting( E_ALL );

//Initialize Oroboros
require_once realpath( dirname( __FILE__ ) )
    . DIRECTORY_SEPARATOR . '..'
    . DIRECTORY_SEPARATOR . '..'
    . DIRECTORY_SEPARATOR . 'init.php';

/**
 * Load an autoloader to handle automatic inclusion of test classes.
 */
$autoloader = new \oroboros\autoload\Psr4();
$autoloader->register();
$autoloader->addNamespace( 'oroboros\\tests', __DIR__ );
$autoloader->addNamespace( 'oroboros\\testclass',
    __DIR__ . '/../testing_classes' );


/**
 * Register the Sentry error handler.
 */
if ( getenv( 'OROBOROS_RAVEN_CLIENT' ) )
{
    /**
     * Error tracking versioning
     */
    $sentry_version = 'ci-build-' . substr( getenv( 'CI_COMMIT_ID' ), 0, 7 );
    $sentry = new \Raven_Client( getenv( 'OROBOROS_RAVEN_CLIENT' ),
        array(
        'release' => $sentry_version,
        'tags' => array(
            'php_version' => phpversion(),
        ),
        'environment' => 'unit-testing',
        'app_path' => OROBOROS_ROOT_DIRECTORY
        ) );
    if ( getenv( 'RAVEN_ACCOUNT_EMAIL' ) )
    {
        $sentry->user_context( array(
            'email' => getenv( 'OROBOROS_ACCOUNT_EMAIL' )
        ) );
    }
    $sentry->tags_context(
        array(
            'context' => 'unit-testing',
            'language' => 'php',
            'package' => 'core'
        )
    );
    $error_handler = new Raven_ErrorHandler( $sentry );
    $error_handler->registerExceptionHandler();
    $error_handler->registerErrorHandler();
    $error_handler->registerShutdownFunction();
    unset( $git_branch );
    unset( $git_hash );
    unset( $sentry_version );
}

define( 'OROBOROS_TEST_ASSETS',
    realpath( __DIR__ . '/../testing_assets' ) . DIRECTORY_SEPARATOR );
define( 'OROBOROS_TEST_CLASSES',
    realpath( __DIR__ . '/../testing_classes' ) . DIRECTORY_SEPARATOR );
define( 'OROBOROS_TEST_OUTPUT',
    realpath( __DIR__ . '/../testing_output' ) . DIRECTORY_SEPARATOR );
