<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\codex;

use PHPUnit\Framework\TestCase;

/**
 * @group codex
 * @group lexicon
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 * @covers \oroboros\core\traits\core\BaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\collection\traits\CollectionTrait
 * @covers \oroboros\collection\traits\ContainerTrait
 * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
 * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
 */
class LexiconTest
    extends TestCase
{

    /**
     * @group codex
     * @group lexicon
     * @covers \oroboros\codex\traits\LexiconTrait
     * @covers \oroboros\codex\traits\LexiconIndexTrait
     * @covers \oroboros\codex\traits\LexiconArchiveTrait
     * @covers \oroboros\codex\traits\LexiconEntryTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\enum\traits\InterfaceEnumeratorTrait
     * @covers \oroboros\parse\traits\JsonTrait
     * @covers \oroboros\core\traits\patterns\behavioral\ManagerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\DirectorTrait
     * @covers \oroboros\core\traits\patterns\behavioral\WorkerTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testLexiconValid()
    {
        $subjects = $this->_getTestInstances();
        $dir = $this->_getSavePath();
        $dir2 = $this->_getAltSavePath();
        $lex = new \oroboros\codex\Lexicon();
        $this->assertTrue( $lex->isInitialized() );
        try
        {
            $lex->addPath( 'default', $dir );
            throw new \Exception( 'Failed to prevent override of Lexicon default path in [LexiconTrait::_lexiconValidatePath] via addPath.' );
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\codex\\LexiconException',
                $e );
        }
        $dir = $lex->getPath( 'default' );
        $lex->addPath( 'alt', $dir2 );
        try
        {
            $lex->savePath( 'default' );
            throw new \Exception( 'Failed to prevent override of Lexicon default path in [LexiconTrait::_lexiconValidatePath] via savePath.' );
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\codex\\LexiconException',
                $e );
        }
        $lex->savePath( 'alt' );
        $lex->loadPaths();
        $this->assertTrue( $lex->checkPath( 'default' ) );
        $this->assertTrue( $lex->checkPathReadable( 'default' ) );
        $this->assertTrue( $lex->checkPathWritable( 'default' ) );
        $this->assertTrue( $lex->checkPath( 'alt' ) );
        $this->assertTrue( $lex->checkPathReadable( 'alt' ) );
        $this->assertTrue( $lex->checkPathWritable( 'alt' ) );
        $this->assertEquals( realpath( $dir ),
            realpath( $lex->getPath( 'default' ) ) );
        $this->assertArrayHasKey( 'default', $lex->getPaths() );
        $this->assertArrayHasKey( 'alt', $lex->getPaths() );
        try
        {
            $lex->forgetPath( false );
            throw new \Exception( 'Failed to prevent bad parameter in Lexicon::forgetPath' );
        } catch ( \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract $e )
        {
            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\InvalidArgumentException',
                $e );
        }
        try
        {
            $lex->forgetPath( 'default' );
            throw new \Exception( 'Failed to prevent forgetting default directory in Lexicon::forgetPath' );
        } catch ( \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract $e )
        {
            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\codex\\LexiconException',
                $e );
        }
        $lex->forgetPath( 'alt' );
        $this->assertFalse( $lex->getPath( 'alt' ) );
    }

    /**
     * Returns an array of testing entries for the LexiconEntry
     * @return array
     */
    private function _getTestInstances()
    {
        return array(
            'trait' => '\\oroboros\\core\\traits\\OroborosTrait',
            'interface' => '\\oroboros\\core\\interfaces\\api\\OroborosApi',
            'abstract' => '\\oroboros\\core\\abstracts\\AbstractBase',
            'concrete' => '\\oroboros\\message\\Stream',
            'closure' => function()
            {

            },
            'function' => '\\oroboros\\testclasses\\DefaultFunction',
            'invalid' => '\\not\\a\\real\\Class'
        );
    }

    private function _getSavePath()
    {
        $dir = \oroboros\Oroboros::TEMP_DIRECTORY . 'codex/lexicon/';
        if ( !realpath( $dir ) )
        {
            mkdir( $dir );
        }
        return $dir;
    }

    private function _getAltSavePath()
    {
        $dir = \oroboros\Oroboros::TEMP_DIRECTORY . 'codex/lexicon2/';
        if ( !realpath( $dir ) )
        {
            mkdir( $dir );
        }
        return $dir;
    }

}
