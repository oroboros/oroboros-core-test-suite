<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\codex;

use PHPUnit\Framework\TestCase;

/**
 * @group codex
 * @group lexicon
 * @group archive
 * @covers \oroboros\codex\traits\LexiconArchiveTrait
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 * @covers \oroboros\core\traits\core\BaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\collection\traits\CollectionTrait
 * @covers \oroboros\collection\traits\ContainerTrait
 * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
 * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
 */
class LexiconArchiveTest
    extends TestCase
{

    /**
     * @group codex
     * @group lexicon
     * @group archive
     * @covers \oroboros\codex\traits\LexiconArchiveTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testLexiconArchiveSerialize()
    {
        $dir = \oroboros\Oroboros::TEMP_DIRECTORY . 'codex/lexicon/';
        if ( !is_dir( $dir ) )
        {
            mkdir( $dir, 0755 );
        }
        $lex = new \oroboros\codex\LexiconEntry( '\\oroboros\\message\\Stream' );
        $lex_index = new \oroboros\codex\LexiconIndex( 'unit-test', $lex );
        $archive = new \oroboros\codex\LexiconArchive( $lex_index, 'test', $dir );
        $serial = serialize( $archive );
        $this->assertStringEndsWith( '}', $serial );
        $this->assertTrue( unserialize( $serial ) instanceof \oroboros\codex\interfaces\contract\LexiconArchiveContract );
        $this->assertInstanceOf( '\\oroboros\\codex\\interfaces\\contract\\LexiconArchiveContract',
            $archive );
        $this->assertInstanceOf( '\\oroboros\\codex\\interfaces\\contract\\LexiconArchiveContract',
            unserialize( $serial ) );
    }

}
