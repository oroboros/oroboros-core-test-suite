<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\libraries\extension;

/**
 * <Extension Test Cases>
 * These tests prove the stable compliance of the validation extension.
 * @group extensions
 * @covers \oroboros\core\traits\extensions\ExtensionTrait
 * @covers \oroboros\core\traits\patterns\structural\StaticControlApiTrait
 * @covers \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\collection\traits\CollectionTrait
 * @covers \oroboros\collection\traits\ContainerTrait
 * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
 *
 */
class ExtensionTest
    extends \oroboros\tests\AbstractTestClass
{

    const TEST_EXTENDABLE_CLASS = '\\oroboros\\testclass\\core\\extensions\\ValidExtendable';
    const TEST_CLASS = '\\oroboros\\testclass\\core\\extensions\\ValidExtension';
    const TEST_CLASS_EXPECTED_CONTRACT = '\\oroboros\\core\\interfaces\\contract\\extensions\\ExtensionContract';
    const TEST_RUN_BASELINE_EXTENDABLE = true;

    protected $expected_extension_context = 'unit-test';
    protected $expected_extension_id = 'test-foo';
    protected $expected_extension_indexes = array(
        'foo'
    );
    protected $expected_extension_api = array(
        'foo' => array(
            'foo',
            'bar',
            'baz'
        )
    );

    /**
     * @group extension
     * @covers \oroboros\core\traits\extensions\ExtendableTrait
     * @covers \oroboros\core\traits\extensions\ExtensionTrait
     * @covers \oroboros\core\traits\patterns\structural\StaticControlApiTrait
     * @covers \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     *
     */
    public function testExtensionValidation()
    {
        $this->runTestExtensionValidation();
    }

    /**
     * The real extension validation test, because PHPUnit is too dumb to run
     * parent::method when you use the backupStaticAttributes annotation,
     * and basically breaks the entire way PHP works.
     * @return void
     */
    protected function runTestExtensionValidation()
    {
        $this->evaluateInvalid();
        $extendable = $this->getExtendableClass();
        $extension = $this->getTestClass();
        $this->assertExtensionId( $this::TEST_CLASS,
            $this->expected_extension_id );
        $this->assertExtensionContext( $extension,
            $this->expected_extension_context );
        $this->assertExtensionContract( $extension,
            $this::TEST_CLASS_EXPECTED_CONTRACT );
        $this->assertExtensionCompatibility( $extension, $extendable );
        $this->assertExtensionIndexes( $extension,
            $this->expected_extension_indexes );
        $indexes = $extension::getIndexes();
        foreach ( $this->expected_extension_indexes as
            $index )
        {
            $this->assertTrue( in_array( $index, $indexes ),
                'Expected extension index [%1%s] does not exist in extension '
                . '[%2%s] in [%3$s] of test class [%4$s]', $index,
                get_class( $extension ), __METHOD__, get_class( $this ) );
        }
        $this->assertTrue( $this::TEST_CLASS_EXPECTED_CONTRACT === $extendable::getExtendableContract(),
            sprintf( 'Failed to assert that extendable '
                . 'class [%1$s] with declared extension contract [%2$s] declares expected '
                . 'contract [%3$s] as its extension contract in [%4$s] of testclass [%5$s]',
                get_class( $extendable ), $extendable::getExtendableContract(),
                $this::TEST_CLASS_EXPECTED_CONTRACT, __METHOD__,
                get_class( $this ) ) );
        $this->assertContractInterfaceValid( $extension );
        $this->assertTrue( $extendable::checkExtendableContext( $extension::getContext() ),
            sprintf( 'Failed to assert that given extension context for [%1$s] of [%2$s] '
                . 'matches extendable class context [%3$s] for extendable class [%4$s] '
                . 'at [%5$s] in testclass [%6$s]', get_class( $extension ),
                $extension::getContext(), $extendable::getExtendableContext(),
                get_class( $extendable ), __METHOD__, get_class( $this ) ) );
        $this->assertTrue( $extendable::checkExtendableContract( $extension ),
            sprintf( 'Extendable [%1$s] failed to assert that contract of [%2$s] '
                . 'exists in extension [%3$s] at [%4$s] of test class [%5$s]',
                get_class( $extendable ), $extendable::getExtendableContract(),
                get_class( $extension ), __METHOD__, get_class( $this ) ) );
        $this->assertTrue( $extendable::hasExtension( $extension::getExtensionId() ),
            sprintf( 'Failed to assert that extendable [%1$s] was properly extended '
                . 'with extension [%2$s] in [%3$s] of test class [%4$s]',
                get_class( $extendable ), get_class( $extension ), __METHOD__,
                get_class( $this ) ) );
    }

    /**
     * @group extension
     * @covers \oroboros\core\traits\extensions\ExtendableTrait
     * @covers \oroboros\core\traits\extensions\ExtensionTrait
     * @covers \oroboros\core\traits\patterns\structural\StaticControlApiTrait
     * @covers \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     *
     */
    public function testExtensionIndexes()
    {
        $this->runTestExtensionIndexes();
    }

    /**
     * The real extension indexes test, because PHPUnit is too dumb to run
     * parent::method when you use the backupStaticAttributes annotation,
     * and basically breaks the entire way PHP works.
     * @return void
     */
    protected function runTestExtensionIndexes()
    {
        $extendable = $this->getExtendableClass();
        $extension = $this->getTestClass();
        $this->assertFalse( $extension->getIndex( 'not a valid index' ),
            sprintf( 'Failed to assert that getIndex returns false for an '
                . 'invalid index for extension [%1$s] in [%2$s] of test class '
                . '[%3$s]', get_class( $extension ), __METHOD__,
                get_class( $this ) ) );
        $this->assertFalse( $extendable->getIndex( 'not a valid index' ),
            sprintf( 'Failed to assert that getIndex returns false for an '
                . 'invalid index for extendable [%1$s] in [%2$s] of test class '
                . '[%3$s]', get_class( $extendable ), __METHOD__,
                get_class( $this ) ) );
        if ( empty( $this->expected_extension_indexes ) )
        {
            //no indexes to test
            return;
        }
        if ( !$extendable::hasExtension( $extension::getExtensionId() ) )
        {
            //We don't want to double extend if
            //another test already extended it.
            $extendable::extend( $extension );
        }
        foreach ( $this->expected_extension_indexes as
            $index )
        {
            $this->assertTrue( $extension->getIndex( $index ) !== false,
                sprintf( 'Failed to assert that valid index [%1$s] was detected '
                    . 'when getIndex was called for extension [%2$s] in [%3$s] '
                    . 'of test class [%4$s]. Subject contains the following '
                    . 'indexes: [%5$s]', $index, get_class( $extension ),
                    __METHOD__, get_class( $this ),
                    print_r( $extension->getIndexes(), 1 ) ) );
            $this->assertTrue( $extendable->getIndex( $index ) !== false,
                sprintf( 'Failed to assert that valid index [%1$s] was detected '
                    . 'when getIndex was called for extendable [%2$s] in [%3$s] '
                    . 'of test class [%4$s].', $index, get_class( $extendable ),
                    __METHOD__, get_class( $this ) ) );
            $this->assertTrue( $extension::api( $index ) !== false,
                sprintf( 'Failed to assert that expected api index [%1$s] exists '
                    . 'in extension [%2$s] at [%3$s] of test class [%4$s]',
                    $index, get_class( $extension ), __METHOD__,
                    get_class( $this ) ) );
            $this->assertTrue( $extendable::api( $index ) !== false,
                sprintf( 'Failed to assert that expected api index [%1$s] exists '
                    . 'in extendable [%2$s] at [%3$s] of test class [%4$s]',
                    $index, get_class( $extendable ), __METHOD__,
                    get_class( $this ) ) );
        }
    }

    /**
     * @group extension
     * @covers \oroboros\core\traits\extensions\ExtendableTrait
     * @covers \oroboros\core\traits\extensions\ExtensionTrait
     * @covers \oroboros\core\traits\patterns\structural\StaticControlApiTrait
     * @covers \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     *
     */
    public function testExtensionApi()
    {
        $this->runTestExtensionApi();
    }

    /**
     * The real extension api test, because PHPUnit is too dumb to run
     * parent::method when you use the backupStaticAttributes annotation,
     * and basically breaks the entire way PHP works.
     * @return void
     */
    protected function runTestExtensionApi()
    {
        $extendable = $this->getExtendableClass();
        $extension = $this->getExtensionClass();
        $api = $extension::api();
        if ( !$extendable::hasExtension( $extension::getExtensionId() ) )
        {
            //We don't want to double extend if
            //another test already extended it.
            $extendable::extend( $extension );
        }
        if ( empty( $api ) )
        {
            //Test that the api is actually empty
            $this->assertEmpty( $this->expected_extension_api,
                sprintf( 'Failed to assert an expected empty api for instance of '
                    . '[%1$s] in test [%2$s] called from test class [%3$s]',
                    get_class( $extension ), __METHOD__, get_class( $this ) ) );
            return;
        }
        if ( empty( $this->expected_extension_api ) )
        {
            //Prevents risky test assessment on an empty api
            $this->assertTrue( true );
            return;
        }
        foreach ( $this->expected_extension_api as
            $index =>
            $methods )
        {
            foreach ( $methods as
                $method )
            {
                $this->assertTrue( $extension::api( $index, $method ) !== false,
                    sprintf( 'Failed to assert that expected api method [%1$s] '
                        . 'in index [%2$s] exists in api of extension [%3$s] in '
                        . '[%4$s] of test class [%4$s]', $method, $index,
                        get_class( $extension ), __METHOD__, get_class( $this ) ) );
                $this->assertTrue( $extendable::api( $index, $method ) !== false,
                    sprintf( 'Failed to assert that expected api method [%1$s] '
                        . 'in index [%2$s] exists in api of extendable [%3$s] in '
                        . '[%4$s] of test class [%5$s]. Provided api set for this '
                        . 'extendable index and method: [%6$s]', $method,
                        $index, get_class( $extendable ), __METHOD__,
                        get_class( $this ),
                        print_r( json_decode( json_encode( $extendable::api( $index,
                                        $method ) ), 1 ), 1 ) ) );
                $this->assertTrue( json_decode( json_encode( $extension::api( $index,
                                $method ) ), 1 ) === json_decode( json_encode( $extendable::api( $index,
                                $method ) ), 1 ),
                    sprintf( 'Failed to assert that api call to extension [%1$s] '
                        . 'and extendable [%2$s] were equal for method [%3$s] of '
                        . 'index [%4$s] after extension, in [%5$s] of test class '
                        . '[%6$s]', get_class( $extension ),
                        get_class( $extendable ), $method, $index, __METHOD__,
                        get_class( $this ) ) );
                $this->assertTrue( $extension::hasIndex( $index ),
                    sprintf( 'Failed to assert that extension [%1$s] returns '
                        . 'true when hasIndex called for expected index [%2$s] '
                        . 'in [%3$s] of [%4$s]', get_class( $extension ),
                        $index, __METHOD__, get_class( $this ) ) );
                $this->assertTrue( $extendable::hasIndex( $index ),
                    sprintf( 'Failed to assert that extension [%1$s] returns '
                        . 'true when hasIndex called for expected index [%2$s] '
                        . 'in [%3$s] of [%4$s]', get_class( $extendable ),
                        $index, __METHOD__, get_class( $this ) ) );
                $this->assertTrue( $extension::hasMethod( $index, $method ),
                    sprintf( 'Failed to assert that extension [%1$s] returns '
                        . 'true when hasMethod called for expected method [%2$s] of index [%3$s] '
                        . 'in [%4$s] of [%5$s]', get_class( $extension ),
                        $method, $index, __METHOD__, get_class( $this ) ) );
                $this->assertTrue( $extendable::hasMethod( $index, $method ),
                    sprintf( 'Failed to assert that extendable [%1$s] returns '
                        . 'true when hasMethod called for expected method [%2$s] of index [%3$s] '
                        . 'in [%4$s] of [%5$s]', get_class( $extendable ),
                        $method, $index, __METHOD__, get_class( $this ) ) );
            }
        }
    }

    /**
     * Performs assertations to insure that invalid
     * extension cases are appropriately handled.
     *
     * This set of assertations only needs to run one time.
     * Overrides should set the class constant TEST_RUN_BASELINE_EXTENDABLE
     * to false to prevent excessive testing overhead.
     *
     * @return void
     */
    protected function evaluateInvalid()
    {
        if ( !$this::TEST_RUN_BASELINE_EXTENDABLE )
        {
            //No need to do this again if it has been done already.
            $this->assertTrue( true );
            return;
        }
        $extendable = $this->getExtendableClass();
        $this->assertEquals( 'test-baz',
            \oroboros\testclass\core\extensions\InvalidContextExtension::getExtensionId(),
            sprintf( 'Extension should declare its id even if initialization has '
                . 'not been called. Expected [%1$s] but received [%2$s].',
                'test-baz',
                \oroboros\testclass\core\extensions\InvalidContextExtension::getExtensionId() ) );
        $this->assertEquals( 'boogity',
            \oroboros\testclass\core\extensions\InvalidContextExtension::getContext(),
            sprintf( 'Extension should declare its context even if initialization'
                . ' has not been called. Expected [%1$s] but received [%2$s].',
                'boogity',
                \oroboros\testclass\core\extensions\InvalidContextExtension::getContext() ) );
        $invalid_context = new \oroboros\testclass\core\extensions\InvalidContextExtension();
        $invalid_contract = new \oroboros\testclass\core\extensions\InvalidContractExtension();
        try
        {
            $extendable::extend( $invalid_contract );
        } catch ( \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract $e )
        {
            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\InvalidArgumentException',
                $e );
        }
        try
        {
            $extendable::extend( $invalid_context );
        } catch ( \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract $e )
        {
            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\InvalidArgumentException',
                $e );
        }
        $this->assertTrue( $extendable::checkExtendableContext( $invalid_contract::getContext() ),
            sprintf( 'Failed to assert that invalid contract extension [%1$s] has valid context in '
                . '[%2$s] of test class [%3$s]. Provided context: [%4$s], expected context: [%5$s]',
                get_class( $invalid_contract ), __METHOD__, get_class( $this ),
                $invalid_contract::getContext(),
                $extendable::getExtendableContext() ) );
        $this->assertFalse( $extendable::checkExtendableContext( $invalid_context::getContext() ),
            sprintf( 'Failed to assert that invalid contract extension [%1$s] has invalid context in '
                . '[%2$s] of test class [%3$s]. Provided context: [%4$s], Expected context: [%5$s]',
                get_class( $invalid_contract ), __METHOD__, get_class( $this ),
                $invalid_contract::getContext(),
                $extendable::getExtendableContext() ) );
        $this->assertFalse( $extendable::checkExtendableContract( $invalid_contract ),
            sprintf( 'Failed to assert that invalid contract extension [%1$s] has invalid contract in '
                . '[%2$s] of test class [%3$s]. Expected contract: [%4$s]',
                get_class( $invalid_contract ), __METHOD__, get_class( $this ),
                $extendable::getExtendableContract() ) );
        $this->assertTrue( $extendable::checkExtendableContract( $invalid_context ) );
        $this->assertFalse( $extendable::hasExtension( $invalid_contract::getExtensionId() ) );
        $this->assertFalse( $extendable::hasExtension( $invalid_contract ) );
        $this->assertFalse( $extendable::hasExtension( $invalid_context::getExtensionId() ) );
        $this->assertFalse( $extendable::hasExtension( $invalid_context ) );
    }

    /**
     * Performs assertions to insure the integrity of the extension id.
     * @param string|object $class The extension class
     * @param string $expected The expected extension id
     */
    protected function assertExtensionId( $class, $expected )
    {
        $this->assertEquals( $expected, $class::getExtensionId(),
            sprintf( 'Extension should declare its id even if initialization has '
                . 'not been called. Expected [%1$s] but received [%2$s].',
                'test-baz', $class::getExtensionId() ) );
    }

    /**
     * Performs assertions to insure the integrity of the extension context.
     * @param string|object $class The extension class
     * @param string $expected The expected extension context
     */
    protected function assertExtensionContext( $class, $expected )
    {
        $this->assertEquals( $expected, $class::getContext(),
            sprintf( 'Extension should declare its context even if initialization'
                . ' has not been called. Expected [%1$s] but received [%2$s]. '
                . 'Encountered in [%3$s] of test class [%4$s].', $expected,
                $class::getContext(), __METHOD__, get_class( $this ) ) );
    }

    /**
     * Performs assertions to insure the integrity of the
     * extension contract interface.
     * @param string|object $class The extension class
     * @param string $expected The expected contract interface
     */
    protected function assertExtensionContract( $class, $expected )
    {
        $subject = (is_object( $class )
            ? get_class( $class )
            : $class);
        $this->assertInstanceOf( $expected, $class,
            sprintf( 'Extension [%1$s] must implement expected contract '
                . 'interface [%2$s] in [%3$s] of test class [%4$s].', $subject,
                $expected, __METHOD__, get_class( $this ) ) );
    }

    /**
     * Performs assertions to insure that the extendable class can
     * receive the extension class as a valid extension.
     * @param \oroboros\core\interfaces\contract\extensions\ExtensionContract $extension The extension class
     * @param \oroboros\core\interfaces\contract\extensions\ExtendableContract $extendable The extendable class
     */
    protected function assertExtensionCompatibility( $extension, $extendable )
    {
        $subject = (is_object( $extension )
            ? get_class( $extension )
            : $extension);
        $extendable_subject = (is_object( $extendable )
            ? get_class( $extendable )
            : $extendable);
        $this->assertExtensionContract( $extension,
            $extendable::getExtendableContract() );
        $this->assertExtensionContext( $extension,
            $extendable::getExtendableContext() );
        try
        {
            if ( !$extendable::hasExtension( $extension::getExtensionId() ) )
            {
                //We don't want to double extend if
                //another test already extended it.
                $extendable::extend( $extension );
            }
        } catch ( \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract $e )
        {
            $this->assertFalse( true,
                sprintf( 'Instance of [%s] should not have thrown an exception '
                    . 'when passed for [%s]. Received exception of type [%s] '
                    . 'with message [%s].', $subject,
                    $extendable_subject . '::extend' ), get_class( $e ),
                $e->getMessage() );
        }
        try
        {
            $extendable::extend( $extension );
        } catch ( \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract $e )
        {

        }
        $expected_api = json_decode( json_encode( $extension::api() ), 1 );
        if ( !$expected_api )
        {
            //no methods to evaluate
            return;
        }
        foreach ( $expected_api as
            $index =>
            $methods )
        {
            if ( !$methods || (!$extension::api( $index ) ) )
            {
                //If an index returns false,
                //skip it.
                continue;
            }
            foreach ( $methods as
                $method =>
                $details )
            {
                $this->assertTrue( $extendable::api( $index, $method ) !== false,
                    sprintf( 'Failed to assert that expected api method [%1$s] '
                        . 'in index [%2$s] exists in api of extendable [%3$s] in '
                        . '[%4$s] of test class [%5$s]. Provided api set for this '
                        . 'extendable index: [%6$s]', $method, $index,
                        $extendable_subject, __METHOD__, get_class( $this ),
                        print_r( json_decode( json_encode( $extendable::api( $index ) ),
                                1 ), 1 ) ) );
                $this->assertTrue( json_decode( json_encode( $extension::api( $index,
                                $method ) ), 1 ) === json_decode(
                        json_encode( $extendable::api( $index, $method ) ), 1 ),
                    sprintf( 'Failed to assert that api call to extension [%1$s] '
                        . 'and extendable [%2$s] were equal for method [%3$s] of '
                        . 'index [%4$s] after extension, in [%5$s] of test class '
                        . '[%6$s]', $subject, $extendable_subject, $method,
                        $index, __METHOD__, get_class( $this ) ) );
                $this->assertTrue( $extendable::hasIndex( $index ),
                    sprintf( 'Failed to assert that extension [%1$s] returns '
                        . 'true when hasIndex called for expected index [%2$s] '
                        . 'in [%3$s] of [%4$s]', $extendable_subject, $index,
                        __METHOD__, get_class( $this ) ) );
                $this->assertTrue( $extendable::hasMethod( $index, $method ),
                    sprintf( 'Failed to assert that extendable [%1$s] returns '
                        . 'true when hasMethod called for expected method [%2$s] of index [%3$s] '
                        . 'in [%4$s] of [%5$s]', $extendable_subject, $method,
                        $index, __METHOD__, get_class( $this ) ) );
            }
        }
    }

    /**
     * Performs assertions to insure the integrity of
     * expected extension api indexes.
     * @param string|object $class The extension class
     * @param array $expected The expected indexes
     */
    protected function assertExtensionIndexes( $class, $expected = array() )
    {
        if ( !$expected || empty( $expected ) )
        {
            //Prevents unsafe test
            $this->assertTrue( true );
            return;
        }
        $subject = (is_object( $class )
            ? get_class( $class )
            : $class);
        $indexes = $class::getIndexes();
        foreach ( $expected as
            $index )
        {
            $this->assertTrue( in_array( $index, $indexes ),
                'Expected extension index [%1%s] does not exist in extension '
                . '[%2%s] in [%3$s] of test class [%4$s]', $index, $subject,
                __METHOD__, get_class( $this ) );
            $this->assertTrue( $class::hasIndex( $index ),
                sprintf( 'Failed to assert that extension [%1$s] returns '
                    . 'true when hasIndex called for expected index [%2$s] '
                    . 'in [%3$s] of [%4$s]', $subject, $index, __METHOD__,
                    get_class( $this ) ) );
        }
    }

    /**
     * Performs assertions to insure the integrity of expected
     * extension api declarations.
     * @param string|object $class
     * @param array $expected
     */
    protected function assertExtensionApi( $class, $expected = array() )
    {
        $subject = (is_object( $class )
            ? get_class( $class )
            : $class);
        $api = $class::api();
        if ( !$expected )
        {
            $expected = array();
        }
        if ( !$api )
        {
            //Test that the api is actually empty
            $this->assertEmpty( $expected,
                sprintf( 'Failed to assert an expected empty api for instance of '
                    . '[%1$s] in test [%2$s] called from test class [%3$s]',
                    $subject, __METHOD__, get_class( $this ) ) );
            return;
        }
        if ( empty( $expected ) )
        {
            return;
        }
        $indexes = array_keys( $expected );
        $this->assertExtensionIndexes( $class, $indexes );
        foreach ( $expected as
            $index =>
            $methods )
        {
            foreach ( $methods as
                $method )
            {
                $this->assertTrue( $class::api( $index, $method ) !== false,
                    sprintf( 'Failed to assert that expected api method [%1$s] '
                        . 'in index [%2$s] exists in api of extension [%3$s] in '
                        . '[%4$s] of test class [%4$s]', $method, $index,
                        $subject, __METHOD__, get_class( $this ) ) );
                $this->assertTrue( $class::hasIndex( $index ),
                    sprintf( 'Failed to assert that extension [%1$s] returns '
                        . 'true when hasIndex called for expected index [%2$s] '
                        . 'in [%3$s] of [%4$s]', $subject, $index, __METHOD__,
                        get_class( $this ) ) );
                $this->assertTrue( $class::hasMethod( $index, $method ),
                    sprintf( 'Failed to assert that extension [%1$s] returns '
                        . 'true when hasMethod called for expected method [%2$s] of index [%3$s] '
                        . 'in [%4$s] of [%5$s]', $subject, $method, $index,
                        __METHOD__, get_class( $this ) ) );
            }
        }
    }

    /**
     * Returns an initialized instance of the valid extendable
     * class for testing the extension.
     * @return \oroboros\core\interfaces\contract\extensions\ExtensionContract
     */
    protected function getExtendableClass()
    {
        $class = $this::TEST_EXTENDABLE_CLASS;
        $instance = new $class();
        $instance::staticInitialize();
        return $instance;
    }

    /**
     * Returns an initialized instance of the test extension class.
     * @return \oroboros\core\interfaces\contract\extensions\ExtendableContract
     */
    protected function getExtensionClass()
    {
        $class = $this::TEST_CLASS;
        $instance = new $class();
        $instance::staticInitialize();
        return $instance;
    }

}
