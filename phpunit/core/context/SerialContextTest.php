<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\core\context;

/**
 * <Serial Context Test Cases>
 * These tests prove the stable functionality serializable context objects.
 *
 * All other context object tests should extend from this class, so that
 * extended functionality can be proven to follow the Liskov substitution
 * principle.
 *
 * @group core
 * @group context
 * @covers \oroboros\core\traits\core\context\SerialContextTrait
 * @covers \oroboros\core\traits\core\context\ContextTrait
 * @covers \oroboros\validate\traits\ValidatorTrait
 */
class SerialContextTest
    extends ContextTest
{

    /**
     * All objects descending from this line provide fast and concise
     * serialization for easy external storage and restoration.
     * All of these objects should be capable of serializing and
     * unserializing without losing integrity, and without a large
     * performance overhead.
     */
    use \oroboros\tests\traits\assertions\context\ContextualSerialEvaluationTrait;
    use \oroboros\tests\traits\tests\context\ContextualSerialTestTrait;

    const TEST_CLASS = '\\oroboros\\testclass\\core\\context\\DefaultTestSerialContext';

    /**
     * Override this constant to declare a default contextual value
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_VALUE = __CLASS__;

    /**
     * Override this constant to declare an expected contextual value
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_VALUE = __CLASS__;

}
