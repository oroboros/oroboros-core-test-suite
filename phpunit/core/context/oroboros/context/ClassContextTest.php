<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\core\context\oroboros\context;

/**
 * <Class Context Test Cases>
 * These tests prove the stable functionality class context test objects.
 * @group core
 * @group context
 * @covers \oroboros\core\traits\core\context\php\classes\ClassContextTrait
 * @covers \oroboros\core\traits\core\context\CoreContextTrait
 * @covers \oroboros\core\traits\core\context\JsonSerialContextTrait
 * @covers \oroboros\core\traits\core\context\SerialContextTrait
 * @covers \oroboros\core\traits\core\context\ContextTrait
 * @covers \oroboros\validate\traits\ValidatorTrait
 */
class ClassContextTest
    extends \oroboros\tests\core\context\CoreContextTest
{

    use \oroboros\core\traits\utilities\core\StringUtilityTrait;

    const TEST_CLASS = '\\oroboros\\core\\internal\\context\\oroboros\\context\\ClassContextContext';

    /**
     * The default contextual type for Oroboros class-context
     * objects is 'class-context'.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_TYPE = 'class-context';

    /**
     * The default class-context declares 'class-primary-context' as its subtype.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBTYPE = 'class-primary-context';

    /**
     * The default context for class-contexts is the meta-type value from
     * the enumerated interface the base logic validates against.
     * The default designation provided by the base accessor class should always
     * validate as the context if the base accessor class is also provided as
     * the value.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_CONTEXT = \oroboros\Oroboros::OROBOROS_CLASS_CONTEXT;

    /**
     * The default parameter for class-contexts a valid class.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_VALUE = '\\oroboros\\Oroboros';

    /**
     * The default contextual category for class-contexts should bear the
     * oroboros-context class type as its category.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_CATEGORY = \oroboros\core\interfaces\enumerated\type\CoreClassTypes::CLASS_TYPE_CORE_CONTEXT_OROBOROS;

    /**
     * The default contextual sub-category for class-contexts is
     * the core class scope designated for abstract class-contexts.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBCATEGORY = \oroboros\core\interfaces\enumerated\scope\CoreClassScopes::CLASS_SCOPE_CORE_CONTEXT_OROBOROS_CLASS_CONTEXT;

    /**
     * The default package for class-contexts is the oroboros root namespace
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_PACKAGE = \oroboros\Oroboros::API_CODEX;

    /**
     * The default sub-package for class-contexts is the core package
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBPACKAGE = \oroboros\Oroboros::API_SCOPE;

    /**
     * The expected contextual type for Oroboros class-context
     * objects is 'class-context'.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_TYPE = 'class-context';

    /**
     * Default class-context logic declares 'class-primary-context' as its subtype.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBTYPE = 'class-primary-context';

    /**
     * The default meta-type context should validate for the default class-context
     * against the equivalent declaration in the base accessor class.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_CONTEXT = \oroboros\Oroboros::OROBOROS_CLASS_CONTEXT;

    /**
     * The expected contextual value by default is the base accessor,
     * which should always validate correctly against all contextual
     * validation.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_VALUE = '\\oroboros\\Oroboros';

    /**
     * Class-context contexts should bear the oroboros context
     * class type as their category.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_CATEGORY = \oroboros\core\interfaces\enumerated\type\CoreClassTypes::CLASS_TYPE_CORE_CONTEXT_OROBOROS;

    /**
     * The default class-context should bear the class-context subcategory.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBCATEGORY = \oroboros\core\interfaces\enumerated\scope\CoreClassScopes::CLASS_SCOPE_CORE_CONTEXT_OROBOROS_CLASS_CONTEXT;

    /**
     * Class-context contexts should be under the oroboros namespace
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_PACKAGE = \oroboros\Oroboros::API_CODEX;

    /**
     * Class-context contexts should be part of the core package
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBPACKAGE = \oroboros\Oroboros::API_SCOPE;

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group context
     * @covers \oroboros\core\traits\core\context\php\classes\ClassContextTrait
     * @covers \oroboros\core\traits\core\context\CoreContextTrait
     * @covers \oroboros\core\traits\core\context\JsonSerialContextTrait
     * @covers \oroboros\core\traits\core\context\SerialContextTrait
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testValidContext()
    {

        try
        {
            //We're going to manually instantiate this so it does not cause
            //an exit fault on failure, because the object MUST NOT resolve
            //if parameters are incorrect.
            $class = $this::TEST_CLASS;
            $test = new $class( $this->getExpectedContextualContext(), $this->getExpectedContextualValue() );
            $test_class = $this->getExpectedContextualValue();
            $this->assertEquals( $test_class::OROBOROS_CLASS_CONTEXT,
                $test->getContext(),
                sprintf( 'Result of getContext() at [%1$s] in instance of [%2$s]'
                    . ' MUST equal the result of the class context of the testing'
                    . ' class instance. Expected [%3$s] but received [%4$s]',
                    __METHOD__, get_class( $test ), $test_class::OROBOROS_CLASS_CONTEXT,
                    $test->getContext() ) );
            $this->assertEquals( $test_class, $test->getValue(),
                sprintf( 'Result of getValue() at [%1$s] in instance of [%2$s] '
                    . 'MUST equal the fully qualified class name of the testing '
                    . 'class instance. Expected [%3$s] but received [%4$s]',
                    __METHOD__, get_class( $test ), $test_class,
                    $test->getValue() ) );
        } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
        {
            $this->assertTrue( false,
                sprintf( 'Failed to resolve object creation in [%1$s] for '
                    . 'instance of [%2$s]. Class instance [%3$s] could not '
                    . 'be used to create a valid class context of [%4$s]. '
                    . 'Error message: [%5$s]', __METHOD__, $this::TEST_CLASS,
                    $this->getExpectedContextualContext(), $this->getExpectedContextualValue(), $e->getMessage() ) );
        }
    }

    /**
     * Tests context matching for invalid parameters.
     * @group core
     * @group context
     * @covers \oroboros\core\traits\core\context\php\classes\ClassContextTrait
     * @covers \oroboros\core\traits\core\context\CoreContextTrait
     * @covers \oroboros\core\traits\core\context\JsonSerialContextTrait
     * @covers \oroboros\core\traits\core\context\SerialContextTrait
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testInvalidContext()
    {
        try
        {
            $class = $this::TEST_CLASS;
            $test_class = $this->getExpectedContextualValue();
            //We know that only the actual class context should resolve,
            //so appending anything to it should break the instantiation
            //of the context object.
            $bad_context = null;
            $test = new $class( $bad_context, $this->getExpectedContextualValue() );
            $this->assertTrue( false,
                sprintf( 'Failed to reject invalid class context in [%1$s] for '
                    . 'instance of [%2$s]. Only context [%4$s] should have '
                    . 'resolved for [%3$s], but object validation failed to '
                    . 'reject bad context [%5$s]', __METHOD__,
                    get_class( $test ), $test_class, $test_class::OROBOROS_CLASS_CONTEXT,
                    $bad_context ) );
        } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
        {
            //this is what should happen here.
            $this->assertTrue( true );
        }
    }

}
