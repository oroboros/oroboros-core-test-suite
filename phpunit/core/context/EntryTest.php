<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\core\context;

/**
 * <Context Entry Test Cases>
 * These tests prove the stability of context entries.
 * @group core
 * @group context
 * @group alpha
 * @covers \oroboros\core\traits\core\context\EntryTrait
 * @covers \oroboros\core\traits\core\context\CoreContextTrait
 * @covers \oroboros\core\traits\core\context\JsonSerialContextTrait
 * @covers \oroboros\core\traits\core\context\SerialContextTrait
 * @covers \oroboros\core\traits\core\context\ContextTrait
 * @covers \oroboros\validate\traits\ValidatorTrait
 */
class EntryTest
    extends \oroboros\tests\AbstractTestClass
{

    const TEST_CLASS = '\\oroboros\\core\\internal\\context\\Entry';
    const TEST_CLASS_EXPECTED_CONTRACT = '\\oroboros\\core\\interfaces\\contract\\core\\context\\EntryContract';
    const TEST_CLASS_EXPECTED_INTERFACE = false;
    const TEST_CLASS_EXPECTED_API = '\\oroboros\\core\\interfaces\\api\\core\\CoreApi';

    /**
     * Override this constant to declare a default contextual type
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_TYPE = 'unit-test-alternate';

    /**
     * Override this constant to declare a default contextual sub-type
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBTYPE = 'unit-test-alternate';

    /**
     * Override this constant to declare a default contextual context
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_CONTEXT = 'unit-test-alternate';

    /**
     * Override this constant to declare a default contextual value
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_VALUE = 'unit-test-alternate';

    /**
     * Override this constant to declare a default contextual category
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_CATEGORY = 'unit-test-alternate';

    /**
     * Override this constant to declare a default contextual sub-category
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBCATEGORY = 'unit-test-alternate';

    /**
     * Override this constant to declare a default contextual package
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_PACKAGE = 'unit-test-alternate';

    /**
     * Override this constant to declare a default contextual sub-package
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBPACKAGE = 'unit-test-alternate';

    /**
     * Override this constant to declare an expected contextual type
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_TYPE = 'unit-test';

    /**
     * Override this constant to declare an expected contextual sub-type
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBTYPE = 'unit-test';

    /**
     * Override this constant to declare an expected contextual context
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_CONTEXT = 'unit-test';

    /**
     * Override this constant to declare an expected contextual value
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_VALUE = 'unit-test';

    /**
     * Override this constant to declare an expected contextual category
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_CATEGORY = \oroboros\core\interfaces\enumerated\type\CoreClassTypes::CLASS_TYPE_CORE_CONTEXT;

    /**
     * Override this constant to declare an expected contextual sub-category
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBCATEGORY = \oroboros\core\interfaces\enumerated\scope\CoreClassScopes::CLASS_SCOPE_CORE_ABSTRACT;

    /**
     * Override this constant to declare an expected contextual package
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_PACKAGE = \oroboros\Oroboros::API_CODEX;

    /**
     * Override this constant to declare an expected contextual sub-package
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBPACKAGE = \oroboros\Oroboros::API_SCOPE;

    private static $_test_contexts = array();
    private static $_test_contracts = array();
    private static $_test_expected_id = null;
    private static $_test_expected_title = null;
    protected $_test_values = array();

    /**
     * Tests null context matching for valid parameters.
     * @group core
     * @group context
     * @group alpha
     * @covers \oroboros\core\traits\core\context\EntryTrait
     * @covers \oroboros\core\traits\core\context\CoreContextTrait
     * @covers \oroboros\core\traits\core\context\JsonSerialContextTrait
     * @covers \oroboros\core\traits\core\context\SerialContextTrait
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testEntryInstantiation()
    {
        $test = $this->getTestClass();
        $this->assertEquals( $this->getDefaultEntryId(), $test->getId() );
        $this->assertEquals( $this->getDefaultEntryTitle(), $test->getTitle() );
//        $this->assertEquals( $this->getExpectedContextualType(), $test->getType() );
        $this->assertEquals( $this->getExpectedContextualContext(),
            $test->getContext() );
        $this->assertEquals( $this->getExpectedContextualValue(),
            $test->getValue() );
        $this->assertEquals( $this->getExpectedContextualCategory(),
            $test->getCategory() );
        $this->assertEquals( $this->getExpectedContextualSubcategory(),
            $test->getSubcategory() );
        $this->assertEquals( $this->getDefaultEntryMetaData(),
            $test->getMetaData()->toArray() );
        $this->assertEquals( $this->getDefaultEntryTags(),
            $test->getTags()->toArray() );
        $this->assertEquals( $this->getDefaultEntryRelations(),
            $test->getRelations()->toArray() );
        $this->assertEquals( $this->getDefaultStringableResult(), (string) $test );
    }

    /**
     * Tests serialization and unserialization.
     * @group core
     * @group context
     * @group alpha
     * @covers \oroboros\core\traits\core\context\EntryTrait
     * @covers \oroboros\core\traits\core\context\CoreContextTrait
     * @covers \oroboros\core\traits\core\context\JsonSerialContextTrait
     * @covers \oroboros\core\traits\core\context\SerialContextTrait
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testEntrySerialization()
    {
        $test = $this->getTestClass();
        $serial = serialize( $test );
        $unserial = unserialize( $serial );
        $this->assertEquals( $test->getId(), $unserial->getId() );
        $this->assertEquals( $test->getTitle(), $unserial->getTitle() );
        $this->assertEquals( $test->getType(), $unserial->getType() );
        $this->assertEquals( $test->getContext(), $unserial->getContext() );
        $this->assertEquals( $test->getValue(), $unserial->getValue() );
        $this->assertEquals( $test->getCategory(), $unserial->getCategory() );
        $this->assertEquals( $test->getSubcategory(),
            $unserial->getSubcategory() );
        $this->assertEquals( $test->getMetaData(), $unserial->getMetaData() );
        $this->assertEquals( $test->getTags(), $unserial->getTags() );
        $this->assertEquals( $test->getRelations(), $unserial->getRelations() );
    }

    /**
     * Tests typical contextual parameters:
     * type, context, value, category, and subcategory.
     * @group core
     * @group context
     * @group alpha
     * @covers \oroboros\core\traits\core\context\EntryTrait
     * @covers \oroboros\core\traits\core\context\CoreContextTrait
     * @covers \oroboros\core\traits\core\context\JsonSerialContextTrait
     * @covers \oroboros\core\traits\core\context\SerialContextTrait
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testEntryContextIntegrity()
    {
        $test = $this->getTestClass();
        $context = $this->getDefaultContextObject();
        $this->assertEquals( $this->getDefaultEntryId(), $test->getId() );
        $test2 = $test->withId( $this->getExpectedEntryId() );
        $this->assertEquals( $this->getExpectedEntryId(), $test2->getId() );
        $this->assertNotEquals( $test->getId(), $test2->getId() );
        $this->assertEquals( $this->getDefaultEntryTitle(), $test->getTitle() );
        $test2 = $test->withTitle( $this->getExpectedEntryTitle() );
        $this->assertEquals( $this->getExpectedEntryTitle(), $test2->getTitle() );
        $this->assertNotEquals( $test->getTitle(), $test2->getTitle() );
        $this->assertEquals( $context->getType(),
            $test->withType( $context )->getType() );
        $this->assertEquals( $context->getCategory(),
            $test->withCategory( $context )->getCategory() );
        $this->assertEquals( $context->getSubcategory(),
            $test->withSubCategory( $context )->getSubcategory() );
        $this->assertEquals( $context->getContext(),
            $test->withContext( $context, $context )->getContext() );
        $this->assertEquals( $context->getValue(),
            $test->withContext( $context, $context )->getValue() );
        $test2 = $test->withContext( $this->getDefaultContextualContext(),
            $this->getDefaultContextualValue() );
        $this->assertEquals( $this->getDefaultContextualContext(),
            $test2->getContext() );
        $this->assertNotEquals( $this->getDefaultContextualContext(),
            $test->getContext() );
        $test3 = $test2->withoutContext();
        $this->assertNull( $test3->getContext() );
        $this->assertNull( $test3->getValue() );
        $test2 = $test->withType( $this->getDefaultContextualType() );
        $this->assertNotEquals( $this->getDefaultContextualType(),
            $test->getType() );
        $this->assertEquals( $this->getDefaultContextualType(),
            $test2->getType() );
        $test2 = $test->withSubType( $this->getDefaultContextualSubType() );
        $this->assertNotEquals( $this->getDefaultContextualSubType(),
            $test->getSubType() );
        $this->assertEquals( $this->getDefaultContextualSubType(),
            $test2->getSubType() );
        $this->assertNull( $test->withoutSubType()->getSubType() );
        $test2 = $test->withPackage( $this->getDefaultContextualPackage() );
        $this->assertNotEquals( $this->getDefaultContextualPackage(),
            $test->getPackage() );
        $this->assertEquals( $this->getDefaultContextualPackage(),
            $test2->getPackage() );
        $this->assertNull( $test->withoutPackage()->getPackage() );
        $test2 = $test->withSubPackage( $this->getDefaultContextualSubPackage() );
        $this->assertNotEquals( $this->getDefaultContextualSubPackage(),
            $test->getSubPackage() );
        $this->assertEquals( $this->getDefaultContextualSubPackage(),
            $test2->getSubPackage() );
        $this->assertNull( $test->withoutSubPackage()->getSubPackage() );
        $test2 = $test->withCategory( $this->getDefaultContextualCategory() );
        $this->assertNotEquals( $this->getExpectedContextualCategory(),
            $test2->getCategory() );
        $this->assertEquals( $this->getExpectedContextualCategory(),
            $test->getCategory() );
        $test3 = $test2->withoutCategory();
        $this->assertNull( $test3->getCategory() );
        $test2 = $test->withSubCategory( $this->getDefaultContextualSubCategory() );
        $this->assertNotEquals( $this->getExpectedContextualSubcategory(),
            $test2->getSubcategory() );
        $this->assertEquals( $this->getExpectedContextualSubcategory(),
            $test->getSubcategory() );
        $test3 = $test2->withoutSubCategory();
        $this->assertNull( $test3->getSubcategory() );
    }

    /**
     * Tests typical contextual parameters:
     * type, context, value, category, and subcategory.
     * @group core
     * @group context
     * @group alpha
     * @covers \oroboros\core\traits\core\context\EntryTrait
     * @covers \oroboros\core\traits\core\context\CoreContextTrait
     * @covers \oroboros\core\traits\core\context\JsonSerialContextTrait
     * @covers \oroboros\core\traits\core\context\SerialContextTrait
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testEntryContextCasting()
    {
        $test = $this->getTestClass();
        $context = $this->getDefaultContextObject();
        $context_class = get_class( $context );
        $test2 = $test->fromContextObject( $test->getId(), $test->getTitle(),
            $context );
        $this->assertEquals( $context,
            $test->fromContextObject( $test->getId(), $test->getTitle(),
                $context )->asContextObject( $context_class ) );
        $this->assertEquals( $context->getType(), $test2->getType() );
        $this->assertEquals( $context->getContext(), $test2->getContext() );
        $this->assertEquals( $context->getValue(), $test2->getValue() );
        $this->assertEquals( $context->getCategory(), $test2->getCategory() );
        $this->assertEquals( $context->getSubcategory(),
            $test2->getSubcategory() );
        $test_context = $test2->asContextObject( $context_class );
        $this->assertEquals( $test_context->getType(), $test2->getType() );
        $this->assertEquals( $test_context->getContext(), $test2->getContext() );
        $this->assertEquals( $test_context->getValue(), $test2->getValue() );
        $this->assertEquals( $test_context->getCategory(), $test2->getCategory() );
        $this->assertEquals( $test_context->getSubcategory(),
            $test2->getSubcategory() );
        try
        {
            //This should always throw an \InvalidArgumentException,
            //as no context object can possibly have this name,
            //and will not validate against the contextual contract.
            $test2->asContextObject( 'not a valid context' );
            $this->assertTrue( false,
                'Failed to throw an expected \InvalidArgumentException on an invalid context object declaration casting attempt.' );
        } catch ( \InvalidArgumentException $e )
        {
            //expected
            $this->assertTrue( true );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            //wrong exception type
            $this->assertTrue( false,
                'Failed to throw expected InvalidArgumentException when casting to an invalid context object. Got exception of type: ' . get_class( $e ) );
        }
        try
        {
            //This should always throw an \InvalidArgumentException,
            //as no context object can be created with a null context.
            $test->withoutContext()->asContextObject( $context_class );
            $this->assertTrue( false,
                'Failed to throw an expected \InvalidArgumentException on an invalid context casting.' );
        } catch ( \InvalidArgumentException $e )
        {
            //expected
            $this->assertTrue( true );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            //wrong exception type
            $this->assertTrue( false,
                'Failed to throw expected InvalidArgumentException when casting to an invalid context object. Got exception of type: ' . get_class( $e ) );
        }
        try
        {
            //Not even a class at all
            $test->fromContextObject( $test->getId(), $test->getTitle(),
                'not a valid context object' );
        } catch ( \InvalidArgumentException $e )
        {
            //expected
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertTrue( false,
                'Failed to throw expected InvalidArgumentException when casting to an invalid context object. Got exception of type: ' . get_class( $e ) );
        }
        try
        {
            //A class that is not contextual
            $test->fromContextObject( $test->getId(), $test->getTitle(),
                new \stdClass() );
        } catch ( \InvalidArgumentException $e )
        {
            //expected
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertTrue( false,
                'Failed to throw expected InvalidArgumentException when casting to an invalid context object. Got exception of type: ' . get_class( $e ) );
        }
        try
        {
            $test->asContextObject( 'not a valid context object' );
        } catch ( \InvalidArgumentException $e )
        {
            //expected
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertTrue( false,
                'Failed to throw expected InvalidArgumentException when casting to an invalid context object. Got exception of type: ' . get_class( $e ) );
        }
        try
        {
            //An entry with a null context and value is not
            //valid for creation of a context object.
            $test3 = $test->withoutContext();
            $test3->asContextObject( $context_class );
        } catch ( \InvalidArgumentException $e )
        {
            //expected
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertTrue( false,
                'Failed to throw expected InvalidArgumentException when casting to an invalid context object. Got exception of type: ' . get_class( $e ) );
        }
    }

    /**
     * Tests Metadata setting, getting, updating and removing.
     * @group core
     * @group context
     * @group alpha
     * @covers \oroboros\core\traits\core\context\EntryTrait
     * @covers \oroboros\core\traits\core\context\CoreContextTrait
     * @covers \oroboros\core\traits\core\context\JsonSerialContextTrait
     * @covers \oroboros\core\traits\core\context\SerialContextTrait
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testEntryMetaData()
    {
        $this->assertEntryContainerization( 'meta', 'getMetaData' );
    }

    /**
     * Tests Metadata setting, getting, updating and removing.
     * @group core
     * @group context
     * @group alpha
     * @covers \oroboros\core\traits\core\context\EntryTrait
     * @covers \oroboros\core\traits\core\context\CoreContextTrait
     * @covers \oroboros\core\traits\core\context\JsonSerialContextTrait
     * @covers \oroboros\core\traits\core\context\SerialContextTrait
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testEntryTags()
    {
        $this->assertEntryContainerization( 'tag', 'getTags' );
    }

    /**
     * Tests Metadata setting, getting, updating and removing.
     * @group core
     * @group context
     * @group alpha
     * @covers \oroboros\core\traits\core\context\EntryTrait
     * @covers \oroboros\core\traits\core\context\CoreContextTrait
     * @covers \oroboros\core\traits\core\context\JsonSerialContextTrait
     * @covers \oroboros\core\traits\core\context\SerialContextTrait
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testEntryRelations()
    {
        $this->assertEntryContainerization( 'relation', 'getRelations' );
    }

    /**
     * Tests internal handling of misconfiguration of child class logic.
     * @group core
     * @group context
     * @group alpha
     * @covers \oroboros\core\traits\core\context\EntryTrait
     * @covers \oroboros\core\traits\core\context\CoreContextTrait
     * @covers \oroboros\core\traits\core\context\JsonSerialContextTrait
     * @covers \oroboros\core\traits\core\context\SerialContextTrait
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testEntryMisconfiguration()
    {
        $has_misconfig = $this->assertMisconfiguredHandled();
        if ( !$has_misconfig )
        {
            $this->assertTrue( true );
        }
    }

    /**
     * Performs assertions for standardized operation of containerization
     * methods, which handle packaged contexts for an entry within a container
     * of related contexts (eg: meta, tags, relations, etc).
     *
     * If the containerization set follows the standard method schema of
     * 'hasType', 'withType', 'withoutType', and 'getType', this method will
     * cover all of its supporting logic.
     *
     * @param string $type The type of containerization method
     * @param string $getAllMethod The getAll method for the specific type
     */
    protected function assertEntryContainerization( $type, $getAllMethod )
    {
        $hasMethod = 'has' . ucfirst( $type );
        $getMethod = 'get' . ucfirst( $type );
        $withMethod = 'with' . ucfirst( $type );
        $withoutMethod = 'without' . ucfirst( $type );
        $test = $this->getTestClass();
        $elements = $this->getExpectedEntryContextObject( $type );
        $contract = $this->getExpectedEntryContractInstance( $type );
        if ( $contract && $elements )
        {
            foreach ( $elements as
                $valid_key =>
                $valid_context )
            {
                if ( !( $valid_context instanceof $contract) )
                {
                    //skip it
                    continue;
                }
                $this->assertFalse( in_array( $valid_context,
                        $test->$getAllMethod()->toArray() ) );
                $this->assertFalse( $test->$hasMethod( $valid_context ) );
                $this->assertFalse( $test->$hasMethod( $valid_context->getContext(),
                        $valid_context->getType() ) );
                $this->assertEquals( $test,
                    $test->$withoutMethod( $valid_context ) );
                $test2 = $test->$withMethod( $valid_context );
                $this->assertTrue( $test2->$hasMethod( $valid_context ) );
                $this->assertTrue( $test2->$hasMethod( $valid_context->getContext(),
                        $valid_context->getType() ) );
                $this->assertFalse( $test->$hasMethod( $valid_context ) );
                $this->assertFalse( $test->$hasMethod( $valid_context->getContext(),
                        $valid_context->getType() ) );
                try
                {
                    $test->$getMethod( $valid_context );
                    $this->assertTrue( false,
                        'Failed to assert that original test object did not receive ' . $type . ' changes' );
                } catch ( \Exception $e )
                {
                    //expected
                }
                try
                {
                    $test->$getMethod( $valid_context->getContext(),
                        $valid_context->getType() );
                    $this->assertTrue( false,
                        'Failed to assert that original test object did not receive ' . $type . ' changes' );
                } catch ( \Exception $e )
                {
                    //expected
                }
                $this->assertEquals( $valid_context,
                    $test2->$getMethod( $valid_context ) );
                $this->assertEquals( $valid_context,
                    $test2->$getMethod( $valid_context->getContext(),
                        $valid_context->getType() ) );
                $test3 = $test2->$withoutMethod( $valid_context );
                $test4 = $test2->$withoutMethod( $valid_context->getContext(),
                    $valid_context->getType() );
                $this->assertEquals( $test3, $test4 );
                $this->assertFalse( $test3->$hasMethod( $valid_context->getContext(),
                        $valid_context->getType() ) );
                $this->assertFalse( $test3->$hasMethod( $valid_context ) );
            }
        }
        /**
         * This test must not be flagged as risky if no context
         * objects exist in the expected set.
         */
        $this->assertTrue( true );
    }

    /**
     * Tests a misconfigured child class that improperly implements
     * the __toString return result.
     * @return bool If this returns false, it did not make any assertions.
     */
    protected function assertMisconfiguredHandled()
    {
        if ( !(__CLASS__ === get_class( $this )) )
        {
            //This method only fires one time. Child classes must override this
            //to provide their own use-case misconfiguration handling, which
            //should always immediately return false if called out of scope
            //to prevent excessive redundant testing overhead.
            return false;
        }
        $class = $this->getMisconfiguredStringableClass();
        $this->enableExpectedErrorHandler();
        //This is expected to fail.
        $result = (string) $class;
        $error = $this->getExpectedError();
        $this->assertTrue( is_array( $error ) && !empty( $error ) );
        $this->assertTrue( strpos( $error['message'],
                '::__toString() must return a string value' ) !== false );
        $this->disableExpectedErrorHandler();
        return true;
    }

    /**
     * Returns a new instance of the default testing class,
     * as defined by the TEST_CLASS class constant.
     *
     * If instantiation parameters do not change, there is no need to
     * override this method. Overriding the class constant with the
     * updated class is sufficient.
     *
     * If the instantiation parameters do change, this method should
     * be overridden to reflect the default testing parameters required
     * for an expected constructor firing correctly.
     *
     * @return \oroboros\core\interfaces\contract\core\context\EntryContract
     */
    protected function getTestClass()
    {
        $class = $this::TEST_CLASS;
        try
        {
            return new $class( $this->getDefaultEntryId(),
                $this->getDefaultEntryTitle(),
                $this->getExpectedContextualContext(),
                $this->getExpectedContextualValue(),
                $this->getExpectedContextualType(),
                $this->getExpectedContextualSubType(),
                $this->getExpectedContextualCategory(),
                $this->getExpectedContextualSubcategory(),
                $this->getExpectedContextualPackage(),
                $this->getExpectedContextualSubPackage(),
                $this->getDefaultEntryMetaData(), $this->getDefaultEntryTags(),
                $this->getDefaultEntryRelations()
            );
        } catch ( \Exception $e )
        {
            $this->renderTestclassError( $this::TEST_CLASS,
                sprintf( 'An exception occurred while loading the context testing class instance: [%s]',
                    $e->getMessage() ), $e->getTrace() );
        }
    }

    /**
     * Returns an intentionally misconfigured class that fails to execute __toString
     * for testing the error handling in the __toString method. PHP does not allow
     * throwing exceptions in this method, which makes error handling in it a chore.
     *
     * @return \oroboros\testclass\core\context\EntryMisconfiguredStringCast
     */
    protected function getMisconfiguredStringableClass()
    {
        return new \oroboros\testclass\core\context\EntryMisconfiguredStringCast(
            $this->getDefaultEntryId(), $this->getDefaultEntryTitle(),
            $this->getExpectedContextualContext(),
            $this->getExpectedContextualValue(),
            $this->getExpectedContextualType(),
            $this->getExpectedContextualSubType(),
            $this->getExpectedContextualCategory(),
            $this->getExpectedContextualSubcategory(),
            $this->getExpectedContextualPackage(),
            $this->getExpectedContextualSubPackage(),
            $this->getDefaultEntryMetaData(), $this->getDefaultEntryTags(),
            $this->getDefaultEntryRelations()
        );
    }

    /**
     * Returns the expected entry id for a new default instance.
     * @return scalar
     */
    protected function getExpectedEntryId()
    {
        return self::$_test_expected_id;
    }

    /**
     * Returns the expected entry title for a new default instance.
     * @return string
     */
    protected function getExpectedEntryTitle()
    {
        return self::$_test_expected_title;
    }

    /**
     * Returns the expected entry metadata for a new default instance.
     * @return scalar|null
     */
    protected function getExpectedEntryMetaData()
    {
        return array();
    }

    /**
     * Returns the expected entry tags for a new default instance.
     * @return scalar|null
     */
    protected function getExpectedEntryTags()
    {
        return array();
    }

    /**
     * Returns the expected entry relations for a new default instance.
     * @return scalar|null
     */
    protected function getExpectedEntryRelations()
    {
        return array();
    }

    /**
     * Returns the expected entry string casting result for a new default instance.
     * @return string
     */
    protected function getExpectedStringableResult()
    {
        return $this->getExpectedEntryTitle();
    }

    /**
     * Returns the default entry id for a new default instance.
     * @return scalar
     */
    protected function getDefaultEntryId()
    {
        return 'default-id';
    }

    /**
     * Returns the default entry title for a new default instance.
     * @return string
     */
    protected function getDefaultEntryTitle()
    {
        return 'Default Entry Title';
    }

    /**
     * Returns the default entry metadata for a new default instance.
     * @return scalar|null
     */
    protected function getDefaultEntryMetaData()
    {
        return array();
    }

    /**
     * Returns the default entry tags for a new default instance.
     * @return scalar|null
     */
    protected function getDefaultEntryTags()
    {
        return array();
    }

    /**
     * Returns the default entry relations for a new default instance.
     * @return scalar|null
     */
    protected function getDefaultEntryRelations()
    {
        return array();
    }

    /**
     * Returns the default entry string casting result for a new default instance.
     * @return string
     */
    protected function getDefaultStringableResult()
    {
        return $this->getDefaultEntryTitle();
    }

    /**
     * Returns a default context object for testing context object parameters
     * as subsitutions of scalar values.
     * @return string
     */
    protected function getDefaultContextObject()
    {
        return new \oroboros\core\internal\context\meta\MetaContext( 'value',
            'foobar' );
    }

    /**
     * Returns the name of the given contract interface if it exists
     * and is a valid interface, otherwise returns false.
     *
     * Tests should skip the logic block that utilizes this value
     * if this method returns false.
     *
     * @param type $type
     * @return string|bool
     */
    protected function getExpectedEntryContractInstance( $type )
    {
        if ( is_scalar( $type ) && array_key_exists( $type,
                self::$_test_contracts ) )
        {
            $contract = self::$_test_contracts[$type];
            if ( interface_exists( $contract, true ) )
            {
                return $contract;
            }
        }
        return false;
    }

    /**
     * Returns a context from the defaults setup before the test class fires its tests.
     * If the given key does not apply, will return false.
     *
     * Tests should skip the logic block that utilizes this value
     * if this method returns false.
     *
     * @param type $type If the test contexts property is an array and
     *     this key exists, will scope the return result to the given type key.
     * @param type $subtype (optional) If provided and the type is an array,
     *     and the subtype is a key of the type array, will return the subtype
     *     key of the type array.
     * @return false|mixed
     */
    protected function getExpectedEntryContextObject( $type, $subtype = null )
    {
        $result = false;
        if ( is_scalar( $type ) && is_array( self::$_test_contexts ) && array_key_exists( $type,
                self::$_test_contexts ) )
        {
            $result = self::$_test_contexts[$type];
            if ( !is_null( $subtype ) && is_scalar( $subtype ) && is_array( $result ) &&
                array_key_exists( $subtype, $result ) )
            {
                $result = $result[$subtype];
            }
        }
        return $result;
    }

    /**
     * Initialize the valid context objects
     * @beforeClass
     */
    public static function bootstrapTestContexts()
    {
        self::$_test_contexts = array(
            'meta' => array(
                //This is a simple meta context object
                //with a default context object contract
                new \oroboros\core\internal\context\meta\MetaContext( 'value',
                    'foobar' )
            ),
            'tag' => array(
                //This is a simple class context context object
                //with a default context object contract
                'class-context' => new \oroboros\core\internal\context\php\classes\ClassContext( \oroboros\Oroboros::OROBOROS_CLASS_CONTEXT,
                    '\\oroboros\\Oroboros' ),
                //This is a class type context object
                //with a default context object contract
                'class-type' => new \oroboros\core\internal\context\oroboros\type\ClassTypeContext( \oroboros\Oroboros::OROBOROS_CLASS_TYPE,
                    'oroboros\\Oroboros' ),
                //This is a class type context object
                //with a default context object contract
                'class-scope' => new \oroboros\core\internal\context\oroboros\scope\ClassScopeContext( \oroboros\Oroboros::OROBOROS_CLASS_SCOPE,
                    '\\oroboros\\Oroboros' ),
            ),
            'relation' => array(
                //This is a simple api interface context object
                //with a default context object contract
                'api-interface' => new \oroboros\core\internal\context\oroboros\interfaces\api\ApiInterfaceContext( 'core-api',
                    '\\oroboros\\core\\interfaces\\api\\core\\CoreApi' ),
                //This is a simple contract interface context object
                //with a default context object contract
                'contract-interface' => new \oroboros\core\internal\context\oroboros\interfaces\contract\ContractInterfaceContext( 'core-contract',
                    '\\oroboros\\core\\interfaces\\contract\\core\\CoreContract' ),
                //This is a simple enumerated interface context object
                //with a default context object contract
                'enum-interface' => new \oroboros\core\internal\context\oroboros\interfaces\enumerated\EnumeratedInterfaceContext( 'core-contract',
                    '\\oroboros\\core\\interfaces\\enumerated\\exception\\ExceptionCode' ),
            ),
        );
    }

    /**
     * Initialize the expected entry internal container contracts.
     * @beforeClass
     */
    public static function bootstrapExpectedContracts()
    {
        self::$_test_contracts = array(
            'meta' => '\\oroboros\\core\\interfaces\\contract\\core\\context\\ContextualContract',
            'tag' => '\\oroboros\\core\\interfaces\\contract\\core\\context\\ContextualContract',
            'relation' => '\\oroboros\\core\\interfaces\\contract\\core\\context\\ContextualContract',
        );
    }

    /**
     * Initialize the expected id.
     * @beforeClass
     */
    public static function bootstrapExpectedId()
    {
        self::$_test_expected_id = 'test-entry';
    }

    /**
     * Initialize the expected id.
     * @beforeClass
     */
    public static function bootstrapExpectedTitle()
    {
        self::$_test_expected_title = 'Oroboros Unit Test Entry Class Instance';
    }

    /**
     * Reset the valid context objects
     * @afterClass
     */
    public static function teardownTestContexts()
    {
        self::$_test_contexts = array();
    }

    /**
     * Reset the expected entry internal container contracts.
     * @afterClass
     */
    public static function teardownTestContracts()
    {
        self::$_test_contracts = array();
    }

    /**
     * Reset the expected id.
     * @afterClass
     */
    public static function teardownExpectedId()
    {
        self::$_test_expected_id = null;
    }

    /**
     * Reset the expected id.
     * @afterClass
     */
    public static function teardownExpectedTitle()
    {
        self::$_test_expected_title = null;
    }

}
