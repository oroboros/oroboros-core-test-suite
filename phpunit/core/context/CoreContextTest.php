<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\core\context;

/**
 * <Core Context Test Cases>
 * These tests prove the auto-setter logic of the core context trait for
 * category and subcategory based on the implemented or declared api interface.
 * @group core
 * @group context
 * @covers \oroboros\core\traits\core\context\CoreContextTrait
 * @covers \oroboros\core\traits\core\context\AutoCategoricalContextTrait
 * @covers \oroboros\core\traits\core\context\JsonSerialContextTrait
 * @covers \oroboros\core\traits\core\context\SerialContextTrait
 * @covers \oroboros\core\traits\core\context\ContextTrait
 * @covers \oroboros\validate\traits\ValidatorTrait
 */
class CoreContextTest
    extends AutoCategoricalTest
{

    /**
     * All objects descending from this line automatically declare package
     * and sub-package dynamically. The test cases must reflect this paradigm
     * to prevent the need to redeclare the expected constant for every single
     * class provided.
     */
    use \oroboros\tests\traits\assertions\context\ContextualCoreEvaluationTrait;
    use \oroboros\tests\traits\tests\context\ContextualCoreTestTrait;

    const TEST_CLASS = '\\oroboros\\testclass\\core\\context\\DefaultTestCoreContext';

    /**
     * Leave this constant null if the sub-category is
     * automatically declared via the CoreContext logic.
     *
     * Override this constant to declare an expected
     * contextual package if the test class also
     * overrides this behavior.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_PACKAGE = null;

    /**
     * Leave this constant null if the sub-category is
     * automatically declared via the CoreContext logic.
     *
     * Override this constant to declare an expected
     * contextual sub-package if the test class also
     * overrides this behavior.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBPACKAGE = null;
}
