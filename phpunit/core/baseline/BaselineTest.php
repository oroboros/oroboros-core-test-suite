<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\core\baseline;

/**
 * <Baseline Test Cases>
 * These tests prove the stable functionality of the BaselineTrait,
 * which is used as the basis of logic for abstract level classes that are instantiated as objects,
 * and most all classes that provide active-state functionality. This trait constitutes the
 * foundation of Oroboros Core object abstraction.
 *
 * This test also covers foundational functionality relied upon universally
 * of some other expected traits, which provide foundational logic to the core.
 * As their stability is required for the baseline internal stability, they are
 * also marked as covered within the scope of their operations with baseline
 * logic, though testing them fully is not the focus of this test class.
 *
 * @group core
 * @group baseline
 * @covers \oroboros\core\traits\core\BaselineTrait
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 */
class BaselineTest
    extends StaticBaselineTest
{

    /**
     * @group core
     * @group baseline
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\OroborosTrait
     */
    public function testObjectInitialization()
    {
        $test = $this->getTestClass();
        $this->assertTrue( true );
    }

    /**
     * @group core
     * @group baseline
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\exception\ExceptionUtilityTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testCleanInitialization()
    {
        try
        {
            $params = $this->_getTestParams();
            $dependencies = $this->_getTestDependencies();
            $flags = $this->_getTestFlags();
            $testclass = new \oroboros\testclass\core\baseline\DefaultBaseline( $params,
                $dependencies, $flags );
            $this->assertTrue( $testclass->isInitialized() );
            $this->assertTrue( $testclass::isStaticInitialized() );
            $this->assertTrue( \oroboros\testclass\core\baseline\DefaultStaticBaseline::isStaticInitialized() );
            $this->assertEquals( $testclass::OROBOROS_API, $testclass->getApi() );
            $this->assertEquals( $testclass::OROBOROS_CLASS_TYPE,
                $testclass->getType() );
            $this->assertEquals( $testclass::OROBOROS_CLASS_SCOPE,
                $testclass->getScope() );
            $this->assertEquals( \oroboros\testclass\core\baseline\DefaultStaticBaseline::OROBOROS_API,
                $testclass->getCompiledApi() );
            $this->assertEquals( \oroboros\testclass\core\baseline\DefaultStaticBaseline::OROBOROS_CLASS_TYPE,
                $testclass->getCompiledType() );
            $this->assertEquals( \oroboros\testclass\core\baseline\DefaultStaticBaseline::OROBOROS_CLASS_SCOPE,
                $testclass->getCompiledScope() );
            $this->assertArrayHasKey( $testclass->getFingerprint(),
                \oroboros\testclass\core\baseline\DefaultStaticBaseline::getActiveInstanceFingerprints() );
            $this->assertTrue( is_string( \oroboros\testclass\core\baseline\DefaultStaticBaseline::getCompiledFingerprint() ) );
            $this->assertEquals( $flags, $testclass->getFlags() );
            $this->assertArraySubset( array(
                'foo' => 'string',
                'bar' => 'stdClass',
                'baz' => 'boolean',
                'bazbar' => 'string'
                ), $testclass->getValidParameters(), true );
            $this->assertArraySubset( array(
                'bazbar' ), $testclass->getRequiredParameters(), true );
            $this->assertArraySubset( array(
                'foo' => 'stdClass',
                'bar' => 'stdClass'
                ), $testclass->getValidDependencies(), true );
            $this->assertArraySubset( array(
                'foo',
                'bar'
                ), $testclass->getRequiredDependencies(), true );
            $this->assertArraySubset( array(
                'foo',
                'bar',
                'baz',
                'bazbarbaz' ), $testclass->getValidFlags(), true );
            $this->assertArraySubset( array(
                'bazbarbaz' ), $testclass->getRequiredFlags(), true );
        } catch ( Exception $e )
        {
            $this->assertTrue( false );
        }
    }

    private function _getTestParams()
    {

        $test_params = array(
            'foo' => 'foo',
            'bar' => new \stdClass(),
            'baz' => true,
            'bazbar' => 'boogity'
        );
        return $test_params;
    }

    private function _getTestDependencies()
    {

        $test_dependencies = array(
            'foo' => new \stdClass(),
            'bar' => new \stdClass(),
            'baz' => new \stdClass(),
        );

        return $test_dependencies;
    }

    private function _getTestFlags()
    {

        $test_flags = array(
            'foo',
            'bar',
            'baz',
            'bazbarbaz',
        );
        return $test_flags;
    }

}
