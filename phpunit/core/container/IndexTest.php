<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\core\container;

/**
 * <Indexed Set Container Test Cases>
 * These tests prove the stable functionality of core indexed set container objects.
 * @group core
 * @group index
 * @group container
 * @group context
 * @covers \oroboros\core\traits\core\container\IndexTrait
 * @covers \oroboros\core\traits\core\context\ContextTrait
 * @covers \oroboros\core\traits\core\container\ContainerSetTrait
 * @covers \oroboros\core\traits\core\container\SetTrait
 * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
 * @covers \oroboros\core\traits\core\container\ContainerTrait
 */
class IndexTest
    extends ConstrainedContainerTest
{

    use \oroboros\tests\traits\assertions\container\IndexContainerEvaluationTrait;
    use \oroboros\tests\traits\tests\container\IndexContainerTestTrait;

    const TEST_CLASS = '\\oroboros\\testclass\\core\\container\\ContainerIndexTestClass';
    const TEST_CLASS_EXPECTED_CONTRACT = '\\oroboros\\core\\interfaces\\contract\\core\\container\\IndexContract';
    const TEST_CLASS_EXPECTED_INTERFACE = '\\Psr\\Container\\ContainerInterface';
    const TEST_CLASS_EXPECTED_API = '\\oroboros\\core\\interfaces\\api\\core\\CoreApi';

    /**
     * The default contextual type for Oroboros index
     * objects is 'index'.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_TYPE = 'unit-test';

    /**
     * Use the default.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBTYPE = null;

    /**
     * The default context for indexes is the meta-type value from
     * the enumerated interface the base logic validates against.
     * This provides a baseline valid scalar string context, which should
     * validate in lieu of a context constraint.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_CONTEXT = 'unit-test';

    /**
     * Provides a default scalar string context value,
     * which should be valid in lieu of a value constraint.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_VALUE = 'test-index';

    /**
     * The default contextual category for indexes should bear the
     * oroboros-context class type as its category.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_CATEGORY = \oroboros\core\interfaces\enumerated\type\CoreClassTypes::CLASS_TYPE_CORE_CONTEXT_OROBOROS;

    /**
     * The default contextual sub-category for indexes is
     * the core class scope designated for abstract indexes.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBCATEGORY = \oroboros\core\interfaces\enumerated\scope\CoreClassScopes::CLASS_SCOPE_CORE_CONTEXT_OROBOROS_CLASS_CONTEXT;

    /**
     * The default package for indexes is the oroboros root namespace
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_PACKAGE = \oroboros\Oroboros::API_CODEX;

    /**
     * The default sub-package for indexes is the core package
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBPACKAGE = \oroboros\Oroboros::API_SCOPE;

    /**
     * The expected contextual type for Oroboros index
     * objects is 'index'.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_TYPE = 'unit-test';

    /**
     * Use the default.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBTYPE = null;

    /**
     * Use the default.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_CONTEXT = 'unit-test';

    /**
     * Use the default.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_VALUE = 'index-unit-test';

    /**
     * Use the default.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_CATEGORY = null;

    /**
     * Use the default.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBCATEGORY = null;

    /**
     * Default indexes should be under the oroboros namespace
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_PACKAGE = \oroboros\Oroboros::API_CODEX;

    /**
     * Default indexes should be part of the core package
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBPACKAGE = \oroboros\Oroboros::API_SCOPE;

    /**
     * Performs assertions to insure that negative constraint checks
     * fire the correct exceptions.
     * @group core
     * @group index
     * @group container
     * @group context
     * @covers \oroboros\core\traits\core\container\IndexTrait
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\core\traits\core\container\ContainerSetTrait
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     * @return void
     */
    public function testIndexFailConstraints()
    {
        if ( !get_class( $this ) === __CLASS__ )
        {
            //Only run this test one time
            //from the root container test instance.
            $this->assertTrue( true );
            return;
        }
        $class = $this->getTestClass();
        $classname = get_class( $class );
        $test_array = array(
            'foo' => array(),
            'bar' => new \stdClass()
        );
        foreach ( $test_array as
            $key =>
            $value )
        {
            try
            {
                //Class MUST raise a ContainerException on an invalid key/value pair.
                $class->offsetSet( $key, $value );
                $this->assertTrue( false,
                    sprintf( 'Failed to throw expected exception [%1$s] for offsetSet '
                        . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                        '\\Psr\\Container\\ContainerExceptionInterface',
                        $classname, __METHOD__, get_class( $this ) )
                );
            } catch ( \PHPUnit\Exception $e )
            {
                //Do now suppress testing framework exceptions
                throw $e;
            } catch ( \Exception $e )
            {
                $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                    $e,
                    sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                        . 'an invalid set key as an instance of [%2$s] for offsetSet '
                        . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                        get_class( $e ),
                        '\\Psr\\Container\\ContainerExceptionInterface',
                        $classname, __METHOD__, get_class( $this ) )
                );
            }
        }
        try
        {
            //Class MUST raise a ContainerException on a duplicate key.
            $class->offsetSet( 'val',
                new \oroboros\core\internal\container\Container() );
            $class->offsetSet( 'val',
                new \oroboros\core\internal\container\Container() );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for duplicate key detection via offsetSet '
                    . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                    . 'an invalid set key as an instance of [%2$s] for offsetSet '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
        $constraint_index = '\\oroboros\\testclass\\core\\container\\ConstrainedIndexTestClass';
        $constraint_valid = '\\oroboros\\testclass\\core\\container\\ConstrainedIndexTestClassValidContext';
        $test = new $constraint_index();
        $valid = new $constraint_valid( $this->getExpectedContextualContext(),
            $this->getExpectedContextualValue() );
        $this->assertTrue( $test->isValidContract( $valid ),
            sprintf( 'Failed to assert true for valid interface [%1$s] when compared to '
                . ' [%2$s] for isValidContract '
                . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                $this->castValueToString( $test->getContract() ),
                get_class( $valid ), get_class( $this ), __METHOD__,
                get_class( $this ) )
        );
        $this->assertTrue( $test->isValidType( $valid ),
            sprintf( 'Failed to assert true for valid contextual type [%1$s] when compared to '
                . ' [%2$s] from [%3$s] for isValidType '
                . 'in instance of [%4$s] in [%5$s] of test class [%6$s]',
                $this->castValueToString( $test->getType() ),
                $this->castValueToString( $valid->getType() ),
                get_class( $valid ), get_class( $this ), __METHOD__,
                get_class( $this ) )
        );
        $this->assertTrue( $test->isValidContext( $valid ),
            sprintf( 'Failed to assert true for valid contextual type [%1$s] when compared to '
                . ' [%2$s] from [%3$s] for isValidContext '
                . 'in instance of [%4$s] in [%5$s] of test class [%6$s]',
                $this->castValueToString( $test->getContext() ),
                $this->castValueToString( $valid->getContext() ),
                get_class( $valid ), get_class( $this ), __METHOD__,
                get_class( $this ) )
        );
        $this->assertTrue( $test->isValidValue( $valid ),
            sprintf( 'Failed to assert true for valid contextual type [%1$s] when compared to '
                . ' [%2$s] from [%3$s] for isValidValue '
                . 'in instance of [%4$s] in [%5$s] of test class [%6$s]',
                $this->castValueToString( $test->getValue() ),
                $this->castValueToString( $valid->getValue() ),
                get_class( $valid ), get_class( $this ), __METHOD__,
                get_class( $this ) )
        );
        $this->assertTrue( $test->isValidCategory( $valid ),
            sprintf( 'Failed to assert true for valid contextual type [%1$s] when compared to '
                . ' [%2$s] from [%3$s] for isValidCategory '
                . 'in instance of [%4$s] in [%5$s] of test class [%6$s]',
                $this->castValueToString( $test->getCategory() ),
                $this->castValueToString( $valid->getCategory() ),
                get_class( $valid ), get_class( $this ), __METHOD__,
                get_class( $this ) )
        );
        $this->assertTrue( $test->isValidSubcategory( $valid ),
            sprintf( 'Failed to assert true for valid contextual type [%1$s] when compared to '
                . ' [%2$s] from [%3$s] for isValidSubcategory '
                . 'in instance of [%4$s] in [%5$s] of test class [%6$s]',
                $this->castValueToString( $test->getSubcategory() ),
                $this->castValueToString( $valid->getSubcategory() ),
                get_class( $valid ), get_class( $this ), __METHOD__,
                get_class( $this ) )
        );
        $test['valid'] = $test;
        try
        {
            $invalid = new $constraint_valid( 'invalid-context', 'invalid-value' );
            $test['invalid'] = $invalid;
            //Class MUST raise a ContainerException on a failing contextual constraint key.
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for failing contextual constraint via offsetSet '
                    . 'in instance of [%2$s] for invalid contextual key in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface',
                    $constraint_index, __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                    . 'an invalid set key as an instance of [%2$s] for offsetSet '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface',
                    $constraint_index, __METHOD__, get_class( $this ) )
            );
        }
    }

}
