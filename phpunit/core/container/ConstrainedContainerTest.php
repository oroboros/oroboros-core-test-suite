<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\core\container;

/**
 * <Container Test Cases>
 * These tests prove the stable functionality of core container objects.
 * @group core
 * @group container
 * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
 * @covers \oroboros\core\traits\core\container\ContainerTrait
 */
class ConstrainedContainerTest
    extends ContainerTest
{

    use \oroboros\tests\traits\assertions\container\ConstrainedContainerEvaluationTrait;
    use \oroboros\tests\traits\tests\container\ConstrainedContainerTestTrait;

    const TEST_CLASS = '\\oroboros\\core\\internal\\container\\DefaultConstrainedContainer';
    const TEST_CLASS_EXPECTED_CONTRACT = '\\oroboros\\core\\interfaces\\contract\\core\\container\\ConstrainedContainerContract';
    const TEST_CLASS_EXPECTED_INTERFACE = '\\Psr\\Container\\ContainerInterface';
    const TEST_CLASS_EXPECTED_API = '\\oroboros\\core\\interfaces\\api\\core\\CoreApi';

    /**
     * Performs assertions to insure that negative constraint checks
     * fire the correct exceptions.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     * @return void
     */
    public function testConstrainedContainerFailConstraints()
    {
        if ( !get_class( $this ) === __CLASS__ )
        {
            //Only run this test one time
            //from the root container test instance.
            $this->assertTrue( true );
            return;
        }
        //Test invalid key assignment
        //Test class always refuses keys
        $invalid = '\\oroboros\\testclass\\core\\container\\ConstrainedContainerTestRefuseKeys';
        $class = new $invalid();
        $this->assertConstraintsValid( $class );
        //Test invalid value assignment
        //Test class always refuses values
        $invalid = '\\oroboros\\testclass\\core\\container\\ConstrainedContainerTestRefuseValues';
        $class = new $invalid();
        $this->assertConstraintsValid( $class );
    }

}
