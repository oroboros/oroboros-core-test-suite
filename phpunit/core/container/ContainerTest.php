<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\core\container;

/**
 * <Container Test Cases>
 * These tests prove the stable functionality of core container objects.
 * @group core
 * @group container
 * @covers \oroboros\core\traits\core\container\ContainerTrait
 */
class ContainerTest
    extends \oroboros\tests\AbstractTestClass
{

    use \oroboros\tests\traits\assertions\container\ContainerEvaluationTrait;
    use \oroboros\tests\traits\tests\container\ContainerTestTrait;

    const TEST_CLASS = '\\oroboros\\core\\internal\\container\\Container';
    const TEST_CLASS_EXPECTED_CONTRACT = '\\oroboros\\core\\interfaces\\contract\\core\\container\\ContainerContract';
    const TEST_CLASS_EXPECTED_INTERFACE = '\\Psr\\Container\\ContainerInterface';
    const TEST_CLASS_EXPECTED_API = '\\oroboros\\core\\interfaces\\api\\core\\CoreApi';

    /**
     * Performs assertions to insure that misconfigured containers still fire
     * the internal exception hook and also cast bad exceptions to a container
     * exception. This test only needs to run one time from the root container
     * instance.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     * @return void
     */
    public function testContainerInvalidInstance()
    {
        if ( !get_class( $this ) === __CLASS__ )
        {
            //Only run this test one time
            //from the root container test instance.
            $this->assertTrue( true );
            return;
        }
        $invalid = '\\oroboros\\testclass\\core\\container\\ContainerTestMisconfigured';
        try
        {
            $fail = new $invalid();
            $fail['foo'] = 'foo value';
            //This method is expected to fail, but still recast to a container
            //exception, and also fire the _onException hook.
            $fail->get( 'foo' );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for misconfigured offsetGet '
                    . 'in instance of [%2$s] in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface', $invalid,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected exception [%1$s] occurred on '
                    . 'a misconfigured class getter as an instance of [%2$s] for offsetGet '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface', $invalid,
                    __METHOD__, get_class( $this ) )
            );
        }
    }

}
