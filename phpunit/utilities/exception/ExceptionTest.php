<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\utilities\exception;

/**
 * @covers \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
 * @covers \oroboros\core\traits\utilities\exception\ExceptionTrait
 * @covers \oroboros\core\traits\core\BaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 */
class ExceptionTest
    extends \oroboros\tests\AbstractTestClass
{

    const TEST_MESSAGE = 'Testing Exceptions';

    /**
     * Tests the extenstion of \Exception
     * @covers \oroboros\core\traits\utilities\exception\ExceptionTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @group exceptions
     */
    public function testExcepton()
    {
        try
        {
            throw new \oroboros\core\utilities\exception\Exception( self::TEST_MESSAGE );
        } catch ( \oroboros\core\utilities\exception\Exception $e )
        {
            $this->assertTrue( is_string( $e->getMessage() ) && $e->getMessage()
                === self::TEST_MESSAGE && is_int( $e->getCode() ) && is_array( $e->getTrace() ) );
        }
    }

    /**
     * Tests the extenstion of \BadFunctionCallException
     * @covers \oroboros\core\traits\utilities\exception\ExceptionTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @group exceptions
     */
    public function testBadFunctionCallExcepton()
    {
        try
        {
            throw new \oroboros\core\utilities\exception\BadFunctionCallException( self::TEST_MESSAGE );
        } catch ( \oroboros\core\utilities\exception\BadFunctionCallException $e )
        {
            $this->assertTrue( is_string( $e->getMessage() ) && $e->getMessage()
                === self::TEST_MESSAGE && is_int( $e->getCode() ) && is_array( $e->getTrace() ) );
        }
    }

    /**
     * Tests the extenstion of \BadMethodCallException
     * @covers \oroboros\core\traits\utilities\exception\ExceptionTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @group exceptions
     */
    public function testBadMethodCallExcepton()
    {
        try
        {
            throw new \oroboros\core\utilities\exception\BadMethodCallException( self::TEST_MESSAGE );
        } catch ( \oroboros\core\utilities\exception\BadMethodCallException $e )
        {
            $this->assertTrue( is_string( $e->getMessage() ) && $e->getMessage()
                === self::TEST_MESSAGE && is_int( $e->getCode() ) && is_array( $e->getTrace() ) );
        }
    }

    /**
     * Tests the extenstion of \DomainException
     * @covers \oroboros\core\traits\utilities\exception\ExceptionTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @group exceptions
     */
    public function testDomainException()
    {
        try
        {
            throw new \oroboros\core\utilities\exception\DomainException( self::TEST_MESSAGE );
        } catch ( \oroboros\core\utilities\exception\DomainException $e )
        {
            $this->assertTrue( is_string( $e->getMessage() ) && $e->getMessage()
                === self::TEST_MESSAGE && is_int( $e->getCode() ) && is_array( $e->getTrace() ) );
        }
    }

    /**
     * Tests the extenstion of \InvalidArgumentException
     * @covers \oroboros\core\traits\utilities\exception\ExceptionTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @group exceptions
     */
    public function testInvalidArgumentException()
    {
        try
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException( self::TEST_MESSAGE );
        } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
        {
            $this->assertTrue( is_string( $e->getMessage() ) && $e->getMessage()
                === self::TEST_MESSAGE && is_int( $e->getCode() ) && is_array( $e->getTrace() ) );
        }
    }

    /**
     * Tests the extenstion of \LengthException
     * @covers \oroboros\core\traits\utilities\exception\ExceptionTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @group exceptions
     */
    public function testLengthException()
    {
        try
        {
            throw new \oroboros\core\utilities\exception\LengthException( self::TEST_MESSAGE );
        } catch ( \oroboros\core\utilities\exception\LengthException $e )
        {
            $this->assertTrue( is_string( $e->getMessage() ) && $e->getMessage()
                === self::TEST_MESSAGE && is_int( $e->getCode() ) && is_array( $e->getTrace() ) );
        }
    }

    /**
     * Tests the extenstion of \LogicException
     * @covers \oroboros\core\traits\utilities\exception\ExceptionTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @group exceptions
     */
    public function testLogicException()
    {
        try
        {
            throw new \oroboros\core\utilities\exception\LogicException( self::TEST_MESSAGE );
        } catch ( \oroboros\core\utilities\exception\LogicException $e )
        {
            $this->assertTrue( is_string( $e->getMessage() ) && $e->getMessage()
                === self::TEST_MESSAGE && is_int( $e->getCode() ) && is_array( $e->getTrace() ) );
        }
    }

    /**
     * Tests the extenstion of \OutOfBoundsException
     * @covers \oroboros\core\traits\utilities\exception\ExceptionTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @group exceptions
     */
    public function testOutOfBoundsException()
    {
        try
        {
            throw new \oroboros\core\utilities\exception\OutOfBoundsException( self::TEST_MESSAGE );
        } catch ( \oroboros\core\utilities\exception\OutOfBoundsException $e )
        {
            $this->assertTrue( is_string( $e->getMessage() ) && $e->getMessage()
                === self::TEST_MESSAGE && is_int( $e->getCode() ) && is_array( $e->getTrace() ) );
        }
    }

    /**
     * Tests the extenstion of \OutOfRangeException
     * @covers \oroboros\core\traits\utilities\exception\ExceptionTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @group exceptions
     */
    public function testOutOfRangeException()
    {
        try
        {
            throw new \oroboros\core\utilities\exception\OutOfRangeException( self::TEST_MESSAGE );
        } catch ( \oroboros\core\utilities\exception\OutOfRangeException $e )
        {
            $this->assertTrue( is_string( $e->getMessage() ) && $e->getMessage()
                === self::TEST_MESSAGE && is_int( $e->getCode() ) && is_array( $e->getTrace() ) );
        }
    }

    /**
     * Tests the extenstion of \OverflowException
     * @covers \oroboros\core\traits\utilities\exception\ExceptionTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @group exceptions
     */
    public function testOverflowException()
    {
        try
        {
            throw new \oroboros\core\utilities\exception\OverflowException( self::TEST_MESSAGE );
        } catch ( \oroboros\core\utilities\exception\OverflowException $e )
        {
            $this->assertTrue( is_string( $e->getMessage() ) && $e->getMessage()
                === self::TEST_MESSAGE && is_int( $e->getCode() ) && is_array( $e->getTrace() ) );
        }
    }

    /**
     * Tests the extenstion of \RangeException
     * @covers \oroboros\core\traits\utilities\exception\ExceptionTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @group exceptions
     */
    public function testRangeException()
    {
        try
        {
            throw new \oroboros\core\utilities\exception\RangeException( self::TEST_MESSAGE );
        } catch ( \oroboros\core\utilities\exception\RangeException $e )
        {
            $this->assertTrue( is_string( $e->getMessage() ) && $e->getMessage()
                === self::TEST_MESSAGE && is_int( $e->getCode() ) && is_array( $e->getTrace() ) );
        }
    }

    /**
     * Tests the extenstion of \RuntimeException
     * @covers \oroboros\core\traits\utilities\exception\ExceptionTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @group exceptions
     */
    public function testRuntimeException()
    {
        try
        {
            throw new \oroboros\core\utilities\exception\RuntimeException( self::TEST_MESSAGE );
        } catch ( \oroboros\core\utilities\exception\RuntimeException $e )
        {
            $this->assertTrue( is_string( $e->getMessage() ) && $e->getMessage()
                === self::TEST_MESSAGE && is_int( $e->getCode() ) && is_array( $e->getTrace() ) );
        }
    }

    /**
     * Tests the extenstion of \UnderflowException
     * @covers \oroboros\core\traits\utilities\exception\ExceptionTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @group exceptions
     */
    public function testUnderflowException()
    {
        try
        {
            throw new \oroboros\core\utilities\exception\UnderflowException( self::TEST_MESSAGE );
        } catch ( \oroboros\core\utilities\exception\UnderflowException $e )
        {
            $this->assertTrue( is_string( $e->getMessage() ) && $e->getMessage()
                === self::TEST_MESSAGE && is_int( $e->getCode() ) && is_array( $e->getTrace() ) );
        }
    }

    /**
     * Tests the extenstion of \UnexpectedValueException
     * @covers \oroboros\core\traits\utilities\exception\ExceptionTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @group exceptions
     */
    public function testUnexpectedValueExceptionn()
    {
        try
        {
            throw new \oroboros\core\utilities\exception\UnexpectedValueException( self::TEST_MESSAGE );
        } catch ( \oroboros\core\utilities\exception\UnexpectedValueException $e )
        {
            $this->assertTrue( is_string( $e->getMessage() ) && $e->getMessage()
                === self::TEST_MESSAGE && is_int( $e->getCode() ) && is_array( $e->getTrace() ) );
        }
    }

}
