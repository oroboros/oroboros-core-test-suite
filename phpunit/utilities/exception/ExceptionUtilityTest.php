<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\utilities\exception;

/**
 * @covers \oroboros\core\traits\utilities\exception\ExceptionUtilityTrait
 */
class ExceptionUtilityTest
    extends \oroboros\tests\AbstractTestClass
{

    const TEST_CLASS = '\\oroboros\\testclass\\utilities\\exception\\DefaultExceptionUtilityTester';

    private static $_code_identifiers = array();

    /**
     * @group exception
     * @covers \oroboros\core\traits\utilities\exception\ExceptionUtilityTrait
     */
    public function testCodes()
    {
        $this->_setupTestingCodes();
        $test = $this->getTestClass();
        $this->assertEquals( 0, $test::getCode() );
        $this->assertEquals( 0, $test::getCode( 'not-a-real-code' ) );
        $const_base = '\\oroboros\\core\\interfaces\\enumerated\\exception\\ExceptionCode::ERROR_';
        foreach ( self::$_code_identifiers as
            $code )
        {
            $const = $const_base . str_replace( '-', '_', strtoupper( $code ) );
            $result = $test::getCode( $code );
            $this->assertEquals( $result, constant( $const ) );
            $this->assertEquals( $code, $test::getCodeIdentifier( $result ),
                sprintf( 'Exception code constant [%s] should equal the result of '
                    . 'getTestCodeIdentifier for canonicalized constant value of code [%s]. Expected result: [%s], provided result: [%s].',
                    $const, $result, $code, $test::getCodeIdentifier( $result ) ) );
        }
        $this->assertFalse( $test::getCodeIdentifier( -1 ) );
    }

    /**
     * @group exception
     * @covers \oroboros\core\traits\utilities\exception\ExceptionUtilityTrait
     */
    public function testMessages()
    {
        $this->_setupTestingCodes();
        $test = $this->getTestClass();
        $const_base = '\\oroboros\\core\\interfaces\\enumerated\\exception\\ExceptionMessage::ERROR_';
        $this->assertEquals( constant( $const_base . 'UNKNOWN_MESSAGE' ),
            $test::getMessage() );
        $this->assertEquals( constant( $const_base . 'UNKNOWN_MESSAGE' ),
            $test::getMessage( 'not-a-real-code' ) );
        foreach ( self::$_code_identifiers as
            $code )
        {
            $const = $const_base . str_replace( '-', '_', strtoupper( $code ) ) . '_MESSAGE';
            if ( defined( $const ) )
            {
                $this->assertEquals( $test::getMessage( $code ),
                    constant( $const ) );
            }
        }
    }

    /**
     * @group exception
     * @covers \oroboros\core\traits\utilities\exception\ExceptionUtilityTrait
     */
    public function testExceptions()
    {
        $this->_setupTestingCodes();
        $test = $this->getTestClass();
        foreach ( self::$_code_identifiers as
            $exception_identifier )
        {
            $exception = $test::getException( $exception_identifier );
            $this->assertInstanceOf( '\\oroboros\\core\\interfaces\\contract\\utilities\\exception\\ExceptionContract',
                $exception );
        }
        $external = new \oroboros\testclass\utilities\exception\ExceptionUtilityExternalTester();
        $external();
        try
        {
            throw $test::recastException( new \Exception( 'This is a test' ) );
            throw new \Exception( 'Failed to throw expected exception.' );
        } catch ( \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract $e )
        {
            $this->assertInstanceOf( '\\oroboros\\core\\interfaces\\contract\\utilities\\exception\\ExceptionContract',
                $e );
        }
        try
        {
            throw $test::recastException( new \Exception( 'This is a test' ),
                'InvalidArgumentException' );
            throw new \Exception( 'Failed to throw expected exception.' );
        } catch ( \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract $e )
        {
            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\InvalidArgumentException',
                $e );
        }
        try
        {
            throw $test::recastException( new \Exception( 'This is a test' ),
                'invalid-argument-exception' );
            throw new \Exception( 'Failed to throw expected exception.' );
        } catch ( \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract $e )
        {
            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\InvalidArgumentException',
                $e );
        }
        try
        {
            $test::throwException( 'invalid-argument', 'This is a test',
                'logic-bad-parameters' );
            throw new \Exception( 'Failed to throw expected exception.' );
        } catch ( \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract $e )
        {
            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\InvalidArgumentException',
                $e );
            $this->assertEquals( \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS,
                $e->getCode() );
        }
    }

    private function _setupTestingCodes()
    {
        if ( is_null( self::$_code_identifiers ) )
        {
            $test = $this->getTestClass();
            self::$_code_identifiers = $test::getExceptionIdentifiers();
        }
    }

}
