<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\utilities\parser;

use PHPUnit\Framework\TestCase;

/**
 * @group parser
 */
class ClassConstantDocBlockTest
    extends TestCase
{

    use \oroboros\parse\traits\ClassConstantDocBlockTrait
    {
        //disable the constructor.
        __construct as private _unused;
    }

    const OROBOROS_CLASS_SCOPE = 'foo';
    const OROBOROS_CLASS_TYPE = 'bar';

    public function __construct( $name = null, array $data = array(),
        $dataName = '' )
    {
        parent::__construct( $name, $data, $dataName );
    }

    /**
     * @group parser
     * @covers \oroboros\parse\traits\ClassConstantDocBlockTrait
     * @covers \oroboros\parse\abstracts\AbstractClassConstantDocBlock
     * @covers \oroboros\parse\ClassConstantDocBlock
     */
    public function testParseClassConstantDocBlocks()
    {
        $string = $this->_getTestString();
        $parser = new \oroboros\parse\ClassConstantDocBlock( $string );
        $this->_assertations( $parser, $string );
    }


//    /**
//     * @group parser
//     * @covers \oroboros\parse\traits\ParserTrait
//     * @covers \oroboros\parse\traits\JsonTrait
//     * @covers \oroboros\parse\abstracts\AbstractJson
//     * @covers \oroboros\parse\Json
//     */
//    public function testParseInvalid()
//    {
//        try
//        {
//            //Booleans are not acceptable values
//            $parser = new \oroboros\parse\Json( false );
//            throw new \Exception( 'Parser failed to catch expected failure.' );
//        } catch ( \Exception $e )
//        {
//            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\InvalidArgumentException',
//                $e );
//            $this->assertEquals( \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_BAD_PARAMETERS,
//                $e->getCode() );
//        }
//        try
//        {
//            //Objects are not acceptable values
//            $parser = new \oroboros\parse\Json( new \stdClass() );
//            throw new \Exception( 'Parser failed to catch expected failure.' );
//        } catch ( \Exception $e )
//        {
//            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\InvalidArgumentException',
//                $e );
//            $this->assertEquals( \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_BAD_PARAMETERS,
//                $e->getCode() );
//        }
//        try
//        {
//            //Nulls are not acceptable values
//            $parser = new \oroboros\parse\Json( null );
//            throw new \Exception( 'Parser failed to catch expected failure.' );
//        } catch ( \Exception $e )
//        {
//            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\InvalidArgumentException',
//                $e );
//            $this->assertEquals( \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_BAD_PARAMETERS,
//                $e->getCode() );
//        }
//        try
//        {
//            //Integers are not acceptable values
//            $parser = new \oroboros\parse\Json( 56 );
//            throw new \Exception( 'Parser failed to catch expected failure.' );
//        } catch ( \Exception $e )
//        {
//            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\InvalidArgumentException',
//                $e );
//            $this->assertEquals( \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_BAD_PARAMETERS,
//                $e->getCode() );
//        }
//        try
//        {
//            //Floats are not acceptable values
//            $parser = new \oroboros\parse\Json( 56.1 );
//            throw new \Exception( 'Parser failed to catch expected failure.' );
//        } catch ( \Exception $e )
//        {
//            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\InvalidArgumentException',
//                $e );
//            $this->assertEquals( \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_BAD_PARAMETERS,
//                $e->getCode() );
//        }
//    }
//
//    /**
//     * @group parser
//     * @covers \oroboros\parse\traits\ParserTrait
//     */
//    public function testParserInternals()
//    {
//        try
//        {
//            //Testing scope already set
//            $this->_parser_scope = 'fail';
//            $this->_setParserScope( null );
//            throw new \Exception( 'Parser failed to catch expected failure.' );
//        } catch ( \Exception $e )
//        {
//            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\LogicException',
//                $e );
//            $this->assertEquals( \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC,
//                $e->getCode() );
//        }
//        try
//        {
//            //Testing bad scope
//            $this->_parser_scope = null;
//            $this->_setParserScope( new \stdClass() );
//            throw new \Exception( 'Parser failed to catch expected failure.' );
//        } catch ( \Exception $e )
//        {
//            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\InvalidArgumentException',
//                $e );
//            $this->assertEquals( \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS,
//                $e->getCode() );
//        }
//        try
//        {
//            //Testing bad parse called internally - object
//            $this->_parserParse( new \stdClass() );
//            throw new \Exception( 'Parser failed to catch expected failure.' );
//        } catch ( \Exception $e )
//        {
//            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\DomainException',
//                $e );
//            $this->assertEquals( \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_BAD_PARAMETERS,
//                $e->getCode() );
//        }
//        try
//        {
//            //Testing bad parse called internally - other type
//            $this->_parserParse( false );
//            throw new \Exception( 'Parser failed to catch expected failure.' );
//        } catch ( \Exception $e )
//        {
//            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\DomainException',
//                $e );
//            $this->assertEquals( \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_BAD_PARAMETERS,
//                $e->getCode() );
//        }
//        try
//        {
//            //Testing bad array conversion called internally - other type
//            $this->_parserConvertArray( false );
//            throw new \Exception( 'Parser failed to catch expected failure.' );
//        } catch ( \Exception $e )
//        {
//            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\DomainException',
//                $e );
//            $this->assertEquals( \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE_PARSER_FAILURE,
//                $e->getCode() );
//        }
//        try
//        {
//            //Testing bad string conversion called internally - other type
//            $this->_parserConvertString( false );
//            throw new \Exception( 'Parser failed to catch expected failure.' );
//        } catch ( \Exception $e )
//        {
//            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\DomainException',
//                $e );
//            $this->assertEquals( \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE_PARSER_FAILURE,
//                $e->getCode() );
//        }
//        try
//        {
//            //Testing bad resource conversion called internally - other type
//            $this->_parserConvertResource( false );
//            throw new \Exception( 'Parser failed to catch expected failure.' );
//        } catch ( \Exception $e )
//        {
//            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\DomainException',
//                $e );
//            $this->assertEquals( \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE_PARSER_FAILURE,
//                $e->getCode() );
//        }
//        try
//        {
//            //Testing bad resource conversion called internally - other type
//            $this->_parserConvertStream( false );
//            throw new \Exception( 'Parser failed to catch expected failure.' );
//        } catch ( \Exception $e )
//        {
//            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\DomainException',
//                $e );
//            $this->assertEquals( \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE_PARSER_FAILURE,
//                $e->getCode() );
//        }
//        try
//        {
//            //Testing bad filename conversion called internally - missing file
//            $this->_parserConvertFile( 'NotAFile' );
//            throw new \Exception( 'Parser failed to catch expected failure.' );
//        } catch ( \Exception $e )
//        {
//            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\DomainException',
//                $e );
//            $this->assertEquals( \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE_PARSER_FAILURE,
//                $e->getCode() );
//        }
//        try
//        {
//            $testfile = OROBOROS_TEST_ASSETS . 'sample_unreadable.txt';
//            if ( !file_exists( $testfile ) && !is_readable( $testfile) )
//            {
//                $fh = fopen( $testfile, 'w' );
//                fclose( $fh );
//                `chmod -r $testfile`;
//            }
//        } catch ( \Exception $e )
//        {
//            die( sprintf( "Could not create test [%s] file for checking unreadable file",
//                    $testfile ) );
//        }
//        try
//        {
//            //Testing bad filename conversion called internally - unreadable file
//            $this->_parserConvertFile( $testfile );
//            throw new \Exception( 'Parser failed to catch expected failure.' );
//        } catch ( \Exception $e )
//        {
//            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\DomainException',
//                $e );
//            $this->assertEquals( \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE_PARSER_FAILURE,
//                $e->getCode() );
//        }
//        $this->_parser_is_parsed = true;
//        $this->assertNull($this->_parserCast());
//    }
//
    private function _assertations( $parser, $subject )
    {
        $this->assertInstanceOf( '\\oroboros\\parse\\interfaces\\contract\\ParserContract',
            $parser );
        $this->assertTrue( is_array( $parser->getParsed()->toArray() ) );
        $this->assertTrue( is_array( $parser->getParsed( true )->toArray() ) );
        $this->assertStringStartsWith( '/**', $parser->cast() );
        $this->assertNull( $parser->parse() );
        $this->assertNull( $parser->reset() );
//        $this->assertEquals( json_encode( json_decode( $this->_getTestString(),
//                    1 ) ), json_encode( $parser->getParsed()->toArray() ) );
//        $this->assertEquals( json_encode( json_decode( $this->_getTestString(),
//                    1 ) ), json_encode( $parser->getParsed( true )->toArray() ) );
//        $this->assertEquals( json_encode( json_decode( $this->_getTestString(),
//                    1 ) ), $parser->cast() );
//        $this->assertEquals( json_decode( $this->_getTestString(), 1 ),
//            $parser->getParsed()->toArray() );
        $this->assertEquals( $subject, $parser->getOriginal() );
        $this->assertInstanceOf( '\\oroboros\\collection\\interfaces\\contract\\CollectionContract',
            $parser->getParsed() );
    }

    /**
     * Returns an array of testing entries for the LexiconEntry
     * @return array
     */
    private function _getTestString()
    {
        return '\\oroboros\\core\\interfaces\\api\\standards\\psr\\Psr7Api';
    }

    /**
     * Returns an array of testing entries for the LexiconEntry
     * @return array
     */
    private function _getTestMalformedString()
    {
        return file_get_contents( OROBOROS_TEST_ASSETS . 'sample_malformed.json' );
    }

    //required by trait
    protected function _parseData( $data )
    {
        ;
    }

    //required by trait
    protected function _castData( $data )
    {
        ;
    }

}
