<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\traits\tests\container;

/**
 * <Oroboros Core Container Test Trait>
 * Provides tests that will automatically fire to
 * assert integrity of container objects.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category traits
 * @category unit-testing
 * @category tests
 * @package oroboros/tests
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait ContainerTestTrait
{

    protected $_test_values = array(
        'foo' => 'foo value',
        'bar' => 'bar value',
        'baz' => 'baz value',
        'quux' => 'quux value'
    );

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainer()
    {
        $class = $this->getTestClass();
        $this->assertContractInterfaceValid( $class );
        $this->assertInterfaceValid( $class );
        $this->assertApiInterfaceValid( $class );
        $classname = get_class( $class );
        try
        {
            //Testing invalid parameter injection into constructor
            $fail = new $classname( new \stdClass() );
            $this->assertFalse( true,
                sprintf( 'Failed to throw expected ContainerExceptionInterface when '
                    . 'instantiated with invalid parameters getter get() '
                    . 'for instance of [%1$s] in '
                    . '[%2$s] of test class [%3$s]', $classname, __METHOD__,
                    get_class( $this ) ) );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Psr\Container\ContainerExceptionInterface $e )
        {
            //expected
            $this->assertTrue( true );
        }
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerSerializable()
    {
        $class = $this->getTestClass();
        $this->assertSerializableValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerJsonSerializable()
    {
        $class = $this->getTestClass();
        $this->assertJsonSerializableValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerIterable()
    {
        $class = $this->getTestClass();
        $this->assertIterableValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerCountable()
    {
        $class = $this->getTestClass();
        $this->assertCountableValid( $class );
    }

    /**
     * Tests context matching for resizing, truncation of excess,
     * and disallowing overflow setting.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerResize()
    {
        $class = $this->getTestClass();
        $this->assertResizeKeyIntegrity( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerArrayAccess()
    {
        $class = $this->getTestClass();
        $this->assertArrayAccessValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerPsr11()
    {
        $class = $this->getTestClass();
        $this->assertPsr11Valid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerSizeable()
    {
        $class = $this->getTestClass();
        $this->assertSizeableValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerPairs()
    {
        $class = $this->getTestClass();
        $this->assertPairsValid( $class );
    }

    /**
     * Tests equivalent interaction between arrays and object representation.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testArrayObjectInterchangable()
    {
        $class = $this->getTestClass();
        $this->assertArrayObjectSubstitution( $class );
    }

    /**
     * Performs assertions to insure that the given class is serializable
     * @param \oroboros\core\interfaces\contract\core\container\ContainerContract $class
     * @return void
     */
    protected function assertSerializableValid( $class )
    {
        $classname = get_class( $class );
        $interface = '\\Serializable';
        $this->assertInstanceOf( '\\Serializable', $class,
            sprintf( 'Failed to assert that [%1$s] is an instance of '
                . '[%2$s] in [%3$s] of test class [%4$s]', $classname,
                $interface, __METHOD__, get_class( $this ) ) );
        $array_assoc = $this->getTestAssociativeArray();
        $array_indexed = $this->getTestIndexedArray();
        $test = $class->fromArray( $array_assoc );
        $serial = serialize( $test );
        $unserial = unserialize( $serial );
        $this->assertEquals( $array_assoc, $unserial->toArray(),
            sprintf( 'Failed to assert that [%1$s] has identical array '
                . 'schema after unserialization of associative array '
                . 'in [%2$s] of test class [%3$s]', $classname, __METHOD__,
                get_class( $this ) )
        );
        $test = $class->fromArray( $array_indexed );
        $serial = serialize( $test );
        $unserial = unserialize( $serial );
        $this->assertEquals( $array_indexed, $unserial->toArray(),
            sprintf( 'Failed to assert that [%1$s] has identical array '
                . 'schema after unserialization of indexed array '
                . 'in [%2$s] of test class [%3$s]', $classname, __METHOD__,
                get_class( $this ) )
        );
    }

    /**
     * Performs assertions to insure that the given class is unserializable
     * @param \oroboros\core\interfaces\contract\core\container\ContainerContract $class
     * @return void
     */
    protected function assertJsonSerializableValid( $class )
    {
        $classname = get_class( $class );
        $interface = '\\JsonSerializable';
        $this->assertInstanceOf( '\\Serializable', $class,
            sprintf( 'Failed to assert that [%1$s] is an instance of '
                . '[%2$s] in [%3$s] of test class [%4$s]', $classname,
                $interface, __METHOD__, get_class( $this ) ) );
        $array_assoc = $this->getTestAssociativeArray();
        $array_indexed = $this->getTestIndexedArray();
        $test = $class->fromArray( $array_assoc );
        $serial = json_encode( $test );
        $this->assertEquals( 'string', gettype( $serial ),
            sprintf( 'Failed to assert return value of jsonSerialize is a string for [%1$s] '
                . 'schema after json decoding of associative array '
                . 'in [%2$s] of test class [%3$s]. Received [%4$s] but expected [%5$s].',
                $classname, __METHOD__, get_class( $this ), gettype( $serial ),
                'string' )
        );
        $unserial = json_decode( $serial, 1 );
        foreach ( $test as
            $key =>
            $value )
        {
            //Json Decode cannot restore objects or resources.
            //This assertion must be skipped in those cases.
            if ( !is_object( $test[$key] ) && !is_resource( $test[$key] ) )
            {
                $this->assertEquals( $test[$key], $unserial[$key],
                    sprintf( 'Failed to assert that [%1$s] has identical array '
                        . 'schema after json encoding and decoding of associative array '
                        . 'in [%2$s] of test class [%3$s]', $classname,
                        __METHOD__, get_class( $this ) )
                );
            }
        }
        $test = $class->fromArray( $array_indexed );
        $serial = json_encode( $test );
        $this->assertEquals( 'string', gettype( $serial ),
            sprintf( 'Failed to assert return value of jsonSerialize is a string for [%1$s] '
                . 'schema after json decoding of indexed array '
                . 'in [%2$s] of test class [%3$s]. Received [%4$s] but expected [%5$s].',
                $classname, __METHOD__, get_class( $this ), gettype( $serial ),
                'string' )
        );
        $unserial = json_decode( $serial, 1 );
        foreach ( $test as
            $key =>
            $value )
        {
            //Json Decode cannot restore objects or resources.
            //This assertion must be skipped in those cases.
            if ( !is_object( $test[$key] ) && !is_resource( $test[$key] ) )
            {
                $this->assertEquals( $test[$key], $unserial[$key],
                    sprintf( 'Failed to assert that [%1$s] has identical array '
                        . 'schema after json encoding and decoding of associative array '
                        . 'in [%2$s] of test class [%3$s]', $classname,
                        __METHOD__, get_class( $this ) )
                );
            }
        }
    }

    /**
     * Performs assertions to insure that the given class is iterable/traversable
     * @param \oroboros\core\interfaces\contract\core\container\ContainerContract $class
     * @return void
     */
    protected function assertIterableValid( $class )
    {
        $classname = get_class( $class );
        $interface = '\\Iterator';
        $this->assertInstanceOf( '\\Serializable', $class,
            sprintf( 'Failed to assert that [%1$s] is an instance of '
                . '[%2$s] in [%3$s] of test class [%4$s]', $classname,
                $interface, __METHOD__, get_class( $this ) ) );
        $array_assoc = $this->getTestAssociativeArray();
        $array_indexed = $this->getTestIndexedArray();
        $test = $class->fromArray( $array_assoc );
        foreach ( $test as
            $key =>
            $value )
        {
            $this->assertEquals( $array_assoc[$key], $test[$key],
                sprintf( 'Failed to assert that iteration key [%1$s] has identical value '
                    . 'to [%2$s] during iteration of associative array '
                    . 'in [%3$s] of test class [%4$s]', $key, $classname,
                    __METHOD__, get_class( $this ) )
            );
            $this->assertEquals( $key, array_search( $value, $array_assoc ),
                sprintf( 'Failed to assert that key [%1$s] has identical value '
                    . 'to [%2$s] during iteration of associative array for [%3$s] '
                    . 'in [%4$s] of test class [%5$s]', $key,
                    array_search( $key, $array_assoc ), $classname, __METHOD__,
                    get_class( $this ) )
            );
        }
        $test->rewind();
        foreach ( $array_assoc as
            $key =>
            $value )
        {
            $this->assertEquals( $value, $test[$key],
                sprintf( 'Failed to assert that iteration key [%1$s] has identical value '
                    . 'to [%2$s] during iteration of associative array '
                    . 'in [%3$s] of test class [%4$s]', $key, $classname,
                    __METHOD__, get_class( $this ) )
            );
            unset( $array_assoc[$key] );
            unset( $test[$key] );
        }
        $test = $class->fromArray( $array_indexed );
        foreach ( $test as
            $key =>
            $value )
        {
            $this->assertEquals( $array_indexed[$key], $test[$key],
                sprintf( 'Failed to assert that iteration key [%1$s] has identical value '
                    . 'to [%2$s] during iteration of indexed array '
                    . 'in [%2$s] of test class [%3$s]', $key, $classname,
                    __METHOD__, get_class( $this ) )
            );
            $this->assertEquals( $key, array_search( $value, $array_indexed ),
                sprintf( 'Failed to assert that key [%1$s] has identical value '
                    . 'to [%2$s] during iteration of indexed array for [%3$s] '
                    . 'in [%4$s] of test class [%5$s]', $key,
                    array_search( $value, $array_indexed ), $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
    }

    /**
     * Performs assertions to insure that the given class is countable
     * @param \oroboros\core\interfaces\contract\core\container\ContainerContract $class
     * @return void
     */
    protected function assertCountableValid( $class )
    {
        $classname = get_class( $class );
        $interface = '\\Countable';
        $this->assertInstanceOf( '\\Serializable', $class,
            sprintf( 'Failed to assert that [%1$s] is an instance of '
                . '[%2$s] in [%3$s] of test class [%4$s]', $classname,
                $interface, __METHOD__, get_class( $this ) ) );
        $array_assoc = $this->getTestAssociativeArray();
        $array_indexed = $this->getTestIndexedArray();
        $test = $class->fromArray( $array_assoc );
        $this->assertEquals( count( $array_assoc ), count( $test ),
            sprintf( 'Failed to assert that count of [%1$s] has identical value '
                . 'to count of test array [%2$s] during iteration of indexed array '
                . 'in [%3$s] of test class [%4$s]', get_class( $test ),
                count( $array_assoc ), __METHOD__, get_class( $this ) )
        );
        $test = $class->fromArray( $array_indexed );
        $this->assertEquals( count( $array_indexed ), count( $test ),
            sprintf( 'Failed to assert that count of [%1$s] has identical value '
                . 'to count of test array [%2$s] during iteration of indexed array '
                . 'in [%3$s] of test class [%4$s]', get_class( $test ),
                count( $array_indexed ), __METHOD__, get_class( $this ) )
        );
    }

    /**
     * Performs assertions to insure that the size is not exceeded when
     * values are added or size is changed.
     * @param \oroboros\core\interfaces\contract\core\container\ContainerContract $class
     * @return void
     */
    protected function assertResizeKeyIntegrity( $class )
    {
        $classname = get_class( $class );
        $test_array = $this->getTestAssociativeArray();
        $test_pairs = $this->getTestPairs();
        $redux = count( $test_array ) - 1;
        /**
         * @todo test resize functions for all methods that add values,
         *     test size of -1 does not restrict, and test that setSize drops
         *     excess values
         */
        try
        {
            $test = new $classname();
            $test->setSize( $redux );
            foreach ( $test_array as
                $key =>
                $value )
            {
                $test[$key] = $value;
            }
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected exception [%1$s] occurred on '
                    . 'an invalid size constrained set operation as instance of [%2$s]'
                    . ' for method [%3$s] in instance of [%4$s] in [%5$s] of test class [%6$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface',
                    'offsetSet', $classname, __METHOD__, get_class( $this ) )
            );
        }
        try
        {
            $test = new $classname();
            $test->fromPairs( $test_pairs, $redux );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected exception [%1$s] occurred on '
                    . 'an invalid size constrained set operation as instance of [%2$s]'
                    . ' for method [%3$s] in instance of [%4$s] in [%5$s] of test class [%6$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface',
                    'fromPairs', $classname, __METHOD__, get_class( $this ) )
            );
        }
        try
        {
            $test = new $classname();
            $test->setSize( $redux );
            $test = $test->fromArray( $test_array );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected exception [%1$s] occurred on '
                    . 'an invalid size constrained set operation as instance of [%2$s]'
                    . ' for method [%3$s] in instance of [%4$s] in [%5$s] of test class [%6$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface',
                    'fromArray', $classname, __METHOD__, get_class( $this ) )
            );
        }
        try
        {
            $test = new $classname();
            $test2 = $test->fromArray( $test_array );
            $test->setSize( $redux );
            $test_serial = unserialize( $test2->serialize() );
            $test_serial['size'] = 2;
            $test->unserialize( serialize( $test_serial ) );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected exception [%1$s] occurred on '
                    . 'an invalid size constrained set operation as instance of [%2$s]'
                    . ' for method [%3$s] in instance of [%4$s] in [%5$s] of test class [%6$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface',
                    'unserialize', $classname, __METHOD__, get_class( $this ) )
            );
        }
        $test = new $classname();
        $test = $test->fromArray( $test_array );
        $test->setSize( $redux );
        $this->assertEquals( $redux, count( $test ),
            sprintf( 'Failed to '
                . 'dropping of redundant keys on resize of [%1$s] '
                . ' expected count [%2$s] but received [%3$s] '
                . 'in [%4$s] of test class [%5$s]', $classname, $redux,
                count( $test ), 'setSize', __METHOD__, get_class( $this ) ) );
    }

    /**
     * Performs assertions to insure that the given class is array accessible
     * @param \oroboros\core\interfaces\contract\core\container\ContainerContract $class
     * @return void
     */
    protected function assertArrayAccessValid( $class )
    {
        $classname = get_class( $class );
        $interface = '\\ArrayAccess';
        $this->assertInstanceOf( '\\Serializable', $class,
            sprintf( 'Failed to assert that [%1$s] is an instance of '
                . '[%2$s] in [%3$s] of test class [%4$s]', $classname,
                $interface, __METHOD__, get_class( $this ) ) );
        $badkey = sha1( microtime() . get_class( $this ) );
        $array_assoc = $this->getTestAssociativeArray();
        $array_indexed = $this->getTestIndexedArray();
        $test_key = key( $array_assoc );
        $test_value = $array_assoc[$test_key];
        if ( !$class->has( $test_key ) )
        {
            $class[$test_key] = $test_value;
        }
        try
        {
            $class->offsetGet( null );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for offsetGet '
                    . 'in instance of [%2$s] in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected exception [%1$s] occurred on '
                    . 'an invalid scalar key as an instance of [%2$s] for offsetGet '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
        try
        {
            $class->offsetSet( null, null );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for offsetSet '
                    . 'in instance of [%2$s] in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected exception [%1$s] occurred on '
                    . 'an invalid scalar key as an instance of [%2$s] for offsetSet '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
        try
        {
            $class->offsetUnset( null );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for offsetUnset '
                    . 'in instance of [%2$s] in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected exception [%1$s] occurred on '
                    . 'an invalid scalar key as an instance of [%2$s] for offsetUnset '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
        try
        {
            $class->offsetExists( null );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for offsetExists '
                    . 'in instance of [%2$s] in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected exception [%1$s] occurred on '
                    . 'an invalid scalar key as an instance of [%2$s] for offsetExists '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
        $this->assertEquals( $test_value, $class[$test_key],
            sprintf( 'Failed to assert offsetSet of key [%1$s] is equal to '
                . 'expected value [%2$s] in instance of [%3$s] in [%4$s] of '
                . 'test class [%5$s]', $test_key,
                $this->castValueToString( $test_value ), $classname, __METHOD__,
                get_class( $this ) ) );
        unset( $class[$test_key] );
        $this->assertFalse( isset( $class[$test_key] ),
            sprintf( 'Failed to assert offsetUnset of key [%1$s] was '
                . 'successfully removed in instance of [%2$s] in [%3$s] of '
                . 'test class [%4$s]', $test_key, $classname, __METHOD__,
                get_class( $this ) ) );
        $test = $class->fromArray( $array_assoc );
        try
        {
            $class->fromArray( null );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for fromArray '
                    . 'in instance of [%2$s] in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected exception [%1$s] occurred on '
                    . 'an invalid scalar key as an instance of [%2$s] for fromArray '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
        $this->assertFalse( isset( $test[$badkey] ),
            sprintf( 'Failed to assert isset of randomly generated key [%1$s] is false '
                . 'in instance of [%2$s] in [%3$s] of test class [%4$s]',
                $badkey, $classname, __METHOD__, get_class( $this ) ) );
        foreach ( $array_assoc as
            $key =>
            $value )
        {
            $this->assertTrue( isset( $test[$key] ),
                sprintf( 'Failed to assert isset of key [%1$s] is true '
                    . 'in instance of [%2$s] in [%3$s] of test class [%4$s]',
                    $key, $classname, __METHOD__, get_class( $this ) ) );
            $this->assertEquals( $value, $test[$key],
                sprintf( 'Failed to assert that value of key [%1$s] has identical value '
                    . 'to instance of [%2$s] during \\ArrayAccess testing of associative array '
                    . 'in [%3$s] of test class [%4$s]', $key,
                    get_class( $test ), __METHOD__, get_class( $this ) )
            );
        }
        $test = $class->fromArray( $array_indexed );
        foreach ( $array_indexed as
            $key =>
            $value )
        {
            $this->assertTrue( isset( $test[$key] ),
                sprintf( 'Failed to assert isset of key [%1$s] is true '
                    . 'in instance of [%2$s] in [%3$s] of test class [%4$s]',
                    $key, $classname, __METHOD__, get_class( $this ) ) );
            $this->assertEquals( $value, $test[$key],
                sprintf( 'Failed to assert that value of key [%1$s] has identical value '
                    . 'to instance of [%2$s] during \\ArrayAccess testing of indexed array '
                    . 'in [%3$s] of test class [%4$s]', $key,
                    get_class( $test ), __METHOD__, get_class( $this ) )
            );
        }
    }

    /**
     * Performs assertions to insure that the given class is Psr-11 compliant
     * @param \oroboros\core\interfaces\contract\core\container\ContainerContract $class
     * @return void
     */
    protected function assertPsr11Valid( $class )
    {
        $classname = get_class( $class );
        $interface = '\\Psr\\Container\\ContainerInterface';
        $this->assertInstanceOf( '\\Serializable', $class,
            sprintf( 'Failed to assert that [%1$s] is an instance of '
                . '[%2$s] in [%3$s] of test class [%4$s]', $classname,
                $interface, __METHOD__, get_class( $this ) ) );
        $test_array = $this->getTestAssociativeArray();
        foreach ( $test_array as
            $key =>
            $value )
        {
            $class[$key] = $value;
            $stringable = $this->getStringableObject( $key );
            $this->assertTrue( $class->has( $stringable ),
                sprintf( 'Failed to assert that Psr-11 check method has() '
                    . 'returned true for stringable object [%1$s] that returns '
                    . 'value valid value [%2$s] for [%3$s] in [%4$s] of test '
                    . 'class [%5$s]', get_class( $stringable ),
                    (string) $stringable, $classname, __METHOD__,
                    get_class( $this ) )
            );
            $this->assertTrue( $class->has( $key ),
                sprintf( 'Failed to assert that Psr-11 check method has() '
                    . 'returned true for expected key [%1$s] is an instance of '
                    . '[%2$s] in [%3$s] of test class [%4$s]', $key, $classname,
                    __METHOD__, get_class( $this ) )
            );
            $this->assertEquals( $value, $class->get( $stringable ),
                sprintf( 'Failed to assert that Psr-11 getter method get() '
                    . 'returned an identical result for expected key [%1$s] '
                    . 'presented by stringable object [%2$s] is an instance of '
                    . '[%3$s] in [%4$s] of test class [%5$s]', $key,
                    get_class( $stringable ), $classname, __METHOD__,
                    get_class( $this ) )
            );
            $this->assertEquals( $value, $class->get( $key ),
                sprintf( 'Failed to assert that Psr-11 getter method get() '
                    . 'returned an identical result for expected key [%1$s] is an instance of '
                    . '[%2$s] in [%3$s] of test class [%4$s]', $key, $classname,
                    __METHOD__, get_class( $this ) )
            );
            unset( $class[$key] );
            $this->assertFalse( $class->has( $key ),
                sprintf( 'Failed to assert that Psr-11 check method has() '
                    . 'returned false for unset key [%1$s] is an instance of '
                    . '[%2$s] in [%3$s] of test class [%4$s]', $key, $classname,
                    __METHOD__, get_class( $this ) )
            );
            try
            {
                $v = $class->get( $key );
                $this->assertFalse( true,
                    sprintf( 'Failed to throw expected NotFoundException when '
                        . 'key [%1$s] is unset and then obtained via Psr-11 '
                        . 'getter get() for instance of [%2$s] in '
                        . '[%3$s] of test class [%4$s]', $key, $classname,
                        __METHOD__, get_class( $this ) ) );
            } catch ( \PHPUnit\Exception $e )
            {
                //Do now suppress testing framework exceptions
                throw $e;
            } catch ( \Psr\Container\NotFoundExceptionInterface $e )
            {
                //expected
            } catch ( \Exception $e )
            {
                $this->assertFalse( true,
                    sprintf( 'Failed to assert that expected exception [%1$s] is an '
                        . 'instance of \\Psr\\Container\\NotFoundExceptionInterface '
                        . 'when requesting an unset key represented as both array '
                        . '[ $object[$key] ] and object [ $object->$key ] notation '
                        . 'for instance of [%2$s] in '
                        . '[%3$s] of test class [%4$s]. Received exception [%5$s] with message [%6$s].',
                        $key, $classname, __METHOD__, get_class( $this ),
                        get_class( $e ), $e->getMessage() ) );
            }
        }
        try
        {
            $v = $class->get( null );
            $this->assertFalse( true,
                sprintf( 'Failed to throw expected ContainerExceptionInterface '
                    . 'instance when requesting an invalid non-scalar key via Psr-11 '
                    . 'getter get() for instance of [%1$s] in '
                    . '[%2$s] of test class [%3$s]', $classname, __METHOD__,
                    get_class( $this ) ) );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Psr\Container\ContainerExceptionInterface $e )
        {
            //expected
        } catch ( \Exception $e )
        {
            $this->assertFalse( true,
                sprintf( 'Failed to assert that expected exception [%1$s] is an '
                    . 'instance of \\Psr\\Container\\NotFoundExceptionInterface '
                    . 'when requesting an unset key represented as both array '
                    . '[ $object[$key] ] and object [ $object->$key ] notation '
                    . 'for instance of [%2$s] in '
                    . '[%3$s] of test class [%4$s]. Received exception [%5$s] with message [%6$s].',
                    $key, $classname, __METHOD__, get_class( $this ),
                    get_class( $e ), $e->getMessage() ) );
        }
    }

    /**
     * Performs assertions to insure that array and object interaction are interchangeable.
     * @param \oroboros\core\interfaces\contract\core\container\ContainerContract $class
     * @return void
     */
    protected function assertArrayObjectSubstitution( $class )
    {
        $classname = get_class( $class );
        $test_array = $this->getTestAssociativeArray();
        foreach ( $test_array as
            $key =>
            $value )
        {
            $class->$key = $value;
            $this->assertEquals( $class->$key, $class[$key],
                sprintf( 'Failed to assert that key [%1$s] is equal when '
                    . 'represented as both array [ $object[$key] ] and object '
                    . '[ $object->$key ] notation for instance of [%2$s] in '
                    . '[%3$s] of test class [%4$s]', $key, $classname,
                    __METHOD__, get_class( $this ) ) );
            $this->assertTrue( isset( $class->$key ),
                sprintf( 'Failed to assert that key [%1$s] isset in object notation when '
                    . 'represented as both array [ $object[$key] ] and object '
                    . '[ $object->$key ] notation for instance of [%2$s] in '
                    . '[%3$s] of test class [%4$s]', $key, $classname,
                    __METHOD__, get_class( $this ) ) );
            $this->assertTrue( isset( $class[$key] ),
                sprintf( 'Failed to assert that key [%1$s] isset in array notation when '
                    . 'represented as both array [ $object[$key] ] and object '
                    . '[ $object->$key ] notation for instance of [%2$s] in '
                    . '[%3$s] of test class [%4$s]', $key, $classname,
                    __METHOD__, get_class( $this ) ) );
            unset( $class->$key );
            $this->assertFalse( isset( $class[$key] ),
                sprintf( 'Failed to assert that key [%1$s] is unset when '
                    . 'represented as both array [ $object[$key] ] and object '
                    . '[ $object->$key ] notation for instance of [%2$s] in '
                    . '[%3$s] of test class [%4$s]', $key, $classname,
                    __METHOD__, get_class( $this ) ) );
            $this->assertFalse( isset( $class->$key ),
                sprintf( 'Failed to assert that key [%1$s] is unset when '
                    . 'represented as both array [ $object[$key] ] and object '
                    . '[ $object->$key ] notation for instance of [%2$s] in '
                    . '[%3$s] of test class [%4$s]', $key, $classname,
                    __METHOD__, get_class( $this ) ) );
            try
            {
                $value = $class->$key;
                $this->assertFalse( true,
                    sprintf( 'Failed to assert that key [%1$s] is unset when '
                        . 'represented as both array [ $object[$key] ] and object '
                        . '[ $object->$key ] notation for instance of [%2$s] in '
                        . '[%3$s] of test class [%4$s]', $key, $classname,
                        __METHOD__, get_class( $this ) ) );
            } catch ( \PHPUnit\Exception $e )
            {
                //Do now suppress testing framework exceptions
                throw $e;
            } catch ( \Psr\Container\NotFoundExceptionInterface $e )
            {
                //expected
            } catch ( \Exception $e )
            {
                $this->assertFalse( true,
                    sprintf( 'Failed to assert that expected exception [%1$s] is an '
                        . 'instance of \\Psr\\Container\\NotFoundExceptionInterface '
                        . 'when requesting an unset key represented as both array '
                        . '[ $object[$key] ] and object [ $object->$key ] notation '
                        . 'for instance of [%2$s] in '
                        . '[%3$s] of test class [%4$s]. Received exception [%5$s] with message [%6$s].',
                        $key, $classname, __METHOD__, get_class( $this ),
                        get_class( $e ), $e->getMessage() ) );
            }
        }
    }

    /**
     * Performs assertions to insure that the given class is sizeable
     * @param \oroboros\core\interfaces\contract\core\container\ContainerContract $class
     * @return void
     */
    protected function assertSizeableValid( $class )
    {
        $this->assertTrue( true );
        $classname = get_class( $class );
        try
        {
            $class->setSize( null );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for setSize '
                    . 'in instance of [%2$s] in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected exception [%1$s] occurred on '
                    . 'an invalid scalar key as an instance of [%2$s] for setSize '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
    }

    /**
     * Performs assertions to insure that the given class correctly
     * implements asPairs and fromPairs
     * @param \oroboros\core\interfaces\contract\core\container\ContainerContract $class
     * @return void
     */
    protected function assertPairsValid( $class )
    {
        $classname = get_class( $class );
        $array = $this->getTestAssociativeArray();
        $pairs = $this->getTestPairs();
        $test = $class->fromPairs( $pairs );
        try
        {
            $class->fromPairs( null );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for fromPairs '
                    . 'in instance of [%2$s] in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected exception [%1$s] occurred on '
                    . 'an invalid scalar key as an instance of [%2$s] for fromPairs '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
        foreach ( $test as
            $key =>
            $value )
        {
            $this->assertEquals( $value, $array[$key],
                sprintf( 'Failed to assert that fromPairs key [%1$s] has identical value '
                    . 'to [%2$s] during iteration of associative array '
                    . 'in [%3$s] in [%4$s] of test class [%5$s]. Expected [%6$s], received [%7$s].',
                    $key, $this->castValueToString( $array[$key] ), $classname,
                    __METHOD__, get_class( $this ),
                    $this->castValueToString( $array[$key] ),
                    $this->castValueToString( $value ) )
            );
            $this->assertEquals( array_search( $value, $array ), $key,
                sprintf( 'Failed to assert that fromPairs key [%1$s] has identical value '
                    . 'to [%2$s] during iteration of associative array for [%3$s] '
                    . 'in [%4$s] of test class [%5$s]. Expected [%6$s], received [%7$s].',
                    $key, array_search( $key, $array ), $classname, __METHOD__,
                    get_class( $this ), array_search( $value, $array ), $key )
            );
        }
        $test_pairs = $test->asPairs();
        $this->assertEquals( $pairs, $test_pairs,
            sprintf( 'Failed to assert that asPairs produced an identical '
                . 'result to the test set it was created from for instance '
                . 'of [%1$s] in [%2$s] of test class [%3$s]. Expected [%4$s], received [%5$s].',
                $classname, __METHOD__, get_class( $this ),
                print_r( $pairs, 1 ), print_r( $test_pairs, 1 ) )
        );
    }

    /**
     * Returns a test array for checking fromArray
     * @return array
     */
    protected function getTestAssociativeArray()
    {
        $array = $this->_test_values;
        ksort( $array );
        return $array;
    }

    /**
     * Returns a test array for checking fromArray
     * @return array
     */
    protected function getTestIndexedArray()
    {
        $values = array();
        foreach ( $this->getTestAssociativeArray() as
            $value )
        {
            $values[] = $value;
        }
        return $values;
    }

    /**
     * Returns a test array for checking fromPairs
     * @return array
     */
    protected function getTestPairs()
    {
        $pairs = array();
        foreach ( $this->getTestAssociativeArray() as
            $key =>
            $value )
        {
            $pairs[] = array(
                $key,
                $value );
        }
        return $pairs;
    }

    /**
     * Returns a test serialized set for testing manual unserialization
     * @return string
     */
    protected function getTestSerialData()
    {
        return serialize( array(
            'size' => -1,
            'values' => $this->getTestArray() ) );
    }

}
