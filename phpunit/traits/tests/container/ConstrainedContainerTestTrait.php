<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\traits\tests\container;

/**
 * <Oroboros Core Constrained Container Test Trait>
 * Provides tests that will automatically fire to
 * assert integrity of constrained container objects.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category traits
 * @category unit-testing
 * @category tests
 * @package oroboros/tests
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait ConstrainedContainerTestTrait
{

    protected $_test_values = array(
        'foo' => 'foo value',
        'bar' => 'bar value',
        'baz' => 'baz value',
        'quux' => 'quux value'
    );

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainer()
    {
        $class = $this->getTestClass();
        $this->assertContractInterfaceValid( $class );
        $this->assertInterfaceValid( $class );
        $this->assertApiInterfaceValid( $class );
        $classname = get_class( $class );
        try
        {
            //Testing invalid parameter injection into constructor
            $fail = new $classname( new \stdClass() );
            $this->assertFalse( true,
                sprintf( 'Failed to throw expected ContainerExceptionInterface when '
                    . 'instantiated with invalid parameters getter get() '
                    . 'for instance of [%1$s] in '
                    . '[%2$s] of test class [%3$s]', $classname, __METHOD__,
                    get_class( $this ) ) );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Psr\Container\ContainerExceptionInterface $e )
        {
            //expected
            $this->assertTrue( true );
        }
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerSerializable()
    {
        $class = $this->getTestClass();
        $this->assertSerializableValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerJsonSerializable()
    {
        $class = $this->getTestClass();
        $this->assertJsonSerializableValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerIterable()
    {
        $class = $this->getTestClass();
        $this->assertIterableValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerCountable()
    {
        $class = $this->getTestClass();
        $this->assertCountableValid( $class );
    }

    /**
     * Tests context matching for resizing, truncation of excess,
     * and disallowing overflow setting.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerResize()
    {
        $class = $this->getTestClass();
        $this->assertResizeKeyIntegrity( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerArrayAccess()
    {
        $class = $this->getTestClass();
        $this->assertArrayAccessValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerPsr11()
    {
        $class = $this->getTestClass();
        $this->assertPsr11Valid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerSizeable()
    {
        $class = $this->getTestClass();
        $this->assertSizeableValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerPairs()
    {
        $class = $this->getTestClass();
        $this->assertPairsValid( $class );
    }

    /**
     * Tests equivalent interaction between arrays and object representation.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testArrayObjectInterchangable()
    {
        $class = $this->getTestClass();
        $this->assertArrayObjectSubstitution( $class );
    }

    /**
     * Tests key and value constraints for containers.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerConstraints()
    {
        $class = $this->getTestClass();
        $this->assertConstraintsValid( $class );
    }

    /**
     * Performs assertions to insure that the given class correctly
     * implements constraint checks
     * @param \oroboros\core\interfaces\contract\core\container\ConstrainedContainerContract $class
     * @return void
     */
    protected function assertConstraintsValid( $class )
    {
        $classname = get_class( $class );
        //We will use infinite sizing to
        //rule out any expected size errors, which would
        //otherwise potentially cause false positives.
        $class->setSize( -1 );
        $array = $this->getTestAssociativeArray();
        foreach ( $array as
            $key =>
            $value )
        {
            $stringable = $this->getStringableObject( $key );
            $class->isValid( $stringable, $value );
            if ( $class->isValid( $key, $value ) )
            {
                try
                {
                    //Class MUST NOT raise an exception if no isValid returns
                    //true and no size constraint is in place that prevents
                    //setting additional values.
                    $class->offsetSet( $key, $value );
                    //Expected behavior
                    $this->assertTrue( true );
                } catch ( \PHPUnit\Exception $e )
                {
                    //Do now suppress testing framework exceptions
                    throw $e;
                } catch ( \Exception $e )
                {
                    $this->assertTrue( false,
                        sprintf( 'Failed to allow setting of valid key [%1$s] for offsetSet '
                            . 'in instance of [%2$s] in [%3$s] of test class [%4$s]',
                            $key, $classname, __METHOD__, get_class( $this ) )
                    );
                }
            } else
            {
                try
                {
                    //Class MUST raise a ContainerException on an invalid key/value pair.
                    $class->offsetSet( $key, $value );
                    $this->assertTrue( false,
                        sprintf( 'Failed to throw expected exception [%1$s] for offsetSet '
                            . 'in instance of [%2$s] in [%3$s] of test class [%4$s]',
                            '\\Psr\\Container\\ContainerExceptionInterface',
                            $classname, __METHOD__, get_class( $this ) )
                    );
                } catch ( \PHPUnit\Exception $e )
                {
                    //Do now suppress testing framework exceptions
                    throw $e;
                } catch ( \Exception $e )
                {
                    $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                        $e,
                        sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                            . 'an invalid key as an instance of [%2$s] for offsetSet '
                            . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                            get_class( $e ),
                            '\\Psr\\Container\\ContainerExceptionInterface',
                            $classname, __METHOD__, get_class( $this ) )
                    );
                }
            }
        }
    }

}
