<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\traits\assertions\integrity;

/**
 * <Oroboros Core Class Integrity Evaluation Trait>
 * Provides assertions for insuring that the universal class declarations for
 * Oroboros classes are defined as expected. This trait will setup all known
 * currently acceptable parameters for insuring the integrity of classes that
 * exist in or extend upon Oroboros Core.
 *
 * This trait checks flags that may enable or disable specific integrity
 * checks via the declaration of class constants in the test class.
 *
 * The nulled flag defaults are defined via class constants in
 * \oroboros\tests\AbstractTestClass
 *
 * They may be overridden from any extension of that abstract to
 * enable or disable integrity checks as needed on a per-test-class basis.
 *
 * This trait provides assertion methods for verifying integrity. All of these
 * may be safely called if the related flag is set to false. In that case,
 * they will perform one single non-failing assertion to prevent a test from
 * being marked as risky and return without any additional assertions.
 *
 * You may implement the Oroboros class api in a mock testing
 * object by extending one of the base abstract classes in the majority of
 * cases, which provides all of the required integrity to pass all of these
 * checks, and do not otherwise make any assumption about environment or
 * logic flow.
 *
 * Eg: a mock object used in a test extending from
 * \oroboros\tests\libraries\LibraryTest
 *
 * Should extend
 * \oroboros\core\abstracts\libraries\AbstractLibrary
 *
 * This will insure that all baseline integrity is valid despite
 * not using a core class for testing. It rarely matters if core classes
 * are used, as all Oroboros logic is derived from traits. The classes only
 * implement interfaces, use traits, and declare constants, and do not
 * otherwise declare methods or properties.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category traits
 * @category unit-testing
 * @category assertions
 * @package oroboros/tests
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 * @see \oroboros\tests\AbstractTestClass
 */
trait OroborosClassIntegrityTrait
{

    use ClassConstantEvaluationTrait;
    use InterfaceEvaluationTrait;

    /**
     * Switch that designates that the class designation should be checked.
     * If true, an integrity assertion will be made to insure that the class
     * provides all expected indirect context assertions, and that it fulfills
     * the base contract interface for any it marks as true.
     * @var bool
     */
    private static $_oroboros_check_class_designation = false;

    /**
     * Switch that designates that the class context should be checked.
     * If true, an integrity assertion will be made to insure that the class
     * declares a class context, and that it exists within the set of expected
     * values.
     * @var bool
     */
    private static $_oroboros_check_class_context = false;

    /**
     * Switch that designates that the class type should be checked.
     * If true, an integrity assertion will be made to insure that the class
     * declares a class type, and that it exists within the set of expected
     * values.
     * @var bool
     */
    private static $_oroboros_check_class_type = false;

    /**
     * Switch that designates that the class scope should be checked.
     * If true, an integrity assertion will be made to insure that the class
     * declares a class scope, and that it exists within the set of expected
     * values.
     * @var bool
     */
    private static $_oroboros_check_class_scope = false;

    /**
     * Switch that designates that the class contract, api, and enumerated
     * interfaces should themselves be validated as valid extensions of the
     * base interface for each of their categories. This validates both the
     * test class and the testing subject. If the test class itself fails,
     * an error will be raised. If the testing subject fails, the test will
     * be failed but it will not result in an error.
     * @var bool
     */
    private static $_oroboros_check_interface_type = false;

    /**
     * Switch that designates that the class should be checked against the
     * provided set of 3rd party or PHP internal interfaces.
     * If true, an integrity assertion will be made that the class honors all
     * of the expected 3rd party or PHP native interfaces provided.
     * @var bool
     */
    private static $_oroboros_check_interfaces = false;

    /**
     * Switch that designates that the class api should be checked.
     * If true, an integrity assertion will be made to insure that the class
     * declared api exists and provides all of the expected values for a
     * valid api interface.
     * @var bool
     */
    private static $_oroboros_check_class_api = false;

    /**
     * Switch that designates that the class contract interface should be checked.
     * If true, an integrity assertion will be made to insure that the class honors
     * the expected contract, and that the expected contract lists all public
     * methods in the class either directly or indirectly.
     * @var bool
     */
    private static $_oroboros_check_class_contract = false;

    /**
     * Switch that designates that the class enumerated api should be checked.
     * If true, an integrity assertion will be made to insure that the provided
     * enumerator is valid and accounts for expected values.
     * @var bool
     */
    private static $_oroboros_check_class_enumerator = false;

    /**
     * Designates the valid class context indirect designations.
     * The given testing class MUST assert true for all of these that are true,
     * but may assert true for ones that are false in this set and still
     * be considered valid. This will be skipped if the set is empty,
     * or if class designation checking is disabled.
     * @var array
     */
    private static $_oroboros_class_designations = array();

    /**
     * Designates a set of valid class contexts for the current testing context.
     * The testing class MUST declare a class context that exists in this set if
     * class context integrity checks are enabled. This will be skipped if this
     * set is empty, or if class context integrity checking is disabled.
     * @var array
     */
    private static $_oroboros_class_contexts = array();

    /**
     * Designates a set of valid class types for the current testing context.
     * The testing class MUST declare a class type that exists in this set if
     * class type integrity checks are enabled. This will be skipped if this
     * set is empty, or if class type integrity checking is disabled.
     * @var array
     */
    private static $_oroboros_class_types = array();

    /**
     * Designates a set of valid class scopes for the current testing context.
     * The testing class MUST declare a class scope that exists in this set if
     * class scope integrity checks are enabled. This will be skipped if this
     * set is empty, or if class scope integrity checking is disabled.
     * @var array
     */
    private static $_oroboros_class_scopes = array();

    /**
     * Designates a set of root level oroboros interface types that testing
     * class interfaces must adhere to. Both the declared expected interface
     * as well as the testing class must honor these for any category checked.
     * If the test class declared expected interface does not honor it, an
     * error will be raised. If the testing class subject does not honor it,
     * the test will be failed.
     *
     * These insure that leaf extension follows an orderly process of interface
     * extension that mirrors the layers of class logic.
     *
     * @var array
     */
    private static $_oroboros_expected_baseline_interfaces = array();

    /**
     * Designates a set of expected interfaces that the testing class must honor.
     * If ANY of these are not implemented by the class, the test will fail.
     * @var array
     */
    private static $_oroboros_expected_interfaces = array();

    /**
     * Designates a set of expected interfaces that the class declared
     * interfaces must honor.
     * If ANY of these are not implemented by the class, the test will fail.
     * If the declared expected contract, enumerated, or api interfaces of
     * the test class do not honor these baseline level interfaces, the
     * test will exit with an error.
     * @var array
     */
    private static $_oroboros_baseline_interface_types = array();

    /**
     * Designates the class context literal that the
     * provided testing class MUST declare.
     * @var bool
     */
    private static $_oroboros_expected_class_context = false;

    /**
     * Designates the class type literal that the
     * provided testing class MUST declare.
     * @var bool
     */
    private static $_oroboros_expected_class_type = false;

    /**
     * Designates the class scope literal that the
     * provided testing class MUST declare.
     * @var bool
     */
    private static $_oroboros_expected_class_scope = false;

    /**
     * Designates the contract interface literal that the
     * provided testing class MUST be an instance of.
     * @var bool
     */
    private static $_oroboros_expected_contract = false;

    /**
     * Designates the api interface literal that the
     * provided testing class MUST either be an instance of,
     * or declare by class constant.
     * @var bool
     */
    private static $_oroboros_expected_api = false;

    /**
     * Designates the enumerated interface literal that the
     * provided testing class MUST be an instance of.
     * @var bool
     */
    private static $_oroboros_expected_enumerator = false;

    /**
     * May be overridden at the test class level. If true, indicates that the
     * given test class MUST designate itself indirectly or directly as a Core Internal.
     *
     * This test will be skipped if designations are disabled for
     * the given test class.
     *
     * @var bool
     */
    private static $_oroboros_expected_core_designation = false;

    /**
     * May be overridden at the test class level. If true, indicates that the
     * given test class MUST designate itself indirectly or directly as a Library.
     *
     * This test will be skipped if designations are disabled for
     * the given test class.
     *
     * @var bool
     */
    private static $_oroboros_expected_library_designation = false;

    /**
     * May be overridden at the test class level. If true, indicates that the
     * given test class MUST designate itself indirectly or directly as a Utility.
     *
     * This test will be skipped if designations are disabled for
     * the given test class.
     *
     * @var bool
     */
    private static $_oroboros_expected_utility_designation = false;

    /**
     * May be overridden at the test class level. If true, indicates that the
     * given test class MUST designate itself indirectly or directly as an Extension.
     *
     * This test will be skipped if designations are disabled for
     * the given test class.
     *
     * @var bool
     */
    private static $_oroboros_expected_extension_designation = false;

    /**
     * May be overridden at the test class level. If true, indicates that the
     * given test class MUST designate itself indirectly or directly as a Module.
     *
     * This test will be skipped if designations are disabled for
     * the given test class.
     *
     * @var bool
     */
    private static $_oroboros_expected_module_designation = false;

    /**
     * May be overridden at the test class level. If true, indicates that the
     * given test class MUST designate itself indirectly or directly as a Component.
     *
     * This test will be skipped if designations are disabled for
     * the given test class.
     *
     * @var bool
     */
    private static $_oroboros_expected_component_designation = false;

    /**
     * May be overridden at the test class level. If true, indicates that the
     * given test class MUST designate itself indirectly or directly as a Pattern.
     *
     * This test will be skipped if designations are disabled for
     * the given test class.
     *
     * @var bool
     */
    private static $_oroboros_expected_pattern_designation = false;

    /**
     * May be overridden at the test class level. If true, indicates that the
     * given test class MUST designate itself indirectly or directly as an Adapter.
     *
     * This test will be skipped if designations are disabled for
     * the given test class.
     *
     * @var bool
     */
    private static $_oroboros_expected_adapter_designation = false;

    /**
     * May be overridden at the test class level. If true, indicates that the
     * given test class MUST designate itself indirectly or directly as a Controller.
     *
     * This test will be skipped if designations are disabled for
     * the given test class.
     *
     * @var bool
     */
    private static $_oroboros_expected_controller_designation = false;

    /**
     * May be overridden at the test class level. If true, indicates that the
     * given test class MUST designate itself indirectly or directly as a Model.
     *
     * This test will be skipped if designations are disabled for
     * the given test class.
     *
     * @var bool
     */
    private static $_oroboros_expected_model_designation = false;

    /**
     * May be overridden at the test class level. If true, indicates that the
     * given test class MUST designate itself indirectly or directly as a View.
     *
     * This test will be skipped if designations are disabled for
     * the given test class.
     *
     * @var bool
     */
    private static $_oroboros_expected_view_designation = false;

    /**
     * <Class Context Evaluation Method>
     * Asserts that the class has a defined class context, and it is valid.
     * @param object|string $class A reference to any object
     *     or class from the core, any object or class that expects
     *     to be compatible with the core, or any object or class that
     *     extends a core class of any sort.
     * @return void
     */
    protected function assertClassContextValid( $class )
    {
        $classname = get_called_class();
        $constant_key = 'TEST_CLASS_EXPECTED_CLASS_CONTEXT';
        $const = $classname . '::' . $constant_key;
        $expected_constant_key = 'OROBOROS_CLASS_CONTEXT';
        if ( !defined( $const ) )
        {
            throw new \PHPUnit\Framework\Error\Error( sprintf(
                'Undefined class constant [%1$s] in test class [%2$s].',
                $constant_key, get_class( $this )
            ), 0, __FILE__, __LINE__ );
        }
        if ( !constant( $const ) )
        {
            //If we are not declaring an expected class context, this passes.
            $this->assertTrue( true );
            return;
        }
        $classname = $class;
        $this->assertClassConstantDefined( $class, $expected_constant_key );
        $this->assertClassConstantNotEquals( true, $class,
            $expected_constant_key );
    }

    private function runCoreClassConstantContextAssertion( $class, $constant_key )
    {
        $classname = get_called_class();
        $constant_key = 'TEST_CLASS_EXPECTED_CLASS_CONTEXT';
        $const = $classname . '::' . $constant_key;
        if ( !defined( $const ) || (!constant( $const ) ) )
        {
            //Do not perform assertations if the defined interface is not set.
            return;
        }
    }

    /**
     * <Contract Interface Evaluation Method>
     * Evaluates that the expected contract interface is implemented as expected.
     * @param object|string $class A reference to any object
     *     or class from the core that is not a core internal
     * @return void
     */
    protected function assertContractInterfaceValid( $class )
    {
        $interface = $this->getExpectedClassContract();
        if ( !$interface || !$this->validateExpectedContractInterface() )
        {
            //A single true assertion will be made to prevent a test from
            //being flagged as risky if designation validation is disabled.
            $this->assertTrue( true );
            return;
        }
        $classname = $class;
        if ( is_object( $class ) )
        {
            $classname = get_class( $class );
        }
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Failed to assert that [%1$s] honors expected contract'
                . ' interface [%2$s] at [%3$s] in test class [%4$s]',
                $classname, $interface, __METHOD__, get_class( $this ) );
        }
        $this->assertInstanceOf( $interface, $class, $message );
    }

    /**
     * <Api Interface Evaluation Method>
     * Evaluates that the expected api interface is implemented as expected.
     * @param object|string $class A reference to any object
     *     or class from the core that is not a core internal
     * @return void
     */
    protected function assertApiInterfaceValid( $class )
    {
        $interface = $this->getExpectedClassApi();
        if ( !$interface || !$this->validateExpectedApiInterface() )
        {
            //A single true assertion will be made to prevent a test from
            //being flagged as risky if designation validation is disabled.
            $this->assertTrue( true );
            return;
        }
        $classname = $class;
        if ( is_object( $class ) )
        {
            $classname = get_class( $class );
        }
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Failed to assert that [%1$s] directly via implementation or '
                . 'indirectly via class constant declaration honors expected api'
                . ' interface [%2$s] at [%3$s] in test class [%4$s]',
                $classname, $interface, __METHOD__, get_class( $this ) );
        }
        if ( !( $class instanceof $interface ) )
        {
            //check indirect declaration, return before asserting false
            //on the next assertion if this passes. Api declarations are allowed
            //to be indirect via class constant as well as by direct implementation.
            $this->assertClassConstantDefined( $class, 'OROBOROS_API',
                sprintf( 'Failed to assert that expected api constant designator '
                    . '[%1$s] is declared in test subject'
                    . ' [%2$s], at [%3$s] in test class [%4$s].',
                    $classname . '::OROBOROS_API', $classname, __METHOD__,
                    get_class( $this )
                )
            );
            if ( ltrim( $interface, '\\' ) === ltrim( constant( $classname . '::OROBOROS_API' ) ) )
            {
                //Indirectly declares the exact api.
                $this->assertTrue( true );
                return;
            }
            //check if it's an extension.
            $this->assertInstanceOf( $interface,
                constant( $classname . '::OROBOROS_API' ), $message );
            return;
        }
        $this->assertInstanceOf( $interface, $class, $message );
    }

    /**
     * <Enumerated Interface Evaluation Method>
     * Evaluates that the expected enumerated interface is implemented as expected.
     * @param object|string $class A reference to any object
     *     or class from the core that is not a core internal
     * @return void
     */
    protected function assertEnumeratedInterfaceValid( $class, $message = null )
    {
        $interface = $this->getExpectedClassEnumerator();
        if ( !$interface || !$this->validateExpectedEnumeratedInterface() )
        {
            //A single true assertion will be made to prevent a test from
            //being flagged as risky if designation validation is disabled.
            $this->assertTrue( true );
            return;
        }
        $classname = $class;
        if ( is_object( $class ) )
        {
            $classname = get_class( $class );
        }
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Failed to assert that [%1$s] honors expected enumerated'
                . ' interface [%2$s] at [%3$s] in test class [%4$s]',
                $classname, $interface, __METHOD__, get_class( $this ) );
        }
        $this->assertInstanceOf( $interface, $class, $message );
    }

    /**
     * <Interface Evaluation Method>
     * Evaluates that the expected 3rd party interface is implemented as expected.
     * @param object|string $class A reference to any object
     *     or class from the core that is not a core internal
     * @return void
     */
    protected function assertInterfacesValid( $class, $message = null )
    {
        $interfaces = $this->getExpectedInterfaces();
        if ( !$interfaces || !$this->validateExpectedInterfaces() )
        {
            //A single true assertion will be made to prevent a test from
            //being flagged as risky if designation validation is disabled.
            $this->assertTrue( true );
            return;
        }
        $classname = $class;
        if ( is_object( $class ) )
        {
            $classname = get_class( $class );
        }
        $sub_message = $message;
        foreach ( $interfaces as
            $key =>
            $interface )
        {
            if ( is_null( $message ) )
            {
                $sub_message = sprintf( 'Failed to assert that class [%1$s] honors expected '
                    . ' interface [%2$s] at [%3$s] in test class [%4$s]',
                    $classname, $interface, __METHOD__, get_class( $this ) );
            }
            $this->assertInstanceOf( $interface, $class, $sub_message );
        }
    }

    /**
     * <Class Designation Integrity Method>
     * Evaluates that the object has declared all of the expected indirect
     * class context designation contants.
     *
     * This assertion does not validate any specific value for them,
     * only that they do actually exist in the given subject.
     *
     * @param object|string $class A reference to any object
     *     or class from the core that is not a core internal
     * @param string $expected The designation expected to be
     *     indirectly declared
     * @return void
     */
    protected function assertClassDesignationsExist( $class, $type,
        $message = null )
    {
        $designations = $this->getValidClassDesignationConstantKeys();
        if ( !$designations || !$this->validateExpectedClassDesignation() )
        {
            //A single true assertion will be made to prevent a test from
            //being flagged as risky if designation validation is disabled.
            $this->assertTrue( true );
            return;
        }
        $classname = $class;
        if ( is_object( $class ) )
        {
            $classname = get_class( $class );
        }
        $sub_message = $message;
        foreach ( $designations as
            $type =>
            $designation )
        {
            $const = $classname . '::' . $designation;
            if ( is_null( $message ) )
            {
                $sub_message = sprintf( 'Failed to assert that expected class constant designator '
                    . '[%1$s] is declared for class context type [%2$s] in test subject'
                    . ' [%3$s], at [%4$s] in test class [%5$s].', $const, $type,
                    $classname, __METHOD__, get_class( $this )
                );
            }
            $this->assertClassConstantDefined( $class, $designation,
                $sub_message );
        }
    }

    /**
     * <Class Designation Evaluation Method>
     * Evaluates that the object has declared an indirect class context.
     * @param object|string $class A reference to any object
     *     or class from the core that is not a core internal
     * @param string $expected The designation expected to be
     *     indirectly declared
     * @return void
     */
    protected function assertClassHasDesignation( $class, $type, $message = null )
    {
        $designations = $this->getValidClassDesignationConstantKeys();
        if ( !$designations || !$this->validateExpectedClassDesignation() )
        {
            //A single true assertion will be made to prevent a test from
            //being flagged as risky if designation validation is disabled.
            $this->assertTrue( true );
            return;
        }
        $classname = $class;
        if ( is_object( $class ) )
        {
            $classname = get_class( $class );
        }
        if ( !array_key_exists( $type, $designations ) )
        {
            throw new \PHPUnit\Framework\Error\Error( sprintf(
                'Specified class designation type [%1$s] is not a class designation'
                . ' in method [%2$s] of test class [%3$s].', $type, __METHOD__,
                get_class( $this )
            ), 0, __FILE__, __LINE__ );
        }
        $designation = $designations[$type];
        $const = $classname . '::' . $designation;
        $this->assertClassConstantDefined( $class, $designation,
            sprintf( 'Failed to assert that expected class constant designator '
                . '[%1$s] is declared for class context type [%2$s] in test subject'
                . ' [%3$s], at [%4$s] in test class [%5$s].', $const, $type,
                $classname, __METHOD__, get_class( $this )
            )
        );
        if ( is_null( $message ) )
        {
            $message = sprintf(
                'Failed to assert that expected class context indirect designation [%1$s] '
                . 'is [true], evaluating [%2$s] in test subject [%2$s], at [%3$s] in test class [%4$s].',
                $type, $const, $classname, __METHOD__, get_class( $this ) );
        }
        $this->assertClassConstantEquals( $expected, $class, $designation,
            $message );
    }

    /**
     * <Class Context Evaluation Method>
     * Evaluates that the object has a class context or nulls its class context
     * properly, and that its class context assertion is a valid enumerate.
     * @param object|string $class A reference to any object
     *     or class from the core that is not a core internal
     * @param string|false $expected The expected class context
     * @return void
     */
    protected function assertClassHasContext( $class, $expected, $message = null )
    {
        if ( !$this->validateExpectedClassContext() )
        {
            //A single true assertion will be made to prevent a test from
            //being flagged as risky if context validation is disabled.
            $this->assertTrue( true );
            return;
        }
        $classname = $class;
        if ( is_object( $class ) )
        {
            $classname = get_class( $class );
        }
        $constant_key = 'OROBOROS_CLASS_CONTEXT';
        $const = $classname . '::' . $constant_key;
        $this->assertClassConstantDefined( $class, $constant_key,
            sprintf( 'Failed to assert that expected class context class constant '
                . '[%1$s] is defined in test subject'
                . ' [%2$s], at [%3$s] in test class [%4$s].', $const,
                $classname, __METHOD__, get_class( $this )
            )
        );
        if ( is_null( $message ) )
        {
            $message = sprintf(
                'Failed to assert that expected class context of [%1$s] matches '
                . 'literal class context of [%2$s] in test subject [%3%s], at '
                . '[%4$s] of test class [%5$s]', $expected, constant( $const ),
                $classname, __METHOD__, get_class( $this )
            );
        }
        $this->assertClassConstantEquals( $expected, $class, $designation,
            $message );
    }

    /**
     * <Class Type Evaluation Method>
     * Evaluates that the object has a class type or nulls its class type
     * properly, and that its class type assertion is a valid enumerate.
     * @param object|string $class A reference to any object
     *     or class from the core that is not a core internal
     * @param string|false $expected The expected class type
     * @return void
     */
    protected function assertClassHasType( $class, $expected, $message = null )
    {
        if ( !$this->validateExpectedClassType() )
        {
            //A single true assertion will be made to prevent a test from
            //being flagged as risky if class type validation is disabled.
            $this->assertTrue( true );
            return;
        }
        $classname = $class;
        if ( is_object( $class ) )
        {
            $classname = get_class( $class );
        }
        $constant_key = 'OROBOROS_CLASS_TYPE';
        $const = $classname . '::' . $constant_key;
        $this->assertClassConstantDefined( $class, $constant_key,
            sprintf( 'Failed to assert that expected class type class constant '
                . '[%1$s] is defined in test subject'
                . ' [%2$s], at [%3$s] in test class [%4$s].', $const,
                $classname, __METHOD__, get_class( $this )
            )
        );
        if ( is_null( $message ) )
        {
            $message = sprintf(
                'Failed to assert that expected class type of [%1$s] matches '
                . 'literal class type of [%2$s] in test subject [%3%s], at '
                . '[%4$s] of test class [%5$s]', $expected, constant( $const ),
                $classname, __METHOD__, get_class( $this )
            );
        }
        $this->assertClassConstantEquals( $expected, $class, $designation,
            $message );
    }

    /**
     * <Class Scope Evaluation Method>
     * Evaluates that the object has a class type or nulls its class scope
     * properly, and that its class scope assertion is a valid enumerate.
     * @param object|string $class A reference to any object
     *     or class from the core that is not a core internal
     * @param string|false $expected The expected class scope
     * @return void
     */
    protected function assertClassHasScope( $class, $expected, $message = null )
    {
        if ( !$this->validateExpectedClassScope() )
        {
            //A single true assertion will be made to prevent a test from
            //being flagged as risky if class scope validation is disabled.
            $this->assertTrue( true );
            return;
        }
        $classname = $class;
        if ( is_object( $class ) )
        {
            $classname = get_class( $class );
        }
        $constant_key = 'OROBOROS_CLASS_SCOPE';
        $const = $classname . '::' . $constant_key;
        $this->assertClassConstantDefined( $class, $constant_key,
            sprintf( 'Failed to assert that expected class context class constant '
                . '[%1$s] is defined in test subject'
                . ' [%2$s], at [%3$s] in test class [%4$s].', $const,
                $classname, __METHOD__, get_class( $this )
            )
        );
        if ( is_null( $message ) )
        {
            $message = sprintf(
                'Failed to assert that expected class scope of [%1$s] matches '
                . 'literal class scope of [%2$s] in test subject [%3%s], at '
                . '[%4$s] of test class [%5$s]', $expected, constant( $const ),
                $classname, __METHOD__, get_class( $this )
            );
        }
        $this->assertClassConstantEquals( $expected, $class, $designation,
            $message );
    }

    /**
     * Returns the valid class designation class constant keys.
     * @return array
     */
    protected function getValidClassDesignationConstantKeys()
    {
        return array(
            'core' => 'OROBOROS_CORE',
            'pattern' => 'OROBOROS_PATTERN',
            'utility' => 'OROBOROS_UTILITY',
            'library' => 'OROBOROS_LIBRARY',
            'routine' => 'OROBOROS_ROUTINE',
            'extension' => 'OROBOROS_EXTENSION',
            'module' => 'OROBOROS_MODULE',
            'component' => 'OROBOROS_COMPONENT',
            'adapter' => 'OROBOROS_ADAPTER',
            'controller' => 'OROBOROS_CONTROLLER',
            'model' => 'OROBOROS_MODEL',
            'view' => 'OROBOROS_VIEW',
        );
    }

    /**
     * Returns the valid class contexts
     * @return array
     */
    protected function getValidClassContexts()
    {
        if ( empty( self::$_oroboros_class_contexts ) )
        {
            return false;
        }
        return self::$_oroboros_class_contexts;
    }

    /**
     * Returns the valid class types
     * @return array
     */
    protected function getValidClassTypes()
    {
        if ( empty( self::$_oroboros_class_types ) )
        {
            return false;
        }
        return self::$_oroboros_class_types;
    }

    /**
     * Returns the valid class scopes
     * @return array
     */
    protected function getValidClassScopes()
    {
        if ( empty( self::$_oroboros_class_scopes ) )
        {
            return false;
        }
        return self::$_oroboros_class_scopes;
    }

    /**
     * Returns the expected class context, or false if it is not declared
     * @return bool|string
     */
    protected function getExpectedClassContext()
    {
        $const = get_class( $this ) . '::TEST_CLASS_EXPECTED_CLASS_CONTEXT';
        return self::$_oroboros_expected_class_context;
    }

    /**
     * Returns the expected class type, or false if it is not declared
     * @return bool|string
     */
    protected function getExpectedClassType()
    {
        $const = get_class( $this ) . '::TEST_CLASS_EXPECTED_CLASS_TYPE';
        return self::$_oroboros_expected_class_type;
    }

    /**
     * Returns the expected class scope, or false if it is not declared
     * @return bool|string
     */
    protected function getExpectedClassScope()
    {
        $const = get_class( $this ) . '::TEST_CLASS_EXPECTED_CLASS_SCOPE';
        return self::$_oroboros_expected_class_scope;
    }

    /**
     * Returns the expected contract interface, or false if it is not declared
     * @return bool|string
     */
    protected function getExpectedClassContract()
    {
        $value = self::$_oroboros_expected_contract;
        if ( !$value )
        {
            //No class contract is enforced.
            return false;
        }
        $const = get_class( $this ) . '::TEST_CLASS_EXPECTED_CONTRACT';
        if ( !interface_exists( $value, true ) )
        {
            throw new \PHPUnit\Framework\Error\Error( sprintf(
                'Specified expected contract interface [%1$s] is not a valid interface.'
                . ' Defined by class constant [%2$s] in test class [%3$s].',
                $this->castValueToString( $value ), $const, get_class( $this )
            ), 0, __FILE__, __LINE__ );
        }
        if ( $this->validateExpectedInterfaceTypes() )
        {
            $baseline = $this->getBaseApiInterface();
            if ( array_key_exists( 'contract', $baseline ) )
            {
                $base = $baseline['contract'];
                if ( !ltrim( $base, '\\' ) === ltrim( $value, '\\' ) && !($value instanceof $base) )
                {
                    //The contract is invalid.
                    //This is an integrity error
                    //with the test class itself.
                    throw new \PHPUnit\Framework\Error\Error( sprintf(
                        'Specified expected contract interface [%1$s] is not equal to or an instance of '
                        . 'expected base contract interface [%2$s] in test class [%3$s]. '
                        . 'Please correct the malformed test class logic before proceeding.',
                        $this->castValueToString( $value ), $base,
                        get_class( $this )
                    ), 0, __FILE__, __LINE__ );
                }
            }
        }
        return $value;
    }

    /**
     * Returns the expected api interface, or false if it is not declared
     * @return bool|string
     */
    protected function getExpectedClassApi()
    {
        $const = get_class( $this ) . '::TEST_CLASS_EXPECTED_API';
        $value = self::$_oroboros_expected_api;
        if ( !$value )
        {
            //No class api is enforced.
            return false;
        }
        if ( !interface_exists( $value, true ) )
        {
            throw new \PHPUnit\Framework\Error\Error( sprintf(
                'Specified expected api interface [%1$s] is not a valid interface.'
                . ' Defined by class constant [%2$s] in test class [%3$s].',
                $this->castValueToString( $value ), $const, get_class( $this )
            ), 0, __FILE__, __LINE__ );
        }
        if ( $this->validateExpectedInterfaceTypes() )
        {
            $baseline = $this->getBaseApiInterface();
            if ( array_key_exists( 'api', $baseline ) )
            {
                $base = $baseline['api'];
                if ( !ltrim( $base, '\\' ) === ltrim( $value, '\\' ) && !($value instanceof $base) )
                {
                    //The api is invalid.
                    //This is an integrity error
                    //with the test class itself.
                    throw new \PHPUnit\Framework\Error\Error( sprintf(
                        'Specified expected api interface [%1$s] is not equal to or an instance of '
                        . 'expected base api interface [%2$s] in test class [%3$s]. '
                        . 'Please correct the malformed test class logic before proceeding.',
                        $this->castValueToString( $value ), $base,
                        get_class( $this )
                    ), 0, __FILE__, __LINE__ );
                }
            }
        }
        return $value;
    }

    /**
     * Returns the expected enumerated interface, or false if it is not declared
     * @return bool|string
     */
    protected function getExpectedClassEnumerator()
    {
        $const = get_class( $this ) . '::TEST_CLASS_EXPECTED_ENUMERATOR';
        $value = self::$_oroboros_expected_enumerator;
        if ( !$value )
        {
            //No class enumerator is enforced.
            return false;
        }
        if ( !interface_exists( $value, true ) )
        {
            throw new \PHPUnit\Framework\Error\Error( sprintf(
                'Specified expected enumerated interface [%1$s] is not a valid interface.'
                . ' Defined by class constant [%2$s] in test class [%3$s].',
                $this->castValueToString( $value ), $const, get_class( $this )
            ), 0, __FILE__, __LINE__ );
        }
        if ( $this->validateExpectedInterfaceTypes() )
        {
            $baseline = $this->getBaseApiInterface();
            if ( array_key_exists( 'enumerated', $baseline ) )
            {
                $base = $baseline['enumerated'];
                if ( !ltrim( $base, '\\' ) === ltrim( $value, '\\' ) && !($value instanceof $base) )
                {
                    //The enumerator is invalid.
                    //This is an integrity error
                    //with the test class itself.
                    throw new \PHPUnit\Framework\Error\Error( sprintf(
                        'Specified expected enumerated interface [%1$s] is not equal to or an instance of '
                        . 'expected base enumerated interface [%2$s] in test class [%3$s]. '
                        . 'Please correct the malformed test class logic before proceeding.',
                        $this->castValueToString( $value ), $base,
                        get_class( $this )
                    ), 0, __FILE__, __LINE__ );
                }
            }
        }
        return $value;
    }

    /**
     * Gets the expected PHP or 3rd party interfaces that the class MUST honor,
     * or false if none are declared.
     * @return array
     */
    protected function getExpectedInterfaces()
    {
        if ( !self::$_oroboros_expected_interfaces || empty( self::$_oroboros_expected_interfaces ) )
        {
            return false;
        }
        $interfaces = self::$_oroboros_expected_interfaces;
        foreach ( $interfaces as
            $interface )
        {
            if ( !interface_exists( $interface, true ) )
            {
                $def_method = get_called_class() . '::bootstrapExpectedInterfaces';
                throw new \PHPUnit\Framework\Error\Error( sprintf(
                    'Specified reuired interface [%1$s] is not a valid interface.'
                    . ' Defined by startup method [%2$s] in test class [%3$s]. '
                    . 'Please correct the malformed test class logic to proceed.',
                    $this->castValueToString( $interface ), $def_method,
                    get_class( $this )
                ), 0, __FILE__, __LINE__ );
            }
        }
        return $interfaces;
    }

    /**
     * Gets the expected class context designations, or a specific one,
     * or false if they are not declared (or if the specified one is not declared).
     * @return bool|array
     */
    protected function getExpectedClassDesignations( $type = null )
    {
        if ( empty( self::$_oroboros_class_designations ) )
        {
            return false;
        }
        if ( !is_null( $type ) && !array_key_exists( $type,
                self::$_oroboros_class_designations ) )
        {
            return false;
        }
        if ( is_null( $type ) )
        {
            return self::$_oroboros_class_designations;
        }
        return self::$_oroboros_class_designations[$type];
    }

    /**
     * Gets the class name of the declared test class subject,
     * without instantiating or initializing it.
     * @return string|bool Returns either a valid class name or false
     */
    protected function getTestSubject()
    {
        $const = get_class( $this ) . '::TEST_CLASS';
        if ( !defined( $const ) || !constant( $const ) )
        {
            return false;
        }
        $class = constant( $const );
        if ( !class_exists( $class, true ) )
        {
            throw new \PHPUnit\Framework\Error\Error( sprintf(
                'Specified default test subject [%1$s] designated by [%2$s] is not a valid class'
                . ' in test class [%3$s].', $this->castValueToString( $class ),
                $const, get_class( $this )
            ), 0, __FILE__, __LINE__ );
        }
        return $class;
    }

    /**
     * Gets the class name of the declared invalid test class subject,
     * without instantiating or initializing it.
     * @return string|bool Returns either a valid class name or false
     */
    protected function getInvalidTestSubject()
    {
        $const = get_class( $this ) . '::TEST_CLASS_INVALID';
        if ( !defined( $const ) || !constant( $const ) )
        {
            return false;
        }
        $class = constant( $const );
        if ( !class_exists( $class, true ) )
        {
            throw new \PHPUnit\Framework\Error\Error( sprintf(
                'Specified invalid test subject [%1$s] designated by [%2$s] is not a valid class'
                . ' in test class [%3$s].', $this->castValueToString( $class ),
                $const, get_class( $this )
            ), 0, __FILE__, __LINE__ );
        }
        return $class;
    }

    /**
     * Gets the class name of the declared misconfigured test class subject,
     * without instantiating or initializing it.
     * @return string|bool Returns either a valid class name or false
     */
    protected function getMisconfiguredTestSubject()
    {
        $const = get_class( $this ) . '::TEST_CLASS_MISCONFIGURED';
        if ( !defined( $const ) || !constant( $const ) )
        {
            return false;
        }
        $class = constant( $const );
        if ( !class_exists( $class, true ) )
        {
            throw new \PHPUnit\Framework\Error\Error( sprintf(
                'Specified misconfigured test subject [%1$s] designated by [%2$s] is not a valid class'
                . ' in test class [%3$s].', $this->castValueToString( $class ),
                $const, get_class( $this )
            ), 0, __FILE__, __LINE__ );
        }
        return $class;
    }

    /**
     * Returns the interface that all further contract interface
     * extensions should validate as an instance of.
     *
     * This is used to designate that a family of scoped classes follows
     * the correct contractual interface schema.
     *
     * This method can be overridden in test classes to scope
     * a set of contract interfaces further.
     *
     * @return string
     */
    protected static function getBaseContractInterface()
    {
        return '\\oroboros\\core\\interfaces\\contract\\BaseContract';
    }

    /**
     * Returns the interface that all further api interface
     * extensions should validate as an instance of.
     *
     * This is used to designate that a family of scoped classes follows
     * the correct api interface schema.
     *
     * This method can be overridden in test classes to scope
     * a set of api interfaces further.
     *
     * @return string
     */
    protected static function getBaseApiInterface()
    {
        return '\\oroboros\\core\\interfaces\\api\\BaseApi';
    }

    /**
     * Returns the interface that all further enumerated interface
     * extensions should validate as an instance of.
     *
     * This is used to designate that a family of scoped classes follows
     * the correct enumerated interface schema.
     *
     * This method can be overridden in test classes to scope
     * a set of enumerated interfaces further.
     *
     * @return string
     */
    protected static function getBaseEnumeratedInterface()
    {
        return '\\oroboros\\core\\interfaces\\enumerated\\BaseEnum';
    }

    /**
     * Designates whether or not to verify the integrity
     * of the secondary class context designations.
     * @return bool
     */
    private function validateExpectedClassDesignation()
    {
        return self::$_oroboros_check_class_designation;
    }

    /**
     * Designates whether or not to verify the integrity
     * of the declared expected class context.
     * @return bool
     */
    private function validateExpectedClassContext()
    {
        return self::$_oroboros_check_class_context;
    }

    /**
     * Designates whether or not to verify the integrity
     * of the declared expected class type.
     * @return bool
     */
    private function validateExpectedClassType()
    {
        return self::$_oroboros_check_class_type;
    }

    /**
     * Designates whether or not to verify the integrity
     * of the declared expected class scope.
     * @return bool
     */
    private function validateExpectedClassScope()
    {
        return self::$_oroboros_check_class_scope;
    }

    /**
     * Designates whether or not to verify the integrity
     * of the declared expected interface types.
     * @return bool
     */
    private function validateExpectedInterfaceTypes()
    {
        return self::$_oroboros_check_interface_type;
    }

    /**
     * Designates whether or not to verify the integrity
     * of the 3rd party or PHP internal interfaces implemented
     * by the testing class.
     * @return bool
     */
    private function validateExpectedInterfaces()
    {
        return self::$_oroboros_check_interfaces;
    }

    /**
     * Designates whether or not to verify the integrity
     * of the expected contract interface.
     * @return bool
     */
    private function validateExpectedContractInterface()
    {
        return self::$_oroboros_check_class_contract;
    }

    /**
     * Designates whether or not to verify the integrity
     * of the expected api interface.
     * @return bool
     */
    private function validateExpectedApiInterface()
    {
        return self::$_oroboros_check_class_api;
    }

    /**
     * Designates whether or not to verify the integrity
     * of the expected enumerated interface.
     * @return bool
     */
    private function validateExpectedEnumeratedInterface()
    {
        return self::$_oroboros_check_class_enumerator;
    }

    /**
     * Designates whether the test class should have the
     * Oroboros class designation of Core internal class context.
     *
     * Overriding this method and returning true or false will designate
     * that the integrity check logic should assert that the test class
     * declares an equal designation in the context of being used as a
     * core internal.
     *
     * If this is true, it can operate as a core internal regardless of its
     * declared class context, If this is false, it cannot operate
     * as a core internal, and the system will reject all attempts to pass it
     * as one.
     *
     * @note Sensitive operations of the core will typically reject substitution
     *     entirely and require an explicit expected class to rule out
     *     pollution of the core engine for many purposes. For this reason,
     *     designating a further extension of a core class does not usually
     *     allow it to substitute within the actual underlying core logic,
     *     though it will remain interchangeable by substitution for outward
     *     distribution via the normal substitution paradigm.
     *     TLDR: Do not try to hack the core. You will break stuff.
     *
     * @return bool
     */
    protected static function getExpectedClassDesignationCore()
    {
        return false;
    }

    /**
     * Designates whether the test class should have the
     * Oroboros class designation of Pattern class context.
     *
     * Overriding this method and returning true or false will designate
     * that the integrity check logic should assert that the test class
     * declares an equal designation in the context of being used as a pattern.
     *
     * If this is true, it can operate as a pattern regardless of its
     * declared class context. If this is false, it cannot operate
     * as a pattern, and the system will reject attempts to pass it
     * as one.
     *
     * @note A pattern designation asserts that the given class uses a
     *     representation of a design pattern within its internal logic.
     *     As design patterns are nebulous utilities used to perform a
     *     specific realm of tasks efficiently in a highly abstract way,
     *     object substitution within the scope of a pattern can produce
     *     irregular results based on how the extending class has implemented
     *     its logic. For this reason, an indirect designation as a pattern
     *     will weight extremely low in the substitution heirarchy compared
     *     to other more relevant options.
     *
     * @return bool
     */
    protected static function getExpectedClassDesignationPattern()
    {
        return false;
    }

    /**
     * Designates whether the test class should have the
     * Oroboros class designation of Routine class context.
     *
     * Overriding this method and returning true or false will designate
     * that the integrity check logic should assert that the test class
     * declares an equal designation in the context of being used as a routine.
     *
     * If this is true, it can operate as a routine regardless of its
     * declared class context. If this is false, it cannot operate
     * as a routine, and the system will reject attempts to pass it
     * as one.
     *
     * @note A routine designation asserts that there is a high likelihood that
     *     the object will assume control of the environment when executed.
     *     It is likely that using a routine out of scope will change global
     *     settings or tweak PHP settings to honor its intended purpose.
     *     This should be accounted for when calling a routine from out of
     *     scope logic. Generally, they should only be used within the scope
     *     of expected assumption of control, typically as a leaf-level
     *     operation.
     *
     * @return bool
     */
    protected static function getExpectedClassDesignationRoutine()
    {
        return false;
    }

    /**
     * Designates whether the test class should have the
     * Oroboros class designation of Utility class context.
     *
     * Overriding this method and returning true or false will designate
     * that the integrity check logic should assert that the test class
     * declares an equal designation in the context of being used as a utility.
     *
     * If this is true, it can operate as a utility regardless of its
     * declared class context. If this is false, it cannot operate
     * as a utility, and the system will reject attempts to pass it
     * as one.
     *
     * @note A utility is a general purpose, typically stateless class used
     *     for accomplishing simple operations. A utility with an indirect
     *     declaration may have altered its internal logic to represent a
     *     more focused task, and may add excessive weight to an operation
     *     that is out of scope of the expected result. For example a more
     *     robust library that extended upon an abstract utility may be
     *     performing heavier operations than the underlying utility would.
     *     In most cases, use the lowest level applicable instance of a
     *     utility (which typically will have a direct designation as a
     *     utility) to accomplish the desired task with performance in mind.
     *
     * @return bool
     */
    protected static function getExpectedClassDesignationUtility()
    {
        return false;
    }

    /**
     * Designates whether the test class should have the
     * Oroboros class designation of Library class context.
     *
     * Overriding this method and returning true or false will designate
     * that the integrity check logic should assert that the test class
     * declares an equal designation in the context of being used as a library.
     *
     * If this is true, it can operate as a library regardless of its
     * declared class context. If this is false, it cannot operate
     * as a library, and the system will reject attempts to pass it
     * as one.
     *
     * @note An indirect class designation as a library typically indicates
     *     that the class is safe to use in the expected library context
     *     regardless of its other class type, and will generally not have
     *     any undesirable results for doing so.
     *
     * @return bool
     */
    protected static function getExpectedClassDesignationLibrary()
    {
        return false;
    }

    /**
     * Designates whether the test class should have the
     * Oroboros class designation of Module class context.
     *
     * Overriding this method and returning true or false will designate
     * that the integrity check logic should assert that the test class
     * declares an equal designation in the context of being used as a module.
     *
     * If this is true, it can operate as a module regardless of its
     * declared class context. If this is false, it cannot operate
     * as a module, and the system will reject attempts to pass it
     * as one.
     *
     * @note An indirect designation as a module indicates that a lower level
     *     abstraction of the class indicated that it was intended to work
     *     explicitly in the context of the internal logic of another class.
     *     In most cases, this means that it should be used in a very similar
     *     scope to its original purpose to avoid the need of formatting output
     *     or working with an excessively complex return value intended to work
     *     in another explicit internal context other than how it is being used.
     *
     * @return bool
     */
    protected static function getExpectedClassDesignationModule()
    {
        return false;
    }

    /**
     * Designates whether the test class should have the
     * Oroboros class designation of Component class context.
     *
     * Overriding this method and returning true or false will designate
     * that the integrity check logic should assert that the test class
     * declares an equal designation in the context of being used as a component.
     *
     * If this is true, it can operate as a component regardless of its
     * declared class context. If this is false, it cannot operate
     * as a component, and the system will reject attempts to pass it
     * as one.
     *
     * @note An indirect designation as a component indicates that the
     *     underlying abstractions original purpose was to work with an
     *     explicit output type. It may be safely used for another purpose,
     *     but will continue to work best when applied to the specified output
     *     type only. Components work with an expected set of return values
     *     without regard to class context, so they are usually not coupled
     *     with any other class that they do not need for their own operations.
     *
     * @return bool
     */
    protected static function getExpectedClassDesignationComponent()
    {
        return false;
    }

    /**
     * Designates whether the test class should have the
     * Oroboros class designation of Extension class context.
     *
     * Overriding this method and returning true or false will designate
     * that the integrity check logic should assert that the test class
     * declares an equal designation in the context of being used as an extension.
     *
     * If this is true, it can operate as an extension regardless of its
     * declared class context. If this is false, it cannot operate
     * as an extension, and the system will reject attempts to pass it
     * as one.
     *
     * @note An indirect class designation as an extension indicates that
     *     the class was intended to extend the public api of another class
     *     directly. These classes may generally be used as standalone
     *     resources without issue, and will contain the exactly identical
     *     api that they append to the class they would normally extend.
     *     They do not inherit the rest of the api available in the extendable
     *     class, so if those methods are needed in conjunction with the
     *     extension logic, you should just use the class it extends
     *     instead. However if you only need the methods provided by the
     *     extension, using the extension alone can reduce the performance
     *     footprint of your use case. For example a number of the internals
     *     of the core utilize the validator extension as a standalone resource,
     *     instead of calling back to the core accessor to use it, which is
     *     particularly relevant in operations that often need to be done
     *     before the core has finished initializing.
     *
     * @return bool
     */
    protected static function getExpectedClassDesignationExtension()
    {
        return false;
    }

    /**
     * Designates whether the test class should have the
     * Oroboros class designation of Adapter class context.
     *
     * Overriding this method and returning true or false will designate
     * that the integrity check logic should assert that the test class
     * declares an equal designation in the context of being used as an adapter.
     *
     * If this is true, it can operate as an adapter regardless of its
     * declared class context. If this is false, it cannot operate
     * as an adapter, and the system will reject attempts to pass it
     * as one.
     *
     * @note It is extremely common for a class to have an indirect class
     *     designation of adapter. Adapters indicate that they interact with
     *     an external resource, such as a database, programmatic api, or the
     *     command line. It is also very common for a class to immediately
     *     designate itself indirectly as an adapter even if it does not
     *     provide a raw adapter class, and has a different direct class
     *     context designation. For this reason, an indirect designation as
     *     an adapter typically means the class is totally safe to use in the
     *     adapter context without issue.
     *
     * @return bool
     */
    protected static function getExpectedClassDesignationAdapter()
    {
        return false;
    }

    /**
     * Designates whether the test class should have the
     * Oroboros class designation of Controller class context.
     *
     * Overriding this method and returning true or false will designate
     * that the integrity check logic should assert that the test class
     * declares an equal designation in the context of being used as a controller.
     *
     * If this is true, it can operate as a controller regardless of its
     * declared class context. If this is false, it cannot operate
     * as a controller, and the system will reject attempts to pass it
     * as one.
     *
     * @note An indirect class designation as a controller is extremely rare,
     *     as a controller is an MVC paradigm that is usually a leaf-level
     *     class. Most controllers also have indirect designations as
     *     libraries or other class contexts, however it is very rare to
     *     see an indirect controller context, except sometimes in the case
     *     of a routine. Controllers should not be used out of scope except
     *     for testing, as they are by design intended to assume control.
     *
     * @return bool
     */
    protected static function getExpectedClassDesignationController()
    {
        return false;
    }

    /**
     * Designates whether the test class should have the
     * Oroboros class designation of Model class context.
     *
     * Overriding this method and returning true or false will designate
     * that the integrity check logic should assert that the test class
     * declares an equal designation in the context of being used as a model.
     *
     * If this is true, it can operate as a model regardless of its
     * declared class context. If this is false, it cannot operate
     * as a model, and the system will reject attempts to pass it
     * as one.
     *
     * @note A class will rarely ever have an indirect context of Model,
     *     as a model is an MVC paradigm that is usually a leaf-level class.
     *     Models can be used pretty easily in an indirect scope, and are
     *     applicable as super-libraries with an extremely focused context.
     *     If you need that specific context outside of the typical MVC paradigm,
     *     using a model out of scope is perfectly fine.
     *
     * @return bool
     */
    protected static function getExpectedClassDesignationModel()
    {
        return false;
    }

    /**
     * Designates whether the test class should have the
     * Oroboros class designation of View class context.
     *
     * Overriding this method and returning true or false will designate
     * that the integrity check logic should assert that the test class
     * declares an equal designation in the context of being used as a view.
     *
     * If this is true, it can operate as a view regardless of its
     * declared class context. If this is false, it cannot operate
     * as a view, and the system will reject attempts to pass it
     * as one.
     *
     * @note A class will rarely ever have an indirect context of View,
     *     as a view is an MVC paradigm that is usually a leaf-level class.
     *     Views in the context of Oroboros are designed to render a specific
     *     output type, using a provided standardized set of available data.
     *     For this reason, they are often extremely useful in other contexts,
     *     particularly in the case of views that have the primary purpose of
     *     data-shaping xml, json, etc. Typically, they should be used in an
     *     MVC paradigm, however for quick and dirty applications they will
     *     output a very complicated set of data into a standardized format
     *     reliably, which is often useful elsewhere. It should be noted that
     *     views typically apply opinion about how an output result should
     *     look, and may also fire headers or other out-of-scope operations,
     *     so this should be accounted for prior to usage, or otherwise the
     *     underlying libraries, adapters and utilities that the view leverages
     *     should be used in place of the view for a non-opinionated expression
     *     of the same result.
     *
     * @return bool
     */
    protected static function getExpectedClassDesignationView()
    {
        return false;
    }

    /**
     * Sets up the baseline interfaces that all extension interfaces MUST honor.
     * @beforeClass
     */
    public static function bootstrapExpectedBaselineInterfaces()
    {
        $class = get_called_class();
        self::$_oroboros_baseline_interface_types = array(
            'contract' => $class::getBaseContractInterface(),
            'api' => $class::getBaseApiInterface(),
            'enumerated' => $class::getBaseEnumeratedInterface(),
        );
        self::$_oroboros_check_interface_type = $class::TEST_CLASS_VALIDATE_BASELINE_INTERFACES;
    }

    /**
     * Sets up the expected set of 3rd party or PHP internal
     * interfaces that the given test class MUST honor
     * @beforeClass
     */
    public static function bootstrapExpectedInterfaces()
    {
        $class = get_called_class();
        self::$_oroboros_expected_interfaces = $class::declareExpectedInterfaces();
    }

    /**
     * This method should be overridden with any interfaces
     * that the test subject class MUST honor.
     *
     * These are collected by the base abstract before the test class is built,
     * and an integrity check against the expected class will preflight it to
     * insure that it honors ALL interfaces found in this set. This enables
     * simple enforcement of Liskov substitution in chains of extending logic.
     *
     * The typical implementation of this is as follows:
     *
     * --------
     *
     * public static function declareExpectedInterfaces()
     * {
     *     return array_merge(
     *         parent::declareExpectedInterfaces(), array( '\\your\\interfaces\\here' ) );
     * }
     *
     * --------
     *
     * The above insures that any inheritance in a chain of
     * extending test cases is retained.
     *
     * The default lowest level implementation of this does not enforce any interfaces.
     * Each subsequent layer of testing will add its own expected contract interfaces
     * and 3rd party expected interfaces to the set.
     *
     */
    public static function declareExpectedInterfaces()
    {
        return array();
    }

    /**
     * Sets up the expected set of class contexts
     * @beforeClass
     */
    public static function bootstrapExpectedClassContexts()
    {
        $class = get_called_class();
        $const = $class . '::' . 'TEST_CLASS_ALLOWED_CLASS_CONTEXTS';
        if ( !defined( $const ) || !constant( $const ) )
        {
            self::$_oroboros_class_contexts = array();
            self::$_oroboros_check_class_context = false;
        }
        $enum = new \oroboros\enum\InterfaceEnumerator( constant( $const ) );
        self::$_oroboros_check_class_context = $class::TEST_CLASS_VALIDATE_CLASS_CONTEXT;
        self::$_oroboros_class_contexts = $enum->toArray();
    }

    /**
     * Sets up the expected set of class types
     * @beforeClass
     */
    public static function bootstrapExpectedClassTypes()
    {
        $class = get_called_class();
        $const = $class . '::' . 'TEST_CLASS_ALLOWED_CLASS_TYPES';
        if ( !defined( $const ) || !constant( $const ) )
        {
            self::$_oroboros_class_types = array();
            self::$_oroboros_check_class_type = false;
        }
        $enum = new \oroboros\enum\InterfaceEnumerator( constant( $const ) );
        self::$_oroboros_check_class_type = $class::TEST_CLASS_VALIDATE_CLASS_TYPE;
        self::$_oroboros_class_types = $enum->toArray();
    }

    /**
     * Sets up the expected set of class scopes
     * @afterClass
     */
    public static function bootstrapExpectedClassScopes()
    {
        $class = get_called_class();
        $const = $class . '::' . 'TEST_CLASS_ALLOWED_CLASS_SCOPES';
        if ( !defined( $const ) || !constant( $const ) )
        {
            self::$_oroboros_class_scopes = array();
            self::$_oroboros_check_class_scope = false;
        }
        $enum = new \oroboros\enum\InterfaceEnumerator( constant( $const ) );
        self::$_oroboros_check_class_scope = $class::TEST_CLASS_VALIDATE_CLASS_SCOPE;
        self::$_oroboros_class_scopes = $enum->toArray();
    }

    /**
     * Sets up the expected set of class context indirect designations
     * @beforeClass
     */
    public static function bootstrapExpectedConstantDesignations()
    {
        $class = get_called_class();
        self::$_oroboros_class_designations = array(
            'core' => $class::getExpectedClassDesignationCore(),
            'pattern' => $class::getExpectedClassDesignationPattern(),
            'routine' => $class::getExpectedClassDesignationRoutine(),
            'utility' => $class::getExpectedClassDesignationUtility(),
            'library' => $class::getExpectedClassDesignationLibrary(),
            'extension' => $class::getExpectedClassDesignationExtension(),
            'module' => $class::getExpectedClassDesignationModule(),
            'component' => $class::getExpectedClassDesignationComponent(),
            'adapter' => $class::getExpectedClassDesignationAdapter(),
            'controller' => $class::getExpectedClassDesignationController(),
            'model' => $class::getExpectedClassDesignationModel(),
            'view' => $class::getExpectedClassDesignationView(),
        );
    }

    /**
     * Sets up the explicitly declared class contract interface,
     * and the option as to whether to check it.
     * @beforeClass
     */
    public static function bootstrapDeclaredClassContract()
    {
        $option = false;
        self::$_oroboros_check_class_contract = $option;
        self::$_oroboros_expected_contract = false;
        $class = get_called_class();
        $const = $class . '::' . 'TEST_CLASS_VALIDATE_CLASS_CONTRACT';
        if ( defined( $const ) )
        {
            $option = constant( $const )
                ? true
                : false;
            $def_const = $class . '::' . 'TEST_CLASS_EXPECTED_CONTRACT';
            if ( !$option || !defined( $def_const ) || !constant( $def_const ) )
            {
                $option = false;
            } else
            {
                self::$_oroboros_expected_contract = constant( $def_const );
            }
        }
        self::$_oroboros_check_class_contract = $option;
    }

    /**
     * Sets up the explicitly declared class api interface,
     * and the option as to whether to check it.
     * @beforeClass
     */
    public static function bootstrapDeclaredClassApi()
    {
        $option = false;
        self::$_oroboros_check_class_api = $option;
        self::$_oroboros_expected_api = $option;
        $class = get_called_class();
        $const = $class . '::' . 'TEST_CLASS_VALIDATE_CLASS_API';
        if ( defined( $const ) )
        {
            $option = constant( $const )
                ? true
                : false;
            $def_const = $class . '::' . 'TEST_CLASS_EXPECTED_API';
            if ( !$option || !defined( $def_const ) || !constant( $def_const ) )
            {
                $option = false;
            } else
            {
                self::$_oroboros_expected_api = constant( $def_const );
            }
        }
        self::$_oroboros_check_class_api = $option;
    }

    /**
     * Sets up the explicitly declared class enumerated interface,
     * and the option as to whether to check it.
     * @beforeClass
     */
    public static function bootstrapDeclaredClassEnumerator()
    {
        $option = false;
        self::$_oroboros_check_class_enumerator = $option;
        self::$_oroboros_expected_enumerator = $option;
        $class = get_called_class();
        $const = $class . '::' . 'TEST_CLASS_VALIDATE_CLASS_ENUMERATOR';
        if ( defined( $const ) )
        {
            $option = constant( $const )
                ? true
                : false;
            $def_const = $class . '::' . 'TEST_CLASS_EXPECTED_ENUMERATOR';
            if ( !$option || !defined( $def_const ) || !constant( $def_const ) )
            {
                $option = false;
            } else
            {
                self::$_oroboros_expected_enumerator = constant( $def_const );
            }
        }
        self::$_oroboros_check_class_enumerator = $option;
    }

    /**
     * Sets up the explicitly declared class interfaces
     * that must be validated against, and the option as to
     * whether to check them.
     * @beforeClass
     */
    public static function bootstrapDeclaredClassInterfaces()
    {
        $option = false;
        self::$_oroboros_check_interfaces = $option;
        $class = get_called_class();
        $const = $class . '::' . 'TEST_CLASS_VALIDATE_INTERFACES';
        if ( defined( $const ) )
        {
            $option = constant( $const )
                ? true
                : false;
            $def_const = $class . '::' . 'TEST_CLASS_VALIDATE_INTERFACES';
            if ( !$option || !defined( $def_const ) || !constant( $def_const ) )
            {
                $option = false;
            }
        }
        self::$_oroboros_check_interfaces = $option;
    }

    /**
     * Sets up the explicitly declared class context indirect designations,
     * and the option as to whether to check them.
     * @beforeClass
     */
    public static function bootstrapDeclaredClassDesignation()
    {
        $option = false;
        self::$_oroboros_check_class_designation = $option;
        $class = get_called_class();
        $const = $class . '::' . 'TEST_CLASS_VALIDATE_DESIGNATIONS';
        if ( defined( $const ) )
        {
            $option = constant( $const )
                ? true
                : false;
        }
        self::$_oroboros_check_class_designation = $option;
    }

    /**
     * Sets up the explicitly declared class context,
     * and the option as to whether to check it.
     * @beforeClass
     */
    public static function bootstrapDeclaredClassContext()
    {
        $option = false;
        self::$_oroboros_check_class_context = $option;
        self::$_oroboros_expected_class_context = $option;
        $class = get_called_class();
        $const = $class . '::' . 'TEST_CLASS_VALIDATE_CLASS_CONTEXT';
        if ( defined( $const ) )
        {
            $option = constant( $const )
                ? true
                : false;
            $def_const = $class . '::' . 'TEST_CLASS_EXPECTED_CLASS_CONTEXT';
            if ( !$option || !defined( $def_const ) || !constant( $def_const ) )
            {
                $option = false;
            } else
            {
                self::$_oroboros_expected_class_context = constant( $def_const );
            }
        }
        self::$_oroboros_check_class_context = $option;
    }

    /**
     * Sets up the explicitly declared class type,
     * and the option as to whether to check it.
     * @beforeClass
     */
    public static function bootstrapDeclaredClassType()
    {
        $option = false;
        self::$_oroboros_check_class_type = $option;
        self::$_oroboros_expected_class_type = $option;
        $class = get_called_class();
        $const = $class . '::' . 'TEST_CLASS_VALIDATE_CLASS_TYPE';
        if ( defined( $const ) )
        {
            $option = constant( $const )
                ? true
                : false;
            $def_const = $class . '::' . 'TEST_CLASS_EXPECTED_CLASS_TYPE';
            if ( !$option || !defined( $def_const ) || !constant( $def_const ) )
            {
                $option = false;
            } else
            {
                self::$_oroboros_expected_class_type = constant( $def_const );
            }
        }
        self::$_oroboros_check_class_type = $option;
    }

    /**
     * Sets up the explicitly declared class scope,
     * and the option as to whether to check it.
     * @beforeClass
     */
    public static function bootstrapDeclaredClassScope()
    {
        $option = false;
        self::$_oroboros_check_class_scope = $option;
        self::$_oroboros_expected_class_scope = $option;
        $class = get_called_class();
        $const = $class . '::' . 'TEST_CLASS_VALIDATE_CLASS_SCOPE';
        if ( defined( $const ) )
        {
            $option = constant( $const )
                ? true
                : false;
            $def_const = $class . '::' . 'TEST_CLASS_EXPECTED_CLASS_SCOPE';
            if ( !$option || !defined( $def_const ) || !constant( $def_const ) )
            {
                $option = false;
            } else
            {
                self::$_oroboros_expected_class_scope = constant( $def_const );
            }
        }
        self::$_oroboros_check_class_scope = $option;
    }

    /**
     * Resets the baseline interfaces that all extension interfaces MUST honor.
     * @afterClass
     */
    public static function teardownExpectedBaselineInterfaces()
    {
        self::$_oroboros_baseline_interface_types = array();
        self::$_oroboros_check_interface_type = false;
    }

    /**
     * Resets the expected set of 3rd party or PHP internal
     * interfaces that the given test class MUST honor
     * @afterClass
     */
    public static function teardownExpectedInterfaces()
    {
        self::$_oroboros_expected_interfaces = array();
    }

    /**
     * Resets the expected set of class contexts
     * @afterClass
     */
    public static function teardownExpectedClassContexts()
    {
        self::$_oroboros_class_contexts = array();
        self::$_oroboros_check_class_context = false;
    }

    /**
     * Resets the expected set of class types
     * @afterClass
     */
    public static function teardownExpectedClassTypes()
    {
        self::$_oroboros_class_types = array();
        self::$_oroboros_check_class_type = false;
    }

    /**
     * Resets the expected set of class scopes
     * @afterClass
     */
    public static function teardownExpectedClassScopes()
    {
        self::$_oroboros_class_scopes = array();
        self::$_oroboros_check_class_scope = false;
    }

    /**
     * Resets the expected set of class context indirect designations
     * @afterClass
     */
    public static function teardownExpectedConstantDesignations()
    {
        self::$_oroboros_class_designations = array();
    }

    /**
     * Resets the explicitly declared class contract interface,
     * and the option as to whether to check it.
     * @afterClass
     */
    public static function teardownDeclaredClassContract()
    {
        self::$_oroboros_check_class_contract = false;
        self::$_oroboros_expected_contract = false;
    }

    /**
     * Resets the explicitly declared class api interface,
     * and the option as to whether to check it.
     * @afterClass
     */
    public static function teardownDeclaredClassApi()
    {
        self::$_oroboros_check_class_api = false;
        self::$_oroboros_expected_api = false;
    }

    /**
     * Resets the explicitly declared class enumerated interface,
     * and the option as to whether to check it.
     * @afterClass
     */
    public static function teardownDeclaredClassEnumerator()
    {
        self::$_oroboros_check_class_enumerator = false;
        self::$_oroboros_expected_enumerator = false;
    }

    /**
     * Resets the explicitly declared class interfaces
     * that must be validated against, and the option as to
     * whether to check them.
     * @afterClass
     */
    public static function teardownDeclaredClassInterfaces()
    {
        self::$_oroboros_check_interfaces = false;
    }

    /**
     * Resets the explicitly declared class context indirect designations,
     * and the option as to whether to check them.
     * @afterClass
     */
    public static function teardownDeclaredClassDesignation()
    {
        self::$_oroboros_check_class_designation = false;
    }

    /**
     * Resets the explicitly declared class context,
     * and the option as to whether to check it.
     * @afterClass
     */
    public static function teardownDeclaredClassContext()
    {
        self::$_oroboros_check_class_context = false;
        self::$_oroboros_expected_class_context = false;
    }

    /**
     * Resets the explicitly declared class type,
     * and the option as to whether to check it.
     * @afterClass
     */
    public static function teardownDeclaredClassType()
    {
        self::$_oroboros_check_class_type = false;
        self::$_oroboros_expected_class_type = false;
    }

    /**
     * Resets the explicitly declared class scope,
     * and the option as to whether to check it.
     * @afterClass
     */
    public static function teardownDeclaredClassScope()
    {
        self::$_oroboros_check_class_scope = false;
        self::$_oroboros_expected_class_scope = false;
    }

}
