<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\traits\assertions\integrity;

/**
 * <Oroboros Core Interface Evaluation Trait>
 * Provides assertions for insuring that expected interfaces,
 * contract interfaces, api interfaces, and enumerated interfaces
 * are honored by the testing subject.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category traits
 * @category unit-testing
 * @category assertions
 * @package oroboros/tests
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait InterfaceEvaluationTrait
{

    /**
     * <Contract Evaluation Method>
     * Evaluates that the contract interfaces are implemented as expected.
     * @param object|string $class A reference to any object
     *     or class from the core that is not a core internal
     * @return void
     */
    protected function assertContractInterfaceValid( $class )
    {
        $base_contract = '\\oroboros\\core\\interfaces\\contract\\BaseContract';
        $classname = get_called_class();
        $const = $classname . '::TEST_CLASS_EXPECTED_CONTRACT';
        if ( !defined( $const ) || (!constant( $const ) ) )
        {
            //Do not perform assertations if the defined interface is not set.
            return;
        }
        $expected = constant( $const );
        if ( !$expected
            || !interface_exists( $expected )
            || !($expected === $base_contract
            || is_subclass_of( $expected,
                $base_contract ))
        )
        {
            //Do not perform assertations if the defined contract is not set.
            return;
        }
        $classname = $class;
        if ( is_object( $class ) )
        {
            $classname = get_class( $class );
        }
        $this->assertInstanceOf( $expected, $class,
            sprintf( 'Failed to assert that [%1$s] honors expected contract '
                . 'interface [%2$s] at [%3$s] in test class [%4$s]', $classname,
                $expected, __METHOD__,
                get_class( $this ) ) );
    }

    /**
     * <Api Interface Evaluation Method>
     * Evaluates that the expected api interface is implemented as expected.
     * @param object|string $class A reference to any object
     *     or class from the core that is not a core internal
     * @return void
     */
    protected function assertApiInterfaceValid( $class )
    {
        $base_api = '\\oroboros\\core\\interfaces\\api\\BaseApi';
        $classname = get_called_class();
        $const = $classname . '::TEST_CLASS_EXPECTED_API';
        if ( !defined( $const ) || (!constant( $const ) ) )
        {
            //Do not perform assertations if the defined interface is not set.
            return;
        }
        $expected = constant( $const );
        if ( !$expected
            || !interface_exists( $expected )
            || !($expected === $base_api
            || is_subclass_of( $expected, $base_api ))
        )
        {
            //Do not perform assertations if the defined api
            //is not set or is not valid.
            return;
        }
        $classname = $class;
        if ( is_object( $class ) )
        {
            $classname = get_class( $class );
        }
        $this->assertInstanceOf( $expected, $class,
            sprintf( 'Failed to assert that [%1$s] honors expected api '
                . 'interface [%2$s] at [%3$s] in test class [%4$s]', $classname,
                $expected, __METHOD__, get_class( $this ) ) );
    }

    /**
     * <Enumerated Interface Evaluation Method>
     * Evaluates that the expected enumerated interface is implemented as expected.
     * @param object|string $class A reference to any object
     *     or class from the core that is not a core internal
     * @return void
     */
    protected function assertEnumeratedInterfaceValid( $class )
    {
        $base_enum = '\\oroboros\\core\\interfaces\\enumerated\\BaseEnum';
        $classname = get_called_class();
        $const = $classname . '::TEST_CLASS_EXPECTED_ENUMERATOR';
        if ( !defined( $const ) || (!constant( $const ) ) )
        {
            //Do not perform assertations if the defined interface is not set.
            return;
        }
        $expected = constant( $const );
        if ( !$expected
            || !interface_exists( $expected )
            || !($expected === $base_enum
            || is_subclass_of( $expected, $base_enum ))
        )
        {
            //Do not perform assertations if the defined enumerator
            //is not set or invalid.
            return;
        }
        $classname = $class;
        if ( is_object( $class ) )
        {
            $classname = get_class( $class );
        }
        $this->assertInstanceOf( $expected, $class,
            sprintf( 'Failed to assert that [%1$s] honors expected enumerated'
                . ' interface [%2$s] at [%3$s] in test class [%4$s]',
                $classname, $expected, __METHOD__, get_class( $this ) ) );
    }

    /**
     * <Interface Evaluation Method>
     * Evaluates that the expected 3rd party interface is implemented as expected.
     * @param object|string $class A reference to any object
     *     or class from the core that is not a core internal
     * @return void
     */
    protected function assertInterfaceValid( $class )
    {
        $classname = get_called_class();
        $const = $classname . '::TEST_CLASS_EXPECTED_INTERFACE';
        if ( !defined( $const ) || (!constant( $const ) ) )
        {
            //Do not perform assertations if the defined interface is not set.
            return;
        }
        $expected = constant( $const );
        $classname = $class;
        if ( is_object( $class ) )
        {
            $classname = get_class( $class );
        }
        $this->assertInstanceOf( $expected, $class,
            sprintf( 'Failed to assert that [%1$s] honors expected '
                . ' interface [%2$s] at [%3$s] in test class [%4$s]',
                $classname, $expected, __METHOD__, get_class( $this ) ) );
    }

}
