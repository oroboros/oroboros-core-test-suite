<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\traits\assertions\integrity;

/**
 * <Class Constant Evaluation Trait>
 * Provides assertions for insuring that class
 * constants exist and fit expected values.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category traits
 * @category unit-testing
 * @category assertions
 * @package oroboros/tests
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait ClassConstantEvaluationTrait
{

    /**
     * Performs an assertion to insure that the given
     * class constant is defined in a given class.
     * @param string|object $class An object or valid class name.
     * @param string $constant The key name of the class constant to check.
     * @return void
     */
    protected function assertClassConstantDefined( $class, $constant,
        $message = null )
    {
        $classname = $class;
        if ( is_object( $class ) )
        {
            $classname = get_class( $class );
        }
        $expected = $classname . '::' . $constant;
        if ( is_null( $message ) )
        {
            $message = sprintf(
                'Failed to assert that expected class constant [%1$s] is '
                . 'defined in class [%2$s], at [%3$s] of test class [%4$s].',
                $expected, $classname, __METHOD__, get_class( $this )
            );
        }
        $this->assertTrue( defined( $expected ), $message );
    }

    /**
     * Performs assertions to insure that a given class
     * constant exists and equals an expected value.
     * @param scalar|null|resource $expected The expected value of the constant.
     *     Can be any value that a class constant can actually have.
     * @param object|string $class The class or interface that should contain the constant
     * @param string $constant The key name of the constant
     * @return void
     */
    protected function assertClassConstantEquals( $expected, $class, $constant,
        $message = null )
    {
        $this->assertHasClassConstant( $classname, $constant );
        $classname = $class;
        if ( is_object( $class ) )
        {
            $classname = get_class( $class );
        }
        $literal = constant( $classname . '::' . $constant );
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Failed to assert that given class constant [%1$s] in class '
                . '[%2$s] with value [%3$s] equals expected value [%4$s], '
                . 'at [%5$s] of test class [%6$s].', $constant, $classname,
                $this->castValueToString( $literal ),
                $this->castValueToString( $expected ), __METHOD__,
                get_class( $this ) );
        }
        $this->assertEqual( $expected, $literal, $message );
    }

    /**
     * Performs assertions to insure that a given class
     * constant exists and does not equal an invalid value.
     * @param scalar|null|resource $expected The expected value of the constant.
     *     Can be any value that a class constant can actually have.
     * @param object|string $class The class or interface that should contain the constant
     * @param string $constant The key name of the constant
     * @return void
     */
    protected function assertClassConstantNotEquals( $expected, $class,
        $constant, $message = null )
    {
        $this->assertHasClassConstant( $classname, $constant );
        $classname = $class;
        if ( is_object( $class ) )
        {
            $classname = get_class( $class );
        }
        $literal = constant( $classname . '::' . $constant );
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Failed to assert that given class constant [%1$s] in class '
                . '[%2$s] with value [%3$s] does not equal invalid value [%4$s], '
                . 'at [%5$s] of test class [%6$s].', $constant, $classname,
                $this->castValueToString( $literal ),
                $this->castValueToString( $expected ), __METHOD__,
                get_class( $this ) );
        }
        $this->assertNotEqual( $expected, $literal, $message );
    }

    /**
     * Performs assertions to insure that a given class
     * constant exists and equals a value in a set of expected values.
     * @param array $expected An array of acceptable values for the constant.
     *     Can be any value that a class constant can actually have.
     * @param object|string $class The class or interface that should contain the constant
     * @param string $constant The key name of the constant
     * @return void
     */
    protected function assertClassConstantIn( $expected, $class, $constant,
        $message = null )
    {
        $this->assertHasClassConstant( $classname, $constant );
        $classname = $class;
        if ( is_object( $class ) )
        {
            $classname = get_class( $class );
        }
        $literal = constant( $classname . '::' . $constant );
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Failed to assert that given class constant [%1$s] in class '
                . '[%2$s] with value [%3$s] equals a valid value from acceptable set [%4$s], '
                . 'at [%5$s] of test class [%6$s].', $constant, $classname,
                $this->castValueToString( $literal ),
                $this->castValueToString( $expected ), __METHOD__,
                get_class( $this ) );
        }
        $this->assertTrue( in_array( $literal, $expected ), $message );
    }

    /**
     * Performs assertions to insure that a given class
     * constant exists and is not a value in a set of invalid values.
     * @param array $expected An array of acceptable values for the constant.
     *     Can be any value that a class constant can actually have.
     * @param object|string $class The class or interface that should contain the constant
     * @param string $constant The key name of the constant
     * @return void
     */
    protected function assertClassConstantNotIn( $expected, $class, $constant,
        $message = null )
    {
        $this->assertHasClassConstant( $classname, $constant );
        $classname = $class;
        if ( is_object( $class ) )
        {
            $classname = get_class( $class );
        }
        $literal = constant( $classname . '::' . $constant );
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Failed to assert that given class constant [%1$s] in class '
                . '[%2$s] with value [%3$s] is not a value from invalid set [%4$s], '
                . 'at [%5$s] of test class [%6$s].', $constant, $classname,
                $this->castValueToString( $literal ),
                $this->castValueToString( $expected ), __METHOD__,
                get_class( $this ) );
        }
        $this->assertTrue( !in_array( $literal, $expected ), $message );
    }

}
