<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\traits\utility;

/**
 * <Oroboros Core Test Suite Test Case Expected Error Handler Trait>
 * Provides methods for handling expected errors without failing CI builds
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category traits
 * @category unit-testing
 * @category error-handling
 * @package oroboros/tests
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait TestCaseExpectedErrorHandlerTrait
{

    /**
     * Represents an expected error, if one existed, which can be checked
     * against by later classes when testing against logic that would
     * throw an error. This allows expected errors to not fail CI builds
     * unless they are not passed as the test case anticipates.
     * @var array
     */
    private $_expected_error = array();

    /**
     * Represents a previous error handler, if one exists
     * @var null|callable
     */
    private $_previous_error_handler;

    /**
     * Represents whether the previous error handler should be notified.
     * In some cases, this may cause tests to fail when handling expected errors,
     * or trigger reporting modules on expected use cases, in which case this
     * should be omitted.
     * @var bool
     */
    private $_notify_previous_error_handler = false;

    /**
     * Sets an error handler that will silence
     * expected errors from failing CI builds.
     *
     * This should be called directly whenever an error
     * is expected that would otherwise cause a fail.
     *
     * @todo Refactor this with a custom extension
     *     of the Oroboros error handler, after
     *     testing coverage for that logic is completed.
     * @param bool $alert_previous If true, the error details will also be
     *     passed to the previous error handler, if one exists when an error
     *     is triggered. Default false.
     * @return void
     */
    protected function enableExpectedErrorHandler( $alert_previous = false )
    {
        $previous = set_error_handler( array(
            $this,
            'handleExpectedTestCaseError' ) );
        $this->_previous_error_handler = $previous;
        $this->_notify_previous_error_handler = ($alert_previous)
            ? true
            : false;
    }

    /**
     * Replaces the prior error handler.
     * This should be called immediately after handling an expected error,
     * to prevent accidentally silencing legitimate errors.
     * @return void
     */
    protected function disableExpectedErrorHandler()
    {
        $this->_expected_error = array();
        $this->_notify_previous_error_handler = false;
        restore_error_handler();
        $this->_previous_error_handler = null;
    }

    /**
     * Resets the internally stored expected error, so that another error
     * can be checked without being polluted by a previous one.
     * @return void
     */
    protected function resetExpectedError()
    {
        $this->_expected_error = array();
    }

    /**
     * Returns the last error received when the expected error handler is enabled.
     * If no previous error was received, will return false.
     * @param bool $reset If true, will also reset the previous error
     *     back to its null state. Default false.
     * @return bool|array
     */
    protected function getExpectedError( $reset = false )
    {
        $error = $this->_expected_error;
        if ( $reset )
        {
            $this->resetExpectedError();
        }
        if ( empty( $error ) )
        {
            return false;
        }
        return $error;
    }

    /**
     * This is an error handling method that silences all recoverable errors.
     * This is used when testing for an expected error, which would otherwise
     * fail a CI build.
     *
     * This error handler can be registered with enableExpectedErrorHandler(),
     * and is disabled with disableExpectedErrorHandler().
     *
     * @param type $errno The error code
     * @param type $errstr The string representation of the error message
     * @param type $errfile The file the error occurred in
     * @param type $errline The line that the error occurred on
     * @return bool This method always returns true
     */
    public function handleExpectedTestCaseError( $errno, $errstr,
        $errfile = null, $errline = null, $errcontext = null )
    {
        if ( $this->_notify_previous_error_handler )
        {
            $this->notifyPreviousErrorHandler( $errno, $errstr, $errfile,
                $errline, $errcontext );
        }
        $this->_expected_error = array(
            'code' => $errno,
            'message' => $errstr,
            'file' => $errfile,
            'line' => $errline
        );
        return true;
    }

    /**
     * Notifies a previous error handler of an error, if one exists.
     * @param type $errno
     * @param type $errstr
     * @param type $errfile
     * @param type $errline
     * @param type $errcontext
     * @return bool
     */
    private function notifyPreviousErrorHandler( $errno, $errstr, $errfile,
        $errline, $errcontext = null )
    {
        if ( is_null( $this->_previous_error_handler ) )
        {
            return;
        }
        return call_user_func_array( $this->_previous_error_handler,
            array(
            $errno,
            $errstr,
            $errfile,
            $errline,
            $errcontext,
            ) );
    }

}
